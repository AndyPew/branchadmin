
  <!--   Core   -->
  <script src="<?php echo base_url('assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js')?>"></script>
  <!--   Optional JS   -->
  <script src="<?php echo base_url('assets/js/plugins/chart.js/dist/Chart.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/plugins/chart.js/dist/Chart.extension.js')?>"></script>
  <!--   Argon JS   -->
  <script src="<?php echo base_url('assets/js/argon-dashboard.min.js?v=1.1.0')?>"></script>
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>

  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-dashboard-free"
      });
  </script>
  <script type="text/javascript">
      $(function(){
          var id = $('#SchoolID option:selected').val();
          $('#output').val(id);
      });    
  </script>   
  <script>
    var p=$('#fos p');
    var divh=$('#fos').height();
    while ($(p).outerHeight()>divh) {
        $(p).text(function (index, text) {
            return text.replace(/\W*\s(\S)*$/, '...');
        });
    }
  </script>
</body>

</html>