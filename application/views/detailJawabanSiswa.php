<div id="FormSoal" class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="">Tambah Tugas</a>
        <!-- Form -->
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="modal-body">
                <div class="form-group">
                    <div class="box-body">
                        <h1><?php echo $tugas[0]->HomeWorkTitle?></h1>
                    </div>
                    <br>
                    <div class="box-body">
                        <p><?php echo $tugas[0]->HomeWorkDesc?></p>
                    </div>
                    <br>
                    <div class="box-body">
                        <p><?php echo $tugas[0]->ClassID?></p>
                    </div>
                    <br>
                    <div class="box-body">
                        <label>Hari Tugas Dikumpulkan : </label>
                        <p><?php echo $tugas[0]->HomeWorkDate?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-body">
            <?php
                foreach ($pertanyaan as $key=>$question) :
            ?>
            <div class="card shadow mt-4">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="box-body">
                            <h2><?php echo $key + 1?>. <?php echo $question->HomeWorkQuestionContent ?></h2>
                        </div>
                        <br>
                        <div class="box-body ml-4">
                                <?php 
                                    foreach ($jawaban[$key] as $key2=>$answer) : 
                                ?>
                                    <div class="row" style="margin: 0">
                                        <label style="width: 28px"><?php if ($key2 == 0) { echo 'A.'; } else if ($key2 == 1) { echo 'B.'; } else if ($key2 == 2) { echo 'C.'; } else if ($key2 == 3) { echo 'D.'; } else { echo 'E.'; } ?></label>
                                        <div style="width: calc(100% - 28px)">
                                            <p><?php echo $answer->HomeWorkAnswerContent; if ($question->HomeWorkUserAnswerValue == $key2 + 1 ) { echo ' <strong>(Jawaban Siswa)</strong>'; } ?></p>
                                        </div>
                                    </div>
                                <?php 
                                    endforeach;
                                ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
                endforeach;
            ?>
        </div>
    </div>
</div>
