<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Kelola Tugas</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="box-title">
                            <a>Jawaban Siswa - <?php echo $DataTugas[0]->HomeWorkTitle ?></a>  
                        </h3>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center" style="margin-bottom: 2rem;">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                Nama Siswa
                            </th>
                            <th scope="col">
                                Jawaban
                            </th>
                            <th scope="col">
                                Dibuat pada
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list p-8">
                        <?php 
                            foreach ($daftarSiswa as $Siswa) :
                        ?>
                            <tr>
                                <td scope="col">
                                    <?php echo $Siswa->UserName ?>
                                </td>
                                <td scope="col">
                                    <?php echo $Siswa->TotalJawaban ?> / <?php echo $totalSoal[0]->total ?> 
                                </td>
                                <td scope="col">
                                    <?php echo $Siswa->createdAt ?>    
                                </td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <a class="dropdown-item" href="<?php echo base_url('C_kelolaTugas/detailJawabanSiswa/'.$Siswa->UserID.'/'.$DataTugas[0]->HomeWorkID) ?>">Detail</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
