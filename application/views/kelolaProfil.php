
  <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaProfil')) ?>">Edit Profil</a>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <div class="media-body d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold"><?php echo $profil->UserName;?></span>
                </div>
                <span class="avatar ml-2 avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="<?php echo base_url($profil->UserProfile);?>">
                </span>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Selamat datang</h6>
              </div>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="<?php echo base_url('Main/logout') ?>" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-4">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-6 order-xl-2 mb-5 mb-xl-0" style="margin:0 auto;">
                <div class="card card-profile shadow" style="margin: 40px 0;">
                    <div class="row justify-content-center">
                        <div class="col-lg-3 order-lg-2">
                            <div class="card-profile-image">
                            <a href="#">
                                <img src="<?php echo base_url($profil->UserProfile);?>" class="rounded-circle">
                            </a>
                            </div>
                        </div>
                    </div>
                <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                        <a href="#myModal-edit<?php echo $profil->UserID?>" data-toggle = "modal" class="btn btn-sm btn-default float-right">Edit</a>
                        <a href="#myModal-changePassword" data-toggle = "modal" class="btn btn-sm btn-default float-right">Change Password</a>
                    </div>
                </div>
                <div class="card-body pt-0 pt-md-4">
                    <div class="row">
                        <div class="col">
                            <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                                
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <h3>
                            <i class="ni education_hat mr-2"></i>
                            <?php echo $profil->UserName;?><span class="font-weight-light"></span>
                        </h3>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i><?php echo $profil->UserBirth;?>
                        </div>
                        <div class="h5 mt-4">
                          <i class="ni education_hat mr-2"></i>
                            <?php 
                                if($profil->UserGender == 1){
                                    echo 'Laki-laki';
                                }     
                                else{
                                    echo 'Perempuan';
                                }
                            ?>
                        </div>
                        <div>
                            <i class="ni education_hat mr-2"></i>
                            <?php echo $profil->UserEmail;?>
                        </div>
                        <hr class="my-4" />
                        <p>
                          <?php 
                              if($profil->UserLevel == 2) {
                                echo 'Admin Guru';
                              } else if($profil->UserLevel == 3) {
                                echo 'Admin Sekolah';
                              } else {
                                echo 'Admin';
                              }
                          ?>
                        </p>
                    </div>
                </div>
             </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal small fade" id="myModal-edit<?php echo $profil->UserID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 id="myModalLabel">Edit profil</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <!-- Data Guru -->
                <form method="post" action="<?php echo (base_url('C_kelolaProfil/update/'.$profil->UserID)) ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="box-body">
                                <label>Nama</label><br>
                                <input type="text" class="form-control" name="UserName" value="<?php echo $profil->UserName; ?>">
                            </div>
                            <div class="box-body">
                                <label>Tanggal Lahir</label><br>
                                <input type="date" class="form-control" name="UserBirth" value="<?php echo $profil->UserBirth; ?>">
                            </div>
                            <div class="box-body">
                                <label>Email</label><br>
                                <input type="text" class="form-control" name="UserEmail" value="<?php echo $profil->UserEmail; ?>">
                            </div>
                            <div class="form-group">
                                <h3>Jenis Kelamin</h3>
                                <div class="custom-control custom-radio mb-3">
                                    <input value="1" name="Gender" class="custom-control-input" id="Gender1" type="radio" <?php if($profil->UserGender == 1){echo "checked";} ?> >
                                    <label class="custom-control-label" for="Gender1">Laki - laki</label>
                                </div>
                                <div class="custom-control custom-radio mb-3">
                                    <input value="2" name="Gender" class="custom-control-input" id="Gender2" type="radio" <?php if($profil->UserGender == 2){echo "checked";} ?>>
                                    <label class="custom-control-label" for="Gender2">Perempuan</label>
                                </div>
                            </div>
                              <div class="box-body">
                              <label class="user-icon">
                                <input type="file" id="img-input" accept="gambar/*" name="gambar">
                                <div class="uploaded_image" style="width:40px;height:40px;"><img src="<?php echo(base_url('gambar/user-in-a-square.png'))?>" style="width:40px;height:40px;" id="img-profile"></div>
                              </label>
                            </div>
                        </div>
                    </div>                                
                    <div class="modal-footer">
                    <button class="btn btn-block btn-success" style="width: 170px">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal small fade" id="myModal-changePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 id="myModalLabel">Change Password</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <!-- Data Guru -->
                <form method="post" action="<?php echo (base_url('C_kelolaProfil/changePassword/'.$profil->UserID)) ?>" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="box-body">
                                <label>Password Lama</label><br>
                                <input type="password" class="form-control" name="UserPassOld">
                            </div>
                            <div class="box-body">
                                <label>Password Baru</label><br>
                                <input type="password" class="form-control" name="UserPassNew">
                            </div>
                        </div>
                    </div>                                
                    <div class="modal-footer">
                    <button class="btn btn-block btn-success" style="width: 170px">Change Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

  <script src="<?php echo base_url('assets/js/Croppie/croppie.js')?>"></script>

  <script> 
    (function(){
    $(document).ready(function(){
      $image_crop = $('#image_demo').croppie({
      enableExif: true,
      enableOrientation: true,
      viewport: {
      width:200,
      height:200,
      type:'square' 
      // type: 'circle'
      },
      boundary:{
      width:300,
      height:300
      },
      enableOrientation: true
    });

    $('#img-input').on('change', function(){
      var reader = new FileReader();
      reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result,
      }).then(function(){
        console.log('jQuery bind complete');
      });
      }
      reader.readAsDataURL(this.files[0]);
      $('#uploadimageModal').modal('show');
    });

    $('.crop-rotate').on('click', function(event) {
      $image_crop.croppie('rotate',
        parseInt($(this).data('deg'))
      );
    });

    $('.crop_image').click(function(event){
      $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
      }).then(function(response){
      $.ajax({
        url:"upload_foto/",
        type: "POST",
        data:{"gambar": response},
        success:function(data)
        {
        $('#uploadimageModal').modal('hide');
        $('.uploaded_image').html(data);
        
        }
      });
      var img = document.getElementById('img-profile');
        img.style.display="none";
      })
    });

    }); 
    })(jQuery); 
  </script>

  <!--===============================================================================================-->
  <div id="uploadimageModal" class="modal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Photo profile</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="cursor: pointer;">×</span></button>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <div id="image_demo" style="margin-top:30px; height:350px;"></div>
          </div>
          <div class="text-center">
            <button class="btn crop-rotate" data-deg="-90" style="cursor: pointer;"><i class="fas fa-undo"></i></button>
            <button class="btn crop-rotate" data-deg="90" style="cursor: pointer;"><i class="fas fa-redo"></i></button>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success crop_image" 	style="cursor: pointer;">Upload</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" style="cursor: pointer;">Close</button>
        </div>
      </div>
    </div>
  </div><br>
  <!--===============================================================================================-->
</div>