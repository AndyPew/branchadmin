
<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaJadwal')) ?>">Kelola Jadwal</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid mt-4 mb--4">
      <div class="row mt-5">
        <div class="col-xl-12 mb-5 mb-xl-0">

          <?php 
              foreach ($daftarKelas as $Kelas) :
          ?>
          
            <div class="card shadow">
              <div class="card-header border-0">
                <div class="row align-items-center">
                  <div class="col">
                    <h2 class="mb-0"><?php echo ($Kelas->ClassName) ?></h2>
                  </div>
                  <div class="col text-right">
                    <a href="<?php echo base_url('C_kelolaJadwal/edit/'.$Kelas->ClassID); ?>" class="btn btn-sm btn-primary pl-4 pr-4">Edit</a>
                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <!-- Projects table -->
                <table class="table align-items-center table-flush">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Jam</th>
                      <th scope="col">Senin</th>
                      <th scope="col">Selasa</th>
                      <th scope="col">Rabu</th>
                      <th scope="col">Kamis</th>
                      <th scope="col">Jumat</th>
                      <th scope="col">Sabtu</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      for ($i = 0; $i < @$Kelas->TotalJam->SchoolScheduleHour; $i++) :
                    ?>
                    <tr>
                      <th scope="row">
                        <?php echo $i + 1; ?>
                      </th>
                      <?php
                        foreach ($Kelas->Jadwal as $index=>$Jadwal) :
                      ?>
                        <td data-toggle="tooltip" data-placement="bottom" title="<?php  echo @$Jadwal[$i]->SchoolScheduleDesc; ?>">
                          <h4 class="mb-0"><?php  echo @$Jadwal[$i]->SubjectName; ?></h4>
                          <span><?php  echo @$Jadwal[$i]->UserName; ?><span>
                        </td>
                      <?php 
                        endforeach;
                      ?>
                    </tr>
                    <?php 
                        endfor;
                    ?>
                    
                  </tbody>
                </table>
              </div>
            </div>
            <br>

          <?php 
              endforeach;
          ?>
        </div>
      </div>