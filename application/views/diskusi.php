<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Diskusi</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="table-responsive">
                <table class="table align-items-center" style="margin-bottom: 2rem;">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                No
                            </th>
                            <th scope="col">
                                Nama Kelas
                            </th>
                            <th scope="col">
                                Dibuat pada
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list p-8">
                        <?php 
                            foreach ($DataKelas as $index=>$Kelas) :
                        ?>
                            <tr>
                                <td scope="col">
                                    <?php echo $index + 1; ?>
                                </td>
                                <td scope="col">
                                    <?php echo $Kelas->ClassName; ?>
                                </td>
                                <td scope="col">
                                    <?php echo $Kelas->createdAt; ?>
                                </td>
                                <td scope="col">
                                  <button class="btn btn-outline-primary ml-4" type="button" onclick="window.location.href='<?php echo (base_url('C_diskusi/Diskusi/'.$Kelas->ClassID)) ?>'">
                                    <span class="btn-inner--text">Diskusi</span>
                                  </button>
                                </td>
                            </tr>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
