
<div class="main-content" id="Jadwal">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div class="container-fluid">
            <!-- Brand -->
            <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Edit Jadwal - <?php echo ($dataKelas->ClassName) ?></a>
            <!-- Form -->
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="container-fluid mt-4 mb--4">
        <div class="row mt-5">
            <form class="col-xl-12 mb-5 mb-xl-0" method="POST" action="<?php echo base_url('C_kelolaJadwal/add') ?>">

                <input type="hidden" name="ClassID" value="<?php echo ($dataKelas->ClassID) ?>">

                <div class="card shadow mb-4" v-for="(myData, index) in data">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h2 class="mb-0">{{myData.Hari}}</h2>
                                <input type="hidden" :name="'IndexCount.' + index" :value="myData.Data.length">
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Jam</th>
                                    <th scope="col">Mata Pelajaran</th>
                                    <th scope="col">Guru</th>
                                    <th scope="col">Keterangan</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(jadwal, ind) in myData.Data" v-show="!jadwal.delete">
                                    <th scope="col">
                                        {{ind + 1}}
                                    </th>
                                    <th scope="col">
                                        <select class="form-control" @change="jadwal.update = true" id="SubjectID" v-model="jadwal.Pelajaran" :name="'SubjectID.' + index + '.' + ind">
                                            <?php 
                                                foreach ($daftarPelajaran as $Pelajaran) {
                                                    echo '<option value="'.$Pelajaran->SubjectSchoolID.'">'.$Pelajaran->SubjectName.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </th>
                                    <th scope="col">
                                        <select class="form-control" @change="jadwal.update = true" id="SubjectID" v-model="jadwal.Guru" :name="'UserID.' + index + '.' + ind">
                                            <?php 
                                                foreach ($daftarGuru as $Guru) {
                                                    echo '<option value="'.$Guru->UserID.'">'.$Guru->UserName.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </th>
                                    <th scope="col">
                                        <textarea type="text" @keyup="jadwal.update = true" :name="'Desc.' + index + '.' + ind" v-model="jadwal.Keterangan" class="form-control form-control-alternative" style="box-shadow: none; border: 1px solid #cad1d7; min-height: 40px" rows="1" placeholder="Keterangan disini..." required></textarea>
                                    </th>
                                    <th scope="col">
                                        <div style="position: absolute">
                                            <input type="hidden" :name="'JamPelajaran.' + index + '.' + ind" :value="ind + 1"><br>
                                            <input type="hidden" :name="'New.' + index + '.' + ind" :value="jadwal.new"><br>
                                            <input type="hidden" :name="'Update.' + index + '.' + ind" :value="jadwal.update"><br>
                                            <input type="hidden" :name="'Delete.' + index + '.' + ind" :value="jadwal.delete"><br>
                                            <input type="hidden" :name="'ID.' + index + '.' + ind" :value="jadwal.ID">
                                        </div>
                                        <a @click="hapusData(index, ind)" v-if="(ind +1) == myData.Data.length"  style="margin-bottom: 0px; font-size: 24px; cursor: pointer;"><span aria-hidden="true">&times;</span></a>
                                    </th>
                                </tr>
                                
                                <tr>
                                    <td scope="col" colspan="5">
                                        <button type="button" class="btn btn-block btn-outline-primary" @click="addData(index)">Tambahkan Jam</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <button class="btn btn-block btn-primary mb-4" @click="send()">Update Data</button>
            </form>
        </div>  
    </div>
</div>    

<script>
var vo = new Vue({
    el: '#Jadwal',
    name: 'Jadwal',
    data() {
        return {
            data: [
                {
                    Hari: 'Senin',
                    Data: [
                    <?php 
                        foreach($Jadwal[1] as $index=>$jdl) :
                    ?>
                        {
                            ID: <?php echo $jdl->SchoolScheduleID; ?>,
                            Pelajaran: <?php echo $jdl->SubjectSchoolID; ?>,
                            Guru: <?php echo $jdl->UserID; ?>, 
                            Keterangan: '<?php echo $jdl->SchoolScheduleDesc; ?>',
                            new: false, 
                            update: false, 
                            delete: false
                        },
                    <?php 
                        endforeach;
                    ?>
                    ]
                },
                {
                    Hari: 'Selasa',
                    Data: [
                    <?php 
                        foreach($Jadwal[2] as $index=>$jdl) :
                    ?>
                        {
                            ID: <?php echo $jdl->SchoolScheduleID; ?>,
                            Pelajaran: <?php echo $jdl->SubjectSchoolID; ?>,
                            Guru: <?php echo $jdl->UserID; ?>, 
                            Keterangan: '<?php echo $jdl->SchoolScheduleDesc; ?>',
                            new: false, 
                            update: false, 
                            delete: false
                        },
                    <?php 
                        endforeach;
                    ?>
                    ]
                },
                {
                    Hari: 'Rabu',
                    Data: [
                    <?php 
                        foreach($Jadwal[3] as $index=>$jdl) :
                    ?>
                        {
                            ID: <?php echo $jdl->SchoolScheduleID; ?>,
                            Pelajaran: <?php echo $jdl->SubjectSchoolID; ?>,
                            Guru: <?php echo $jdl->UserID; ?>, 
                            Keterangan: '<?php echo $jdl->SchoolScheduleDesc; ?>',
                            new: false, 
                            update: false, 
                            delete: false
                        },
                    <?php 
                        endforeach;
                    ?>
                    ]
                },
                {
                    Hari: 'Kamis',
                    Data: [
                    <?php 
                        foreach($Jadwal[4] as $index=>$jdl) :
                    ?>
                        {
                            ID: <?php echo $jdl->SchoolScheduleID; ?>,
                            Pelajaran: <?php echo $jdl->SubjectSchoolID; ?>,
                            Guru: <?php echo $jdl->UserID; ?>, 
                            Keterangan: '<?php echo $jdl->SchoolScheduleDesc; ?>',
                            new: false, 
                            update: false, 
                            delete: false
                        },
                    <?php 
                        endforeach;
                    ?>
                    ]
                },
                {
                    Hari: 'Jumat',
                    Data: [
                    <?php 
                        foreach($Jadwal[5] as $index=>$jdl) :
                    ?>
                        {
                            ID: <?php echo $jdl->SchoolScheduleID; ?>,
                            Pelajaran: <?php echo $jdl->SubjectSchoolID; ?>,
                            Guru: <?php echo $jdl->UserID; ?>, 
                            Keterangan: '<?php echo $jdl->SchoolScheduleDesc; ?>',
                            new: false, 
                            update: false, 
                            delete: false
                        },
                    <?php 
                        endforeach;
                    ?>
                    ]
                },
                {
                    Hari: 'Sabtu',
                    Data: [
                    <?php 
                        foreach($Jadwal[6] as $index=>$jdl) :
                    ?>
                        {
                            ID: <?php echo $jdl->SchoolScheduleID; ?>,
                            Pelajaran: <?php echo $jdl->SubjectSchoolID; ?>,
                            Guru: <?php echo $jdl->UserID; ?>, 
                            Keterangan: '<?php echo $jdl->SchoolScheduleDesc; ?>',
                            new: false, 
                            update: false, 
                            delete: false
                        },
                    <?php 
                        endforeach;
                    ?>
                    ]
                },
            ]
        }
    },
    methods: {
        addData(day) {
            if (this.data[day].Data.length > 0) {
                if (this.data[day].Data[this.data[day].Data.length - 1].delete == true) {
                    this.data[day].Data[this.data[day].Data.length - 1].delete = false
                } else {
                    this.data[day].Data.push({ID: null, Pelajaran: 0, Guru: 0, Keterangan: '', new: true, update: false, delete: false})
                }
            } else {
                this.data[day].Data.push({ID: null, Pelajaran: 0, Guru: 0, Keterangan: '', new: true, update: false, delete: false})
            }
        },
        hapusData(day, index) {
            if (this.data[day].Data[index].new) {
                this.data[day].Data.splice(index, 1);
            } else {
                this.data[day].Data[index].delete = true;
            }
        },
    }
});
</script>