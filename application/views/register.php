<!--

=========================================================
* Argon Dashboard - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
  <title>Branch - Daftarkan Sekolah</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon -->
  <link href="<?php echo base_url('assets/img/BranchRounded.png')?>" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="<?php echo(base_url('assets/js/plugins/nucleo/css/nucleo.css'))?>" rel="stylesheet" />
  <link href="<?php echo(base_url('assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css'))?>" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="<?php echo(base_url('assets/css/argon-dashboard.css?v=1.1.0'))?>" rel="stylesheet" />
</head>

<body class="bg-default">
  <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
      <div class="container px-4">
        <a class="navbar-brand" href="<?php echo base_url('')?>">
          <img src="<?php echo base_url('assets/img/Branch_white.png')?>" style="height: 40px"/>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-collapse-main">
          <!-- Collapse header -->
          <div class="navbar-collapse-header d-md-none">
            <div class="row">
              <div class="col-6 collapse-brand">
                <a href="">
                  <img src="<?php echo base_url('assets/img//Branch_blue.png')?>">
                </a>
              </div>
              <div class="col-6 collapse-close">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                  <span></span>
                  <span></span>
                </button>
              </div>
            </div>
          </div>
          <!-- Navbar items -->
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link nav-link-icon" href="<?php echo base_url('C_register')?>">
                <i class="ni ni-circle-08"></i>
                <span class="nav-link-inner--text">Register</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-link-icon" href="<?php echo base_url('C_login')?>">
                <i class="ni ni-key-25"></i>
                <span class="nav-link-inner--text">Login</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
              <h1 class="text-white">Selamat Datang di Branch!</h1>
              <p class="text-lead text-light">bergabunglah dengan kami dengan mendaftarkan sekolahmu</p>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <!-- Table -->
      <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
          <div class="card bg-secondary shadow border-0">
            <div class="card-header bg-transparent">
              <h2>Daftarkan Sekolah</h2>
            </div>
            <div class="card-body px-lg-5 py-lg-5">
              <form action="" method="post">
                <div class="form-group">
                  <div class="input-group input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                    </div>
                    <input class="form-control" name="SchoolStatisticNumber" placeholder="NSS" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-hat-3"></i></span>
                    </div>
                    <input class="form-control" name="SchoolNationalUnicID" placeholder="NPSN" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-single-02"></i></span>
                    </div>
                    <input class="form-control" name="SchoolName" placeholder="Nama Sekolah" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-pin-3"></i></span>
                    </div>
                    <input class="form-control" name="SchoolAddress" placeholder="Alamat Sekolah" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control"  name="SchoolEmail" placeholder="Email Sekolah" type="email">
                  </div>
                </div>
                <div class="form-group">
                  <h3>Tingkatan Sekolah</h3>
                  <div class="custom-control custom-radio mb-3">
                    <input value="1" name="SchoolLevelID" class="custom-control-input" id="school-level1" type="radio">
                    <label  class="custom-control-label" for="school-level1">SD/Sederajat</label>
                  </div>
                  <div class="custom-control custom-radio mb-3">
                    <input value="2" name="SchoolLevelID" class="custom-control-input" id="school-level2" type="radio">
                    <label class="custom-control-label" for="school-level2">SMP/Sederajat</label>
                  </div>
                  <div class="custom-control custom-radio mb-3">
                    <input value="3" name="SchoolLevelID" class="custom-control-input" id="school-level3" type="radio">
                    <label class="custom-control-label" for="school-level3">SMA/Sederajat</label>
                  </div>
                </div>
                <div class="row my-4">
                  <div class="col-12">
                    <div class="custom-control custom-control-alternative custom-checkbox">
                      <input name="PrivacyPolicy" class="custom-control-input" id="customCheckRegister" type="checkbox">
                      <label class="custom-control-label" for="customCheckRegister">
                        <span class="text-muted">I agree with the <a href="#!">Privacy Policy</a></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="text-center">
                  <button class="btn btn-primary">Create Account</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>



    <div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
      <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content bg-gradient-danger">
          <div class="modal-header">
            <h6 class="modal-title" id="modal-title-notification">Berhasil</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="py-3 text-center">
                <i class="ni ni-bell-55 ni-3x"></i>
                <h4 class="heading mt-4">Yeay! Sekolah Anda Sudah Terdaftar!</h4>
                <p>Tunggu proses verifikasi sekolah anda. Kami akan menguhubungi anda secepatnya melalui email yang anda kirimkan.</p>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Tutup</button>
              <button type="button" class="btn btn-white" onclick="window.location.href='<?php echo base_url('C_login') ?>'">Ok, Lanjutkan</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-notification2" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
      <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content bg-gradient-danger">
          <div class="modal-header">
            <h6 class="modal-title" id="modal-title-notification">Gagal</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="py-3 text-center">
                <i class="ni ni-bell-55 ni-3x"></i>
                <h4 class="heading mt-4">Sekolah Anda Sudah Terdaftar</h4>
                <p>Tunggu proses verifikasi sekolah anda atau login dengan akun anda</p>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-white" onclick="window.location.href='<?php echo base_url('C_login') ?>'">Ok, Tutup</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--   Core   -->
  <script src="<?php echo(base_url('assets/js/plugins/jquery/dist/jquery.min.js'))?>"></script>
  <script src="<?php echo(base_url('assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js'))?>"></script>
  <!--   Optional JS   -->
  <!--   Argon JS   -->
  <script src="<?php echo(base_url('assets/js/argon-dashboard.min.js?v=1.1.0'))?>"></script>
  <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
  
  <?php if(@$modal):?>
    <script> $('#modal-notification').modal('show');</script>
  <?php endif;?>

  <?php if(@$error):?>
    <script> $('#modal-notification2').modal('show');</script>
  <?php endif;?>
  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-dashboard-free"
      });
  </script>
</body>

</html>