<div id="data" class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="./index.html">Diskusi - <?php echo $dataKelas->ClassName; ?></a>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div id="chat-scroll" class="p-4" style="overflow-x:hidden;
    overflow-y:visible;
    height:500px;">
                <?php 
                    foreach ($dataDiskusi as $index=>$Diskusi) :
                        $LastID = @$dataDiskusi[$index - 1] ? $dataDiskusi[$index - 1]->UserID : null;
                ?>
                <?php
                    if ($Diskusi->UserID == $this->session->userdata('UserID')) {
                ?>
                <div class="col text-right">
                    <div class="card shadow mt-2 p-2 pl-4 pr-4" style="display: inline-block; border-radius: 20px 20px 4px 20px; background-color: #1C375B; color: #FFFFFF">
                        <p class="mb-0"><?php echo $Diskusi->DiscussionContent ?></p>
                    </div>
                </div>
                <?php
                    } else {
                ?>
                <p class="mb-0"><?php echo $Diskusi->UserID == $LastID ? null : $Diskusi->UserName ?></p>
                <div class="card shadow  mt-2 p-2 pl-4 pr-4" style="display: inline-block; border-radius: 20px 20px 20px 4px;">
                    <p class="mb-0"><?php echo $Diskusi->DiscussionContent ?></p>
                </div>
                <br>
                <?php
                    }
                ?>
                <?php 
                    endforeach;
                ?>
            </div>
            <form action="" method="POST">
                <div class="box-body row">
                    <div class="col-9">
                        <input type="text" class="form-control" name="Message" placeholder="Your message here..." required>
                    </div>
                    <div class="col-3">
                        <button class="btn btn-block btn-primary">Kirim</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

$(document).ready(function() {
    $('#chat-scroll').animate({
        scrollTop: $('#chat-scroll').get(0).scrollHeight
    }, 1000);
});

</script>