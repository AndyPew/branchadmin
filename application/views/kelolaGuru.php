<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaGuru')) ?>">Kelola Guru</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" method="post" action="<?php echo (base_url('C_kelolaGuru/search')) ?>" enctype="multipart/form-data">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text" name="keyword">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="box-title">
                            <a href="#myModal-delete" class="btn btn-block btn-success" style="width: 170px" data-base_url = "<?php echo base_url('C_kelolaGuru'); ?>" data-toggle = "modal">Tambahkan Guru</a>  
                        </h3>
                    </div>
                    <div class="col text-right">
                        <nav class="pagination-nav" aria-label="...">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">
                                    <i class="fa fa-angle-left"></i>
                                    <span class="sr-only">Previous</span>
                                </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active">
                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                <a class="page-link" href="#">
                                    <i class="fa fa-angle-right"></i>
                                    <span class="sr-only">Next</span>
                                </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                NIP
                            </th>
                            <th scope="col">
                                Nama
                            </th>
                            <th scope="col">
                                Email
                            </th>
                            <th scope="col">Jenis Kelamin</th>
                            <th scope="col">
                                Terakhir Masuk
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        
                        <?php 
                            foreach ($daftarGuru as $Guru) :
                        ?>

                        <tr>
                            <td class="status">
                                <span class="badge badge-dot mr-4">
                                    <?php echo $Guru->UserNationalEmployeeID;?>
                                </span>
                            </td>
                            <th scope="row" class="name">
                                <div class="media align-items-center">
                                    <a href="<?php echo base_url($Guru->UserProfile);?>" class="avatar mr-3" style="overflow:hidden;">
                                    <img alt="Image placeholder" src="<?php echo base_url($Guru->UserProfile);?>" style="border-radius:0px;">
                                    </a>
                                    <div class="media-body">
                                        <span class="mb-0 text-sm"><?php echo $Guru->UserName;?></span>
                                    </div>
                                </div>
                            </th>
                            <td class="budget">
                                <?php echo $Guru->UserEmail;?>
                            </td>
                            <td class="align-items-center">
                                <span class="badge badge-dot mr-4">
                                    <?php 
                                        if($Guru->UserGender == 1){
                                            echo 'Laki-laki';
                                        }     
                                        else{
                                            echo 'Perempuan';
                                        }
                                    ?>
                                </span>
                            </td>
                            <td class="completion">
                                <div class="d-flex align-items-center">
                                    <span class="mr-2"><?php echo $Guru->createdAt;?></span>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#myModal-delete<?php echo $Guru->UserID?>" data-toggle = "modal">Edit</a>
                                        <a class="dropdown-item" href="#myModal-detail<?php echo $Guru->UserID?>" data-toggle = "modal">Detail</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteModal<?php echo $Guru->UserID?>" >Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <div class="modal small fade" id="myModal-detail<?php echo $Guru->UserID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Detail Guru</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <!-- Data Guru -->
                                    <form method="post" action="<?php echo (base_url('C_kelolaGuru/detail/'.$Guru->UserTeacherID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                            <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama : </h4>
                                                    <p name="UserName"><?php echo $Guru->UserName; ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Email : </h4>
                                                    <p name="UserEmail"><?php echo $Guru->UserEmail; ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">NIP : </h4>
                                                    <p name="UserSchoolUnicID"><?php echo $Guru->UserNationalEmployeeID; ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Mata Pelajaran yang Diajar : </h4>
                                                    <p name="UserSchoolUnicID"><?php echo $Guru->MataPelajaran; ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama Sekolah : </h4>
                                                    <p>
                                                        <?php
                                                            echo $Guru->SchoolName;
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Jenis Kelamin : </h4>
                                                        <p><?php if($Guru->UserGender == 1){echo "Laki-laki";} ?></p>
                                                        <p><?php if($Guru->UserGender == 2){echo "Perempuan";} ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>     
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal small fade" id="myModal-delete<?php echo $Guru->UserID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Edit Guru</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <!-- Data Guru -->
                                    <form method="post" action="<?php echo (base_url('C_kelolaGuru/update/'.$Guru->UserID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body">
                                                    <label>Nama</label><br>
                                                    <input type="text" class="form-control" name="UserName" value="<?php echo $Guru->UserName; ?>">
                                                </div>
                                                <div class="box-body">
                                                    <label>Email</label><br>
                                                    <input type="text" class="form-control" name="UserEmail" value="<?php echo $Guru->UserEmail; ?>">
                                                </div>
                                                <div class="box-body">
                                                    <label>NIS</label><br>
                                                    <input type="text" class="form-control" name="UserSchoolUnicID" value="<?php echo $Guru->UserNationalEmployeeID; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Pilih Sekolah</label>
                                                    <select class="form-control" id="SchoolID" name="" <?php if ($this->session->userdata('UserLevel') == 3) { echo 'disabled'; }?>>
                                                        <?php 
                                                            foreach($kelolaGuru as $row){
                                                                if ($this->session->userdata('UserLevel') == 3) {
                                                                    if ($dataSekolah[0]->SchoolID == $row->SchoolID) {
                                                                        echo '<option value="'.$row->SchoolID.'"selected>'.$row->SchoolName.'</option>';
                                                                    } else {
                                                                        echo '<option value="'.$row->SchoolID.'">'.$row->SchoolName.'</option>';
                                                                    }
                                                                } else {
                                                                    echo '<option value="'.$row->SchoolID.'">'.$row->SchoolName.'</option>';
                                                                }
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <input type="hidden" id="DataID<?php echo $Guru->UserID?>" name="SchoolID" />
                                                <script type="text/javascript">
                                                  $(function(){
                                                      var id = $('#SchoolID option:selected').val();
                                                      $('#DataID'+<?php echo $Guru->UserID; ?>).val(id); 
                                                  });   
                                                </script>

                                                <div class="form-group">
                                                    <label for="sel1">Pilih Pelajaran yang diajar 1</label>
                                                    <select class="form-control" name="SubjectID[]" id="<?php echo "pilihan1_".$Guru->UserID; ?>" onchange="hideOption<?php echo $Guru->UserID?>()" required>
                                                        <option value="">
                                                            -- Pilih Pelajaran --
                                                        </option>
                                                            <?php 
                                                                $hasilArray = array();
                                                                $teks = $Guru->MataPelajaranID;
                                                                $pecah = explode(",", $teks);
                                                                for ($i=0; $i < 2; $i++) { 
                                                                     $hasil = $pecah[$i];
                                                                     array_push($hasilArray, $hasil);
                                                                }
                                                                foreach($daftarPelajaran as $row) {
                                                                    ?>
                                                                        <option value="<?php echo ($row->SubjectID) ?>" 
                                                                        <?php if ($row->SubjectID == $hasilArray[0]){echo 'selected';}?>>
                                                                        <?php echo $row->SubjectName ?>
                                                                        </option>";
                                                                    <?php
                                                                }
                                                            ?>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="sel1">Pilih Pelajaran yang diajar 2</label>
                                                    <select class="form-control" name="SubjectID[]" id="<?php echo "pilihan2_".$Guru->UserID; ?>" onchange="hideOption2<?php echo $Guru->UserID?>()">
                                                        <option value="">
                                                            -- Pilih Pelajaran --
                                                        </option>
                                                            <?php 
                                                                $hasilArray = array();
                                                                $teks = $Guru->MataPelajaranID;
                                                                $pecah = explode(",", $teks);
                                                                for ($i=0; $i < 2; $i++) { 
                                                                     $hasil = $pecah[$i];
                                                                     array_push($hasilArray, $hasil);
                                                                }
                                                                foreach($daftarPelajaran as $row) {
                                                                    ?>
                                                                        <option value="<?php echo ($row->SubjectID) ?>" 
                                                                        <?php if ($row->SubjectID == $hasilArray[1]){echo 'selected';}?>>
                                                                        <?php echo $row->SubjectName ?>
                                                                        </option>";
                                                                    <?php
                                                                }
                                                            ?>
                                                    </select>
                                                </div>

                                                <script type="text/javascript">
                                                    function hideOption<?php echo $Guru->UserID?>(){
                                                        var data1 = document.getElementById("pilihan1_<?php echo $Guru->UserID?>");
                                                        var data2 = document.getElementById("pilihan2_<?php echo $Guru->UserID?>");
                                                        var id = data1.selectedIndex;
                                                        if (data1.selectedIndex == id) {
                                                            for (var i = 0; i < data1.length; i++) {
                                                                data2.options[0].selected = true;
                                                                data2.options[i].hidden = false;
                                                            }
                                                            data2.options[id].hidden = true;
                                                        }
                                                    }
                                                    function hideOption2() {
                                                        var data1 = document.getElementById("pilihan1_<?php echo $Guru->UserID?>");
                                                        var data2 = document.getElementById("pilihan2_<?php echo $Guru->UserID?>");
                                                        var id = data2.selectedIndex;
                                                        if (data2.selectedIndex == id) {
                                                            for (var i = 0; i < data2.length; i++) {
                                                                data1.options[i].hidden = false;
                                                            }
                                                            data1.options[id].hidden = true;
                                                        }
                                                    }
                                                </script>

                                                <div class="form-group">
                                                    <h3>Jenis Kelamin</h3>
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input value="1" name="UserGender" class="custom-control-input" id="UserGender1_<?php echo $Guru->UserID?>" type="radio" <?php if($Guru->UserGender == 1){echo "checked";} ?>>
                                                        <label class="custom-control-label" for="UserGender1_<?php echo $Guru->UserID?>">Laki - laki</label>
                                                    </div>
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input value="2" name="UserGender" class="custom-control-input" id="UserGender2_<?php echo $Guru->UserID?>" type="radio"
                                                        <?php if($Guru->UserGender == 2){echo "checked";} ?>>
                                                        <label class="custom-control-label" for="UserGender2_<?php echo $Guru->UserID?>">Perempuan</label>
                                                    </div>
                                                </div>

                                                 <div class="box-body">
                                                    <label>Gambar</label><br>  
                                                    <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="200">
                                                </div>
                                            </div>
                                        </div>                                
                                        <div class="modal-footer">
                                        <button class="btn btn-block btn-success" style="width: 170px">Edit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="deleteModal<?php echo $Guru->UserID?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Guru</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Apakah anda yakin untuk menghapus guru dengan nama <?php echo $Guru->UserName;?> ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo (base_url('C_kelolaGuru/delete/'.$Guru->UserID)) ?>'">Hapus</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal small fade" id="myModal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="myModalLabel">Tambah Guru Sebagai Kontribusi</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="post" action="<?php echo (base_url('C_kelolaGuru/add')) ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="box-body">
                            <label>Nama</label><br>
                            <input type="text" class="form-control" name="UserName">
                        </div>
                        <div class="box-body">
                            <label>Email</label><br>
                            <input type="text" class="form-control" name="UserEmail">
                        </div>
                        <div class="box-body">
                            <label>Password</label><br>
                            <input type="password" class="form-control" name="UserPassword">
                        </div>
                        <div class="box-body">
                            <label>NIP</label><br>
                            <input type="text" class="form-control" name="UserNationalEmployeeID">
                        </div>
                        <div class="form-group">
                            <label for="sel1">Pilih Sekolah</label>
                            <select class="form-control" id="SchoolID" name="" <?php if ($this->session->userdata('UserLevel') == 3) { echo 'disabled'; }?>>
                                <?php 
                                    foreach($kelolaGuru as $row){
                                        if ($this->session->userdata('UserLevel') == 3) {
                                            if ($dataSekolah[0]->SchoolID == $row->SchoolID) {
                                                echo '<option value="'.$row->SchoolID.'"selected>'.$row->SchoolName.'</option>';
                                            } else {
                                                echo '<option value="'.$row->SchoolID.'">'.$row->SchoolName.'</option>';
                                            }
                                        } else {
                                            echo '<option value="'.$row->SchoolID.'">'.$row->SchoolName.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <input type="hidden" id="output" name="SchoolID"/>

                        <div class="form-group">
                            <label for="sel1">Pilih Pelajaran yang diajar 1</label>
                            <select class="form-control" name="SubjectID" id="option1" onchange="myFunction()" <?php if ($this->session->userdata('UserLevel') == 3)?> required>
                                <option value="">
                                    -- Pilih Pelajaran --
                                </option>
                                    <?php 
                                        foreach($daftarPelajaran as $row){
                                            if ($this->session->userdata('UserLevel') == 3) {
                                                if ($dataSekolah[0]->MajorID == $row->SubjectID) {
                                                    echo '<option value="'.$row->SubjectID.'"selected>'.$row->SubjectName.'</option>';
                                                } else {
                                                    echo '<option value="'.$row->SubjectID.'">'.$row->SubjectName.'</option>';
                                                }
                                            } else {
                                                echo '<option value="'.$row->SubjectID.'">'.$row->SubjectName.'</option>';
                                            }
                                        }
                                    ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="sel1">Pilih Pelajaran yang diajar 2</label>
                            <select class="form-control" name="SubjectID2" id="option2" onchange="myFunction2()" <?php if ($this->session->userdata('UserLevel') == 3)?>>
                                <option value="">
                                    -- Pilih Pelajaran --
                                </option>
                                    <?php 
                                        foreach($daftarPelajaran as $row){
                                            if ($this->session->userdata('UserLevel') == 3) {
                                                if ($dataSekolah[0]->MajorID == $row->SubjectID) {
                                                    echo '<option value="'.$row->SubjectID.'"selected>'.$row->SubjectName.'</option>';
                                                } else {
                                                    echo '<option value="'.$row->SubjectID.'">'.$row->SubjectName.'</option>';
                                                }
                                            } else {
                                                echo '<option value="'.$row->SubjectID.'">'.$row->SubjectName.'</option>';
                                            }
                                        }
                                    ?>
                            </select>
                        </div>

                        <script type="text/javascript">
                            function myFunction() {
                                var data1 = document.getElementById("option1");
                                var data2 = document.getElementById("option2");
                                var id = data1.selectedIndex;
                                if (data1.selectedIndex == id) {
                                    for (var i = 0; i < data1.length; i++) {
                                        data2.options[0].selected = true;
                                        data2.options[i].hidden = false;
                                    }
                                    data2.options[id].hidden = true;
                                }
                            }
                            function myFunction2() {
                                var data1 = document.getElementById("option2");
                                var data2 = document.getElementById("option1");
                                var id = data2.selectedIndex;
                                if (data2.selectedIndex == id) {
                                    for (var i = 0; i < data2.length; i++) {
                                        data1.options[i].hidden = false;
                                    }
                                    data1.options[id].hidden = true;
                                }
                            }
                        </script>

                        <div class="form-group">
                            <h3>Jenis Kelamin</h3>
                            <div class="custom-control custom-radio mb-3">
                                <input value="1" name="UserGender" class="custom-control-input" id="UserGender1" type="radio" checked>
                                <label class="custom-control-label" for="UserGender1">Laki - laki</label>
                            </div>
                            <div class="custom-control custom-radio mb-3">
                                <input value="2" name="UserGender" class="custom-control-input" id="UserGender2" type="radio">
                                <label class="custom-control-label" for="UserGender2">Perempuan</label>
                            </div>
                        </div>
                        <div class="box-body">
                            <label>Gambar</label><br>  
                            <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="200">
                        </div>
                    </div>
                </div>                                
                <div class="modal-footer">
                <button class="btn btn-block btn-success" style="width: 170px">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>