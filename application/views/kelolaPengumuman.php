<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaPengumuman')) ?>">Kelola Pengumuman</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" method="post" action="<?php echo (base_url('C_kelolaPengumuman/search')) ?>" enctype="multipart/form-data">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text" name="keyword">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="box-title">
                            <a href="#myModal-delete" class="btn btn-block btn-success" data-base_url = "<?php echo base_url('C_kelolaPengumuman'); ?>" data-toggle = "modal">Tambahkan Pengumuman</a>  
                        </h3>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                Judul Pengumuman
                            </th>
                            <th scope="col">
                                Isi Pengumuman
                            </th>
                            <th scope="col">
                                Nama Sekolah
                            </th>
                            <th scope="col">
                                Gambar
                            </th>
                            <th scope="col">
                                Terakhir Dibuat
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        
                        <?php 
                            foreach ($daftarPengumuman as $Pengumuman) :
                        ?>

                        <tr>
                            <td scope="col">
                                <?php echo $Pengumuman->SchoolAnnoucementTitle?>
                            </td>
                            <td scope="col">
                                <div id="fos">
                                    <p><?php echo substr($Pengumuman->SchoolAnnoucementDesc, 0, 50)?></p>
                                </div>
                            </td>
                            <td scope="col">
                                <?php echo $Pengumuman->SchoolName?>
                            </td>
                            <td scope="col">
                                <div class="media align-items-center">
                                    <a href="<?php echo base_url($Pengumuman->SchoolAnnoucementImage);?>" class="avatar mr-3" style="overflow:hidden;">
                                    <img alt="Image placeholder" src="<?php echo base_url($Pengumuman->SchoolAnnoucementImage);?>" style="border-radius:0px;">
                                    </a>
                                </div>
                            </td>
                            <td scope="col">
                                <?php echo $Pengumuman->createdAt?>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" data-toggle="modal" href="#myModal-update<?php echo $Pengumuman->SchoolAnnoucementID?>" >Edit</a>
                                        <a class="dropdown-item" data-toggle="modal" href="#myModal-detail<?php echo $Pengumuman->SchoolAnnoucementID?>">Detail</a>
                                        <a class="dropdown-item" data-toggle="modal" href="#deleteModal<?php echo $Pengumuman->SchoolAnnoucementID?>" >Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <div class="modal small fade" id="myModal-detail<?php echo $Pengumuman->SchoolAnnoucementID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Detail Pengumuman</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form method="post" action="<?php echo (base_url('C_kelolaPengumuman/detail'.$Pengumuman->SchoolAnnoucementID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Judul Pengumuman : </h4>
                                                    <p name="SubjectName"><?php echo $Pengumuman->SchoolAnnoucementTitle?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Isi Pengumuman : </h4>
                                                    <p name="SubjectName" style="max-width: 310px;padding: 0 10px;"><?php echo $Pengumuman->SchoolAnnoucementDesc?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;"> 
                                                    <h4 style="margin-right:4px;">Nama Sekolah : </h4>
                                                    <p name="SubjectName"><?php echo $Pengumuman->SchoolName?></p>
                                                </div>
                                            </div>
                                        </div>           
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal small fade" id="myModal-update<?php echo $Pengumuman->SchoolAnnoucementID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Edit Pengumuman</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form method="post" action="<?php echo (base_url('C_kelolaPengumuman/update/'.$Pengumuman->SchoolAnnoucementID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body">
                                                        <label>Judul Pengumuman</label><br>
                                                        <input type="text" class="form-control" name="SchoolAnnoucementTitle" value="<?php echo $Pengumuman->SchoolAnnoucementTitle?>">
                                                    </div>
                                                    <div class="box-body">
                                                        <label>Isi Pengumuman</label><br>
                                                        <textarea class="form-control form-control-alternative" name="SchoolAnnoucementDesc"><?php echo $Pengumuman->SchoolAnnoucementDesc?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="sel1">Pilih Nama Sekolah</label>
                                                        <select class="form-control" id="SchoolID" name="SchoolID">
                                                            <?php 
                                                                foreach($daftarSekolah as $row) {
                                                                    ?>
                                                                        <option value="<?php echo ($row->SchoolID) ?>" 
                                                                        <?php if ($row->SchoolID == $Pengumuman->SchoolID){echo 'selected';}?>>
                                                                        <?php echo $row->SchoolName ?>
                                                                        </option>";
                                                                    <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="box-body">
                                                        <label>Gambar</label><br>  
                                                        <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="200">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-block btn-success" style="width: 170px">Edit</button>
                                            </div>
                                        </div>                                
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="deleteModal<?php echo $Pengumuman->SchoolAnnoucementID?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Pengumuman</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Apakah anda yakin untuk menghapus Pengumuman dengan nama <?php echo $Pengumuman->SchoolAnnoucementTitle;?> ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo (base_url('C_kelolaPengumuman/delete/'.$Pengumuman->SchoolAnnoucementID)) ?>'">Hapus</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal small fade" id="myModal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="myModalLabel">Tambah Pengumuman</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="post" action="<?php echo (base_url('C_kelolaPengumuman/add')) ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="box-body">
                            <label>Judul Pengumuman</label><br>
                            <input type="text" class="form-control" name="SchoolAnnoucementTitle">
                        </div>
                        <div class="box-body">
                            <label>Isi Pengumuman</label><br>
                            <textarea class="form-control" name="SchoolAnnoucementDesc"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Pilih Nama Sekolah</label>
                            <select class="form-control" id="SchoolID" name="SchoolID" <?php if ($this->session->userdata('UserLevel') == 3) { echo 'disabled'; }?>>
                                <?php 
                                    foreach($daftarSekolah as $row){
                                        if ($this->session->userdata('UserLevel') == 3) { ?>
                                            <option value="<?php echo ($row->SchoolID) ?>" 
                                                <?php if ($row->SchoolID == $this->session->userdata('UserDetailID')){echo 'selected';}?>>
                                                <?php echo $row->SchoolName ?>
                                            </option>";
                                        <?php } else { ?>
                                            <option value="<?php echo $row->SchoolID;?>"><?php echo $row->SchoolName;?></option>
                                        <?php }
                                    }
                                ?>
                            </select>
                        </div>
                        <input type="hidden" id="output" name="SchoolID"/>
                        <div class="box-body">
                            <label>Gambar</label><br>  
                            <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="200">
                        </div>
                    </div>
                </div>                                
                <div class="modal-footer">
                <button class="btn btn-block btn-success" style="width: 170px">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>