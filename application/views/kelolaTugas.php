<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaTugas')) ?>">Kelola Tugas</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" method="post" action="<?php echo (base_url('C_kelolaTugas/search')) ?>" enctype="multipart/form-data">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text" name="keyword">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="box-title">
                            <a href="<?php echo base_url('C_kelolaTugas/tambah'); ?>" class="btn btn-block btn-success">Tambahkan Tugas</a>  
                        </h3>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center" style="margin-bottom: 2rem;">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                Nama Tugas
                            </th>
                            <th scope="col">
                                Kelas
                            </th>
                            <th scope="col">
                                Pelajaran
                            </th>
                            <th scope="col">
                                Dikumpulkan pada
                            </th>
                            <th scope="col">
                                Dibuat pada
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list p-8">
                        <?php 
                            foreach ($daftarTugas as $Tugas) :
                        ?>
                            <tr>
                                <td scope="col">
                                    <?php echo $Tugas->HomeWorkTitle ?>
                                </td>
                                <td scope="col">
                                    <?php echo $Tugas->ClassName ?>
                                </td>
                                <td scope="col">
                                    <?php echo $Tugas->SubjectName ?>
                                </td>
                                <td scope="col">
                                    <?php echo $Tugas->HomeWorkDate ?>
                                </td>
                                <td scope="col">
                                    <?php echo $Tugas->createdAt ?>    
                                </td>
                                <td class="text-right">
                                    <div class="dropdown">
                                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                            <?php 
                                                if (strtotime($Tugas->HomeWorkDate) < strtotime('now')) {
                                                    echo '<a  href="'.base_url('C_kelolaTugas/nilaiSiswa/'.$Tugas->HomeWorkID).'" class="dropdown-item">Nilai Siswa</a>';
                                                }
                                            ?>
                                            <a class="dropdown-item" href="<?php echo base_url('C_kelolaTugas/jawabanSiswa/'.$Tugas->HomeWorkID) ?>">Jawaban Siswa</a>
                                            <a class="dropdown-item" href="<?php echo base_url('C_kelolaTugas/detail/'.$Tugas->HomeWorkID) ?>">Detail</a>
                                            <a class="dropdown-item">Hapus</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
