<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaSekolah')) ?>">Kelola Guru</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" method="post" action="<?php echo (base_url('C_kelolaSekolah/search')) ?>" enctype="multipart/form-data">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text" name="keyword">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
    <div class="card shadow">
        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="mb-0">Permintaan Sekolah</h2>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">
                            NSS
                        </th>
                        <th scope="col">
                            NPNS
                        </th>
                        <th scope="col">
                            Nama Sekolah
                        </th>
                        <th scope="col">
                            Jenjang Sekolah
                        </th>
                        <th scope="col">
                            Tanggal Dibuat
                        </th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody class="list">

                    <?php 
                        foreach ($daftarSchool as $sekolah) :
                    ?>

                    <tr>
                        <td scope="col">
                            <?php echo $sekolah->SchoolStatisticNumber;?>
                        </td>
                        <td scope="col">
                            <?php echo $sekolah->SchoolNationalUnicID;?>
                        </td>
                        <td scope="col">
                            <?php echo $sekolah->SchoolName;?>
                        </td>
                        <td scope="col">
                            <?php if ($sekolah->SchoolLevelID == 1) { echo 'SD/Sederajat'; } else if ($sekolah->SchoolLevelID == 2) { echo 'SMP/Sederajat'; } else if ($sekolah->SchoolLevelID == 3) { echo 'SMA/Sederajat'; } ;?>
                        </td>
                        <td scope="col">
                            <?php echo $sekolah->createdAt;?>
                        </td>
                        <td scope="col">
                            <button class="btn btn-outline-primary ml-4" type="button" onclick="window.location.href='<?php echo (base_url('C_kelolaSekolah/verifikasi/'.$sekolah->SchoolID)) ?>'">
                                <span class="btn-inner--icon"><i class="ni ni-check-bold"></i></span>
                                
                                <span class="btn-inner--text">Verifikasi</span>
                                
                            </button>
                        </td>
                    </tr>

                    <?php 
                        endforeach;
                    ?>
                </tbody>
            </table>
        </div>
        </div>

        <br><br><br>
        
        <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h2 class="mb-0">Seluruh Sekolah</h2>
                </div>
                <div class="col text-right">
                    <nav class="pagination-nav" aria-label="...">
                        <ul class="pagination">
                            <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">
                                <i class="fa fa-angle-left"></i>
                                <span class="sr-only">Previous</span>
                            </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item active">
                            <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#">
                                <i class="fa fa-angle-right"></i>
                                <span class="sr-only">Next</span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                </div>
              </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                NSS
                            </th>
                            <th scope="col">
                                NPNS
                            </th>
                            <th scope="col">
                                Nama Sekolah
                            </th>
                            <th scope="col">
                                Jenjang Sekolah
                            </th>
                            <th scope="col">
                                Alamat Email Sekolah
                            </th>
                            <th scope="col">
                                Verifikasi
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                    <?php 
                            foreach ($daftarSchool2 as $sekolah2) :
                        ?>

                        <tr>
                            <td scope="col">
                                <?php echo $sekolah2->SchoolStatisticNumber;?>
                            </td>
                            <td scope="col">
                                <?php echo $sekolah2->SchoolNationalUnicID;?>
                            </td>
                            <td scope="col">
                                <?php echo $sekolah2->SchoolName;?>
                            </td>
                            <td scope="col">
                            <?php if ($sekolah2->SchoolLevelID == 1) { echo 'SD/Sederajat'; } else if ($sekolah2->SchoolLevelID == 2) { echo 'SMP/Sederajat'; } else if ($sekolah2->SchoolLevelID == 3) { echo 'SMA/Sederajat'; } ;?>
                            </td>
                            <td scope="col">
                                <?php echo $sekolah2->SchoolEmail;?>
                            </td>
                            <td scope="col">
                                <?php if ($sekolah2->SchoolVerified == 1) { echo 'Sudah'; } else { echo 'Belum'; };?>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#myModal-update<?php echo $sekolah2->SchoolID?>" data-base_url = "<?php echo base_url('C_kelolaKelas'); ?>" data-toggle = "modal">Edit</a>
                                        <a class="dropdown-item" href="#myModal-detail<?php echo $sekolah2->SchoolID?>" data-base_url = "<?php echo base_url('C_kelolaKelas'); ?>" data-toggle = "modal">Detail</a>
                                        <?php if ($sekolah2->SchoolVerified == 1 ) { ?>
                                            <a class="dropdown-item" data-toggle="modal" href="#cancelVerfModal<?php echo $sekolah2->SchoolID?>" >Cancel Verifikasi</a>
                                        <?php }?> 
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteModal<?php echo $sekolah2->SchoolID?>" >Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <div class="main-content">
                            <div class="modal small fade" id="myModal-detail<?php echo $sekolah2->SchoolID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 id="myModalLabel">Detail Sekolah</h3>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <form method="post" action="<?php echo (base_url('C_kelolaSekolah/detail'.$sekolah2->SchoolID)) ?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                        <h4 style="margin-right:4px;">NSS : </h4>
                                                        <p><?php echo ($sekolah2->SchoolStatisticNumber) ?></p>
                                                    </div>
                                                    <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                        <h4 style="margin-right:4px;">NPNS : </h4>
                                                        <p><?php echo ($sekolah2->SchoolNationalUnicID) ?></p>
                                                    </div>
                                                    <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                        <h4 style="margin-right:4px;">School Name : </h4>
                                                        <p ><?php echo ($sekolah2->SchoolName) ?></p>
                                                    </div>
                                                    <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                        <h4 style="margin-right:4px;">School Email : </h4>
                                                        <p><?php echo ($sekolah2->SchoolEmail) ?></p>
                                                    </div>
                                                    <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                        <h4 style="margin-right:4px;">School Address : </h4>
                                                        <p><?php echo ($sekolah2->SchoolAddress) ?></p>
                                                    </div>
                                                    <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Jenjang : </h4>
                                                        <p>
                                                            <?php
                                                                echo $sekolah2->SchoolLevelName;
                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>              
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="main-content">
                            <div class="modal small fade" id="myModal-update<?php echo $sekolah2->SchoolID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h3 id="myModalLabel">Edit Sekolah</h3>
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        </div>
                                        <form method="post" action="<?php echo (base_url('C_kelolaSekolah/update/'.$sekolah2->SchoolID)) ?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <div class="box-body">
                                                        <label>NSS</label><br>
                                                        <input type="text" class="form-control" value="<?php echo ($sekolah2->SchoolStatisticNumber) ?>" name="SchoolStatisticNumber">
                                                    </div>
                                                    <div class="box-body">
                                                        <label>NPNS</label><br>
                                                        <input type="text" class="form-control" value="<?php echo ($sekolah2->SchoolNationalUnicID) ?>" name="SchoolNationalUnicID">
                                                    </div>
                                                    <div class="box-body">
                                                        <label>School Name</label><br>
                                                        <input type="text" class="form-control" value="<?php echo ($sekolah2->SchoolName) ?>" name="SchoolName">
                                                    </div>
                                                    <div class="box-body">
                                                        <label>School Email</label><br>
                                                        <input type="text" class="form-control" value="<?php echo ($sekolah2->SchoolEmail) ?>" name="SchoolEmail">
                                                    </div>
                                                    <div class="box-body">
                                                        <label>School Address</label><br>
                                                        <input type="text" class="form-control" value="<?php echo ($sekolah2->SchoolAddress) ?>" name="SchoolAddress">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="sel1">Pilih Jenjang</label>
                                                        <select class="form-control" id="SchoolLevelID" name="SchoolLevelID">
                                                            <?php
                                                                foreach($kelolaSekolah as $row) {
                                                                    ?>
                                                                        <option value="<?php echo ($row->SchoolLevelID) ?>" 
                                                                        <?php if ($row->SchoolLevelID == $sekolah2->SchoolLevelID){echo 'selected';}?>>
                                                                        <?php echo $row->SchoolLevelName ?>
                                                                        </option>";
                                                                    <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>                                
                                            <div class="modal-footer">
                                                <?php if ($sekolah2->SchoolVerified == 1 ) { ?>
                                                    <button class="btn btn-outline-primary ml-4" type="button" class="dropdown-item" data-toggle="modal" href="#cancelVerfModal<?php echo $sekolah2->SchoolID?>" data-dismiss="modal" aria-hidden="true">
                                                        <span class="btn-inner--icon"><i class="fa fa-close"></i></span>
                                                        <span class="btn-inner--text">Cancel Verifikasi</span>
                                                    </button>
                                                <?php }?> 
                                                <button class="btn btn-block btn-success" style="width: 170px">Ubah</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="deleteModal<?php echo $sekolah2->SchoolID?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Sekolah</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Apakah anda yakin untuk menghapus sekolah dengan nama<?php echo $sekolah2->SchoolName;?> ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo (base_url('C_kelolaSekolah/delete/'.$sekolah2->SchoolID)) ?>'">Hapus</button>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="cancelVerfModal<?php echo $sekolah2->SchoolID?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Akses</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Apakah anda yakin untuk menghapus hak akses sekolah dengan nama <?php echo $sekolah2->SchoolName;?> ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button class="btn btn-outline-primary ml-4" type="button" onclick="window.location.href='<?php echo (base_url('C_kelolaSekolah/Cancelverifikasi/'.$sekolah2->SchoolID)) ?>'">
                                        <span class="btn-inner--icon"><i class="fa fa-close"></i></span>
                                        <span class="btn-inner--text">Cancel Verifikasi</span>
                                    </button>
                                </div>
                                </div>
                            </div>
                        </div>

                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table> 
            </div>
        </div>
    </div>
</div>