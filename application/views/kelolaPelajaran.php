<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaPelajaran')) ?>">Kelola Kelas</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" method="post" action="<?php echo (base_url('C_kelolaPelajaran/search')) ?>" enctype="multipart/form-data">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text" name="keyword">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="box-title">
                            <a href="#myModal-delete" class="btn btn-block btn-success" data-base_url = "<?php echo base_url('C_kelolaPelajaran'); ?>" data-toggle = "modal">Tambahkan Pelajaran</a>  
                        </h3>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                Nama Pelajaran
                            </th>
                            <th scope="col">
                                Dibuat pada
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        
                        <?php 
                            foreach ($daftarPelajaran as $Pelajaran) :
                        ?>

                        <tr>
                            <td scope="col">
                                <div class="media align-items-center">
                                    <!-- <a href="<?php echo base_url($Pelajaran->SubjectImage);?>" class="avatar mr-3" style="overflow:hidden;">
                                    <img alt="Image placeholder" src="<?php echo base_url($Pelajaran->SubjectImage);?>" style="border-radius:0px;">
                                    </a> -->
                                    <div class="media-body">
                                        <span class="mb-0 text-sm"><?php echo $Pelajaran->SubjectName;?></span>
                                    </div>
                                </div>
                            </td>
                            <td scope="col">
                                <?php echo $Pelajaran->createdAt?>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" data-toggle="modal" data-target="#myModal-update<?php echo $Pelajaran->SubjectID?>" >Edit</a>
                                        <a class="dropdown-item" data-toggle="modal" href="#myModal-detail<?php echo $Pelajaran->SubjectID?>">Detail</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteModal<?php echo $Pelajaran->SubjectID?>" >Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <div class="modal small fade" id="myModal-detail<?php echo $Pelajaran->SubjectID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Detail Pelajaran</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form method="post" action="<?php echo (base_url('C_kelolaPelajaran/detail'.$Pelajaran->SubjectID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama Pelajaran : </h4>
                                                    <p name="SubjectName" ><?php echo ($Pelajaran->SubjectName) ?></p>
                                                </div>
                                            </div>
                                        </div>           
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal small fade" id="myModal-update<?php echo $Pelajaran->SubjectID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Edit Pelajaran</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form method="post" action="<?php echo (base_url('C_kelolaPelajaran/update/'.$Pelajaran->SubjectID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body">
                                                    <label>Nama Pelajaran</label><br>
                                                    <input type="text" class="form-control" name="SubjectName" value="<?php echo $Pelajaran->SubjectName?>">
                                                </div>
                                            </div>
                                        </div>              
                                        <div class="modal-body" style="padding-top:0;">
                                            <label>Gambar</label><br>  
                                            <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="200">
                                        </div>                  
                                        <div class="modal-footer">
                                        <button class="btn btn-block btn-success" style="width: 170px">Edit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="deleteModal<?php echo $Pelajaran->SubjectID?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Pelajaran</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Apakah anda yakin untuk menghapus Pelajaran dengan nama <?php echo $Pelajaran->SubjectName;?> ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo (base_url('C_kelolaPelajaran/delete/'.$Pelajaran->SubjectID)) ?>'">Hapus</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal small fade" id="myModal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="myModalLabel">Tambah Pelajaran</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="post" action="<?php echo (base_url('C_kelolaPelajaran/add')) ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="box-body">
                            <label>Nama Pelajaran</label><br>
                            <input type="text" class="form-control" name="SubjectName">
                        </div>
                    </div>
                </div>               
                <div class="modal-body" style="padding-top:0;">
                    <label>Gambar</label><br>  
                    <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="200">
                </div>                     
                <div class="modal-footer">
                <button class="btn btn-block btn-success" style="width: 170px">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>