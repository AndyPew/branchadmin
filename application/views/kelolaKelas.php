<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaKelas')) ?>">Kelola Kelas</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" method="post" action="<?php echo (base_url('C_kelolaKelas/search')) ?>" enctype="multipart/form-data">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text" name="keyword">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="box-title">
                            <a href="#myModal-delete" class="btn btn-block btn-success" data-base_url = "<?php echo base_url('C_kelolaKelas'); ?>" data-toggle = "modal">Tambahkan Kelas</a>  
                        </h3>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                Nama Kelas
                            </th>
                            <th scope="col">
                                Nama Sekolah
                            </th>
                            <th scope="col">
                                Jurusan
                            </th>
                            <th scope="col">
                                Dibuat pada
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        
                        <?php 
                            foreach ($daftarKelas as $Kelas) :
                        ?>

                        <tr>
                            <td scope="col">
                                <?php echo $Kelas->ClassName?>
                            </td>
                            <td scope="col">
                                <?php echo $Kelas->SchoolName?>
                            </td>
                            <td scope="col">
                                <?php echo $Kelas->MajorName?>
                            </td>
                            <td scope="col">
                                <?php echo $Kelas->createdAt?>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#myModal-update<?php echo $Kelas->ClassID?>" data-base_url = "<?php echo base_url('C_kelolaKelas'); ?>" data-toggle = "modal">Edit</a>
                                        <a class="dropdown-item" data-toggle="modal" href="#myModal-detail<?php echo $Kelas->ClassID?>" data-base_url = "<?php echo base_url('C_kelolaKelas'); ?>">Detail</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteModal<?php echo $Kelas->ClassID?>" >Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <div class="modal small fade" id="myModal-detail<?php echo $Kelas->ClassID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Detail Kelas</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form method="post" action="<?php echo (base_url('C_kelolaKelas/detail'.$Kelas->ClassID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama Kelas : </h4>
                                                    <p name="ClassName" ><?php echo ($Kelas->ClassName) ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama Sekolah : </h4>
                                                    <p>
                                                        <?php
                                                            echo $Kelas->SchoolName;
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama Jurusan : </h4>
                                                    <p>
                                                        <?php
                                                            echo $Kelas->MajorName;
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>       
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal small fade" id="myModal-update<?php echo $Kelas->ClassID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Edit Kelas</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form method="post" action="<?php echo (base_url('C_kelolaKelas/update/'.$Kelas->ClassID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body">
                                                    <label>Nama</label><br>
                                                    <input type="text" class="form-control" name="ClassName" value="<?php echo $Kelas->ClassName;?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Pilih Nama Sekolah</label>
                                                    <select class="form-control" id="SchoolID" name="SchoolID" <?php if ($this->session->userdata('UserLevel') == 3) { echo 'disabled'; }?>>
                                                        <?php
                                                            foreach($daftarNamaSekolah as $row) {
                                                                ?>
                                                                    <option value="<?php echo ($row->SchoolID) ?>" 
                                                                    <?php if ($row->SchoolID == $Kelas->SchoolID){echo 'selected';}?>>
                                                                    <?php echo $row->SchoolName ?>
                                                                    </option>";
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <input type="hidden" id="output" name="SchoolID"/>
                                                <div class="form-group">
                                                    <label for="sel1">Pilih Jurusan</label>
                                                    <select class="form-control" id="MajorID" name="MajorID">
                                                        <?php
                                                            foreach($daftarJurusan as $row) {
                                                                ?>
                                                                    <option value="<?php echo ($row->MajorID) ?>" 
                                                                    <?php if ($row->MajorID == $Kelas->MajorID){echo 'selected';}?>>
                                                                    <?php echo $row->MajorName ?>
                                                                    </option>";
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>                                
                                        <div class="modal-footer">
                                        <button class="btn btn-block btn-success" style="width: 170px">Edit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="deleteModal<?php echo $Kelas->ClassID?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Kelas</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Apakah anda yakin untuk menghapus Kelas dengan nama <?php echo $Kelas->ClassName;?> ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo (base_url('C_kelolaKelas/delete/'.$Kelas->ClassID)) ?>'">Hapus</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal small fade" id="myModal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="myModalLabel">Tambah Kelas Sebagai Kontribusi</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="post" action="<?php echo (base_url('C_kelolaKelas/add')) ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="box-body">
                            <label>Nama</label><br>
                            <input type="text" class="form-control" name="ClassName">
                        </div>
                        <div class="form-group">
                            <label for="sel1">Nama Sekolah</label>
                            <select class="form-control" id="SchoolID" name="SchoolID" <?php if ($this->session->userdata('UserLevel') == 3) { echo 'disabled'; }?>>
                                <?php 
                                    foreach($daftarNamaSekolah as $row){
                                        if ($this->session->userdata('UserLevel') == 3) {
                                            if ($dataSekolah[0]->SchoolID == $row->SchoolID) {
                                                echo '<option value="'.$row->SchoolID.'"selected>'.$row->SchoolName.'</option>';
                                            } else {
                                                echo '<option value="'.$row->SchoolID.'">'.$row->SchoolName.'</option>';
                                            }
                                        } else {
                                            echo '<option value="'.$row->SchoolID.'">'.$row->SchoolName.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <input type="hidden" id="SchoolID_INPUT" name="SchoolID"/>
                        <script>
                            $(function(){
                              var id = $('#SchoolID option:selected').val();
                              console.log(id);
                              $('#SchoolID_INPUT').val(id);
                            }); 
                        </script>
                        <div class="form-group">
                            <label for="sel1">Nama Jurusan</label>
                            <select class="form-control" id="MajorID" name="MajorID" <?php if ($this->session->userdata('UserLevel') == 3)?>>
                                <?php 
                                    foreach($daftarNamaJurusan as $row){
                                        if ($this->session->userdata('UserLevel') == 3) {
                                            if ($dataSekolah[0]->MajorID == $row->MajorID) {
                                                echo '<option value="'.$row->MajorID.'"selected>'.$row->MajorName.'</option>';
                                            } else {
                                                echo '<option value="'.$row->MajorID.'">'.$row->MajorName.'</option>';
                                            }
                                        } else {
                                            echo '<option value="'.$row->MajorID.'">'.$row->MajorName.'</option>';
                                        }
                                    }
                                ?>
                                <!-- <?php 
                                    foreach($daftarNamaJurusan as $row){
                                        if ($this->session->userdata('UserLevel') == 3) {
                                            echo '<option value="'.$row->MajorID.'">'.$row->MajorName.'</option>';
                                        } else {
                                            echo '<option value="'.$row->MajorID.'">'.$row->MajorName.'</option>';
                                        }
                                    }
                                ?> -->
                            </select>
                        </div>
                    </div>
                </div>                                
                <div class="modal-footer">
                <button class="btn btn-block btn-success" style="width: 170px">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>