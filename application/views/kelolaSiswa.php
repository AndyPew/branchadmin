<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaSiswa')) ?>">Kelola Siswa</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" method="post" action="<?php echo (base_url('C_kelolaSiswa/search')) ?>" enctype="multipart/form-data">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text" name="keyword">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="box-title">
                            <a href="#myModal-delete" class="btn btn-block btn-success" style="width: 170px" data-base_url = "<?php echo base_url('C_kelolaSiswa'); ?>" data-toggle = "modal">Tambahkan Siswa</a>  
                        </h3>
                    </div>
                    <div class="col text-right">
                        <nav class="pagination-nav" aria-label="...">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                <a class="page-link" href="#" tabindex="-1">
                                    <i class="fa fa-angle-left"></i>
                                    <span class="sr-only">Previous</span>
                                </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active">
                                <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                <a class="page-link" href="#">
                                    <i class="fa fa-angle-right"></i>
                                    <span class="sr-only">Next</span>
                                </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                NIS
                            </th>
                            <th scope="col">
                                Nama
                            </th>
                            <th scope="col">
                                Email
                            </th>
                            <th scope="col">Jenis Kelamin</th>
                            <th scope="col">
                                Terakhir Masuk
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        
                        <?php 
                            foreach ($daftarSiswa as $Siswa) :
                        ?>

                        <tr>
                            <td class="status">
                                <span class="badge badge-dot mr-4">
                                    <?php echo $Siswa->UserSchoolUnicID;?>
                                </span>
                            </td>
                            <th scope="row" class="name">
                                <div class="media align-items-center">
                                    <a href="<?php echo base_url($Siswa->UserProfile);?>" class="avatar mr-3" style="overflow:hidden;">
                                    <img alt="Image placeholder" src="<?php echo base_url($Siswa->UserProfile);?>" style="border-radius:0px;">
                                    </a>
                                    <div class="media-body">
                                        <span class="mb-0 text-sm"><?php echo $Siswa->UserName;?></span>
                                    </div>
                                </div>
                            </th>
                            <td class="budget">
                                <?php echo $Siswa->UserEmail;?>
                            </td>
                            <td class="align-items-center">
                                <span class="badge badge-dot mr-4">
                                    <?php 
                                        if($Siswa->UserGender == 1){
                                            echo 'Laki-laki';
                                        }     
                                        else{
                                            echo 'Perempuan';
                                        }
                                    ?>
                                </span>
                            </td>
                            <td class="completion">
                                <div class="d-flex align-items-center">
                                    <span class="mr-2"><?php echo $Siswa->createdAt;?></span>
                                </div>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="#myModal-delete<?php echo $Siswa->UserID?>" data-base_url = "<?php echo base_url('C_kelolaSiswa'); ?>" data-toggle = "modal" >Edit</a>
                                        <a class="dropdown-item" href="#myModal-update<?php echo $Siswa->UserID?>" data-base_url = "<?php echo base_url('C_kelolaSiswa'); ?>" data-toggle = "modal">Detail</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteModal<?php echo $Siswa->UserID?>" >Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <div class="modal small fade" id="myModal-update<?php echo $Siswa->UserID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Detail Siswa</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <!-- Data Siswa -->
                                    <form method="post" action="<?php echo (base_url('C_kelolaSiswa/detail/'.$Siswa->UserStudentID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama : </h4>
                                                    <p name="UserName"><?php echo $Siswa->UserName; ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Email : </h4>
                                                    <p name="UserEmail"><?php echo $Siswa->UserEmail; ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">NIS : </h4>
                                                    <p name="UserSchoolUnicID"><?php echo $Siswa->UserSchoolUnicID; ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama Sekolah : </h4>
                                                    <p>
                                                        <?php
                                                            echo $Siswa->SchoolName;
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Jenis Kelamin : </h4>
                                                    <p><?php if($Siswa->UserGender == 1){echo "Laki-laki";} ?></p>
                                                    <p><?php if($Siswa->UserGender == 2){echo "Perempuan";} ?></p>
                                                </div>
                                            </div>
                                        </div>          
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal small fade" id="myModal-delete<?php echo $Siswa->UserID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Edit Siswa</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <!-- Data Siswa -->
                                    <form method="post" action="<?php echo (base_url('C_kelolaSiswa/update/'.$Siswa->UserStudentID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body">
                                                    <label>Nama</label><br>
                                                    <input type="text" class="form-control" name="UserName" value="<?php echo $Siswa->UserName; ?>">
                                                </div>
                                                <div class="box-body">
                                                    <label>Email</label><br>
                                                    <input type="text" class="form-control" name="UserEmail" value="<?php echo $Siswa->UserEmail; ?>">
                                                </div>
                                                <div class="box-body">
                                                    <label>NIS</label><br>
                                                    <input type="text" class="form-control" name="UserSchoolUnicID" value="<?php echo $Siswa->UserSchoolUnicID; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Pilih Kelas</label>
                                                    <select class="form-control" id="ClassID" name="ClassID">
                                                        <?php
                                                            foreach($daftarKelas as $row) {
                                                                ?>
                                                                    <option value="<?php echo ($row->ClassID) ?>" 
                                                                    <?php if ($row->ClassID == $Siswa->ClassID){echo 'selected';}?>>
                                                                    <?php echo $row->ClassName ?>
                                                                    </option>";
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <h3>Jenis Kelamin</h3>
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input value="1" name="Gender" class="custom-control-input" id="Gender1" type="radio" <?php if($Siswa->UserGender == 1){echo "checked";} ?> >
                                                        <label class="custom-control-label" for="Gender1">Laki - laki</label>
                                                    </div>
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input value="2" name="Gender" class="custom-control-input" id="Gender2" type="radio" <?php if($Siswa->UserGender == 2){echo "checked";} ?>>
                                                        <label class="custom-control-label" for="Gender2">Perempuan</label>
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <label>Gambar</label><br>  
                                                    <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="200">
                                                </div>
                                            </div>
                                        </div>                                
                                        <div class="modal-footer">
                                        <button class="btn btn-block btn-success" style="width: 170px">Edit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="deleteModal<?php echo $Siswa->UserID?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Siswa</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Apakah anda yakin untuk menghapus Siswa dengan nama <?php echo $Siswa->UserName;?> ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo (base_url('C_kelolaSiswa/delete/'.$Siswa->UserID)) ?>'">Hapus</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal small fade" id="myModal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="myModalLabel">Tambah Siswa Sebagai Kontribusi</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="post" action="<?php echo (base_url('C_kelolaSiswa/add')) ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="box-body">
                            <label>Nama</label><br>
                            <input type="text" class="form-control" name="UserName">
                        </div>
                        <div class="box-body">
                            <label>Email</label><br>
                            <input type="text" class="form-control" name="UserEmail">
                        </div>
                        <div class="box-body">
                            <label>Password</label><br>
                            <input type="password" class="form-control" name="UserPassword">
                        </div>
                        <div class="box-body">
                            <label>NIS</label><br>
                            <input type="text" class="form-control" name="UserSchoolUnicID">
                        </div>
                        <div class="form-group">
                            <label for="sel1">Pilih Kelas</label>
                            <select class="form-control" id="SchoolID" name="ClassID">
                                <?php 
                                    foreach($daftarKelas as $row){
                                        echo '<option value="'.$row->ClassID.'">'.$row->ClassName.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <h3>Jenis Kelamin</h3>
                            <div class="custom-control custom-radio mb-3">
                                <input value="1" name="UserGender" class="custom-control-input" id="UserGender1" type="radio" checked>
                                <label class="custom-control-label" for="UserGender1">Laki - laki</label>
                            </div>
                            <div class="custom-control custom-radio mb-3">
                                <input value="2" name="UserGender" class="custom-control-input" id="UserGender2" type="radio">
                                <label class="custom-control-label" for="UserGender2">Perempuan</label>
                            </div>
                        </div>
                        <div class="box-body">
                            <label>Gambar</label><br>  
                            <input type="file" accept="gambar/*" name="gambar" class="dropify" data-height="200">
                        </div>
                    </div>
                </div>                                
                <div class="modal-footer">
                <button class="btn btn-block btn-success" style="width: 170px">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>

