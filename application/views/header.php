<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if($this->session->userdata('UserLevel')){
  
}else{
  redirect(base_url('C_login'));
}
?><!--

=========================================================
* Argon Dashboard - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8" />
  <title>Branch</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon -->
  <link href="<?php echo base_url('assets/img/BranchRounded.png')?>" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="<?php echo base_url('assets/js/plugins/nucleo/css/nucleo.css')?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css')?>" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="<?php echo base_url('assets/css/argon-dashboard.css?v=1.1.0')?>" rel="stylesheet" />

  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="<?php echo base_url('assets/js/plugins/jquery/dist/jquery.min.js')?>"></script>

  <!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo(base_url('assets/js/Croppie/croppie.css'))?>" type="text/css" />
  <!--===============================================================================================-->
</head>

<body class="">
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="<?php echo base_url('')?>">
        <img src="<?php echo base_url('assets/img/Branch_blue.png')?>" class="navbar-brand-img" alt="...">
      </a>
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="<?php echo base_url('')?>">
                <img src="<?php echo base_url('assets/img/Branch_blue.png')?>">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Navigation -->
        <ul class="navbar-nav">
          <?php if ($this->session->userdata('UserLevel') == 4 || $this->session->userdata('UserLevel') == 3) { ?>
            <li class="nav-item <?php if($page == 'dashboard'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'dashboard'){ echo 'active'; }?>" href="<?php echo base_url('') ?>"> <i class="ni ni-tv-2 text-primary"></i> Dashboard</a>
            </li>
          <?php } ?>
          <?php if ($this->session->userdata('UserLevel') == 4 ) { ?>
            <li class="nav-item <?php if($page == 'kelolaSekolah'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'kelolaSekolah'){ echo 'active'; }?>" href="<?php echo base_url('C_kelolaSekolah') ?>"> <i class="ni ni-briefcase-24 text-primary"></i> Kelola Sekolah</a>
            </li>
          <?php } if ($this->session->userdata('UserLevel') == 4 || $this->session->userdata('UserLevel') == 3) { ?>
            <li class="nav-item <?php if($page == 'kelolaPelajaran'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'kelolaPelajaran'){ echo 'active'; }?>" href="<?php echo base_url('C_kelolaPelajaran') ?>"> <i class="ni ni-book-bookmark text-primary"></i> Kelola Pelajaran</a>
            </li>
          <?php } ?>
          <?php if ($this->session->userdata('UserLevel') == 4 || $this->session->userdata('UserLevel') == 3) { ?>
            <li class="nav-item <?php if($page == 'kelolaJurusan'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'kelolaJurusan'){ echo 'active'; }?>" href="<?php echo base_url('C_kelolaJurusan') ?>"> <i class="ni ni-archive-2 text-primary"></i> Kelola Jurusan</a>
            </li>
          <?php } ?>
          <?php if ($this->session->userdata('UserLevel') == 3) { ?>
            <li class="nav-item <?php if($page == 'kelolaJadwal'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'kelolaJadwal'){ echo 'active'; }?>" href="<?php echo base_url('C_kelolaJadwal') ?>"> <i class="ni ni-book-bookmark text-primary"></i> Kelola Jadwal</a>
            </li>
          <?php } ?>
          <?php if ($this->session->userdata('UserLevel') == 4 || $this->session->userdata('UserLevel') == 3) { ?>
            <li class="nav-item <?php if($page == 'kelolaKelas'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'kelolaKelas'){ echo 'active'; }?>" href=" <?php echo base_url('C_kelolaKelas') ?>"> <i class="ni ni-hat-3 text-primary"></i> Kelola Kelas</a>
            </li>
            <li class="nav-item <?php if($page == 'kelolaGuru'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'kelolaGuru'){ echo 'active'; }?>" href="<?php echo base_url('C_kelolaGuru') ?>"> <i class="ni ni-circle-08 text-primary"></i> Kelola Guru</a>
            </li> 
            </li>
            <li class="nav-item <?php if($page == 'kelolaSiswa'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'kelolaSiswa'){ echo 'active'; }?>" href="<?php echo base_url('C_kelolaSiswa') ?>"> <i class="ni ni-satisfied text-primary"></i> Kelola Siswa</a>
            </li>     
            </li>
            <li class="nav-item <?php if($page == 'kelolaPengumuman'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'kelolaPengumuman'){ echo 'active'; }?>" href="<?php echo base_url('C_kelolaPengumuman') ?>"> <i class="ni ni-bell-55 text-primary"></i> Kelola Pengumuman</a>
            </li> 
          <?php } ?>
          <?php if ($this->session->userdata('UserLevel') == 4 || $this->session->userdata('UserLevel') == 3 || $this->session->userdata('UserLevel') == 2) { ?>
            <li class="nav-item <?php if($page == 'kelolaBuku'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'kelolaBuku'){ echo 'active'; }?>" href="<?php echo base_url('C_kelolaBuku') ?>"> <i class="ni ni-books text-primary"></i> Kelola Buku</a>
            </li>
            <li class="nav-item <?php if($page == 'kelolaTugas'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'kelolaTugas'){ echo 'active'; }?>" href="<?php echo base_url('C_kelolaTugas') ?>"> <i class="ni ni-single-copy-04 text-primary"></i> Kelola Tugas</a>
            </li>  
          <?php } ?>
          <?php if ($this->session->userdata('UserLevel') == 2) { ?>
            <li class="nav-item <?php if($page == 'diskusi'){ echo 'active'; }?>">
              <a class=" nav-link <?php if($page == 'diskusi'){ echo 'active'; }?>" href="<?php echo base_url('C_diskusi') ?>"> <i class="ni ni-book-bookmark text-primary"></i> Diskusi</a>
            </li>
          <?php } ?>
        </ul>
        <!-- Divider -->
        <hr class="my-3">
        <!-- Navigation -->
        <ul class="navbar-nav mb-md-3">
          <li class="nav-item  <?php if($page == 'kelolaProfil'){ echo 'active'; }?>">
            <a class="nav-link <?php if($page == 'kelolaProfil'){ echo 'active'; }?>" href="<?php echo base_url('C_kelolaProfil') ?>">
              <i class="ni ni-single-02"></i> Edit Profile
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('Main/logout') ?>">
              <i class="ni ni-user-run"></i> Log Out
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>