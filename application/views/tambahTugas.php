<div id="FormSoal" class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="">Tambah Tugas</a>
        <!-- Form -->
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <form method="post" action="<?php echo base_url('C_kelolaTugas/tambahTugas') ?>" enctype="multipart/form-data">
            <div class="card shadow">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="box-body">
                            <label>Judul Tugas</label><br>
                            <input type="text" class="form-control" name="title" placeholder="Tugas Minggu Pertama..." required>
                            <input type="hidden" name="banyakSoal" :value="banyakSoal">
                            <input type="hidden" name="banyakJawaban" :value="banyakJawaban">
                            <input type="hidden" name="userID" value="<?php echo $this->session->userdata('UserID') ?>">
                        </div>
                        <br>
                        <div class="box-body">
                            <label>Deskripsi Tugas</label><br>
                            <textarea type="text" class="form-control form-control-alternative" style="box-shadow: none; border: 1px solid #cad1d7; min-height: 100px" name="desc" rows="5" placeholder="Deskripsi Tugas..." required></textarea>
                        </div>
                        <br>
                        <div class="box-body">
                            <label>Hari Tugas Dikumpulkan</label><br>
                            <input type="datetime-local" class="form-control" name="date" required>
                        </div>
                        <br>
                        <div class="box-body">
                            <label for="sel1">Pilih Kelas</label>
                            <select class="form-control" id="SchoolID" name="classID">
                                <?php 
                                    foreach($daftarKelas as $row){
                                        echo '<option value="'.$row->ClassID.'">'.$row->ClassName.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <br>
                        <div class="box-body">
                            <label for="sel1">Pilih Pelajaran</label>
                            <select class="form-control" id="SchoolID" name="subjectID">
                                <?php 
                                    foreach($daftarPelajaran as $row){
                                        echo '<option value="'.$row->SubjectID.'">'.$row->SubjectName.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <br>
                        <div class="box-body">
                            <label for="sel1">Jumlah Jawaban</label>
                            <select class="form-control" v-model="banyakJawaban" name="banyakJawaban">
                                <option value="2">2</option>;
                                <option value="3">3</option>;
                                <option value="4">4</option>;
                                <option value="5">5</option>;
                            </select>
                        </div>
                        <br>
                        <div class="box-body">
                            <button class="btn btn-block btn-primary">Upload Tugas</button>
                        </div>
                        <p class="mt-4" style="text-align: center">tugas akan langsung diberikan kepada siswa saat anda menguploadnya</p>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="card shadow mt-4" v-for="(index) in banyakSoal">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="box-body">
                                <h2>Pertanyaan No {{index}}</h2>     
                                <a v-if="index == banyakSoal && index != 1" @click="kurangiSoal()" style="position: absolute; top: 12px; right: 20px; font-size: 24px; cursor: pointer;"><span aria-hidden="true">&times;</span></a>
                                <textarea type="text" class="form-control form-control-alternative" style="box-shadow: none; border: 1px solid #cad1d7; min-height: 40px" :name="'Pertanyaan' + index" rows="1" placeholder="Pertanyaan disini..." required></textarea>
                            </div>
                            <br>
                            <label>Jawaban</label><br>
                            <div class="box-body custom-control custom-radio">
                                <div v-for="(index2) in parseInt(banyakJawaban, 10)">
                                    <br>
                                    <div class="row" style="margin: 0">
                                        <input :value="index2" :name="'answerofquestion' + index" class="custom-control-input" :id="'anwerofquestion' + index + index2" type="radio" required>
                                        <label v-if="index2 == 1" class="custom-control-label" style="width: 28px" :for="'anwerofquestion' + index + index2">A.</label>
                                        <label v-if="index2 == 2" class="custom-control-label" style="width: 28px" :for="'anwerofquestion' + index + index2">B.</label>
                                        <label v-if="index2 == 3" class="custom-control-label" style="width: 28px" :for="'anwerofquestion' + index + index2">C.</label>
                                        <label v-if="index2 == 4" class="custom-control-label" style="width: 28px" :for="'anwerofquestion' + index + index2">D.</label>
                                        <label v-if="index2 == 5" class="custom-control-label" style="width: 28px" :for="'anwerofquestion' + index + index2">E.</label>
                                        <div style="width: calc(100% - 28px)">
                                            <textarea type="text" class="form-control form-control-alternative" style="box-shadow: none; border: 1px solid #cad1d7; min-height: 40px" :name="'Jawaban' + index +',' + index2" rows="1" placeholder="Jawaban disini..." required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-block btn-outline-primary" @click="tambahSoal()">Tambahkan Soal</button>
            </div>
        </form>
    </div>
</div>

<script>
var vo = new Vue({
    el: '#FormSoal',
    name: 'FormSoal',
    data() {
        return {
            banyakSoal: 1,
            banyakJawaban: 4,
        }
    },
    methods: {
        tambahSoal() {
            this.banyakSoal += 1
        },
        kurangiSoal() {
            this.banyakSoal -= 1
        },
    }
});
</script>
