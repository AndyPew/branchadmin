<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaBuku')) ?>">Kelola Buku</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" method="post" action="<?php echo (base_url('C_kelolaBuku/search')) ?>" enctype="multipart/form-data">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text" name="keyword">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="box-title">
                            <a href="#myModal-delete" class="btn btn-block btn-success" data-base_url = "<?php echo base_url('C_kelolaBuku'); ?>" data-toggle = "modal">Tambahkan Buku</a>  
                        </h3>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                Nama Buku
                            </th>
                            <th scope="col">
                                Nama Pengirim
                            </th>
                            <th scope="col">
                                Untuk Kelas
                            </th>
                            <th scope="col">
                                Pelajaran
                            </th>
                            <th scope="col">
                                Dibuat pada
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        
                        <?php 
                            foreach ($daftarBuku as $Buku) :
                        ?>

                        <tr>
                            <td scope="col">
                                <?php echo $Buku->BookTitle?>
                            </td>
                            <td scope="col">
                                <?php echo $Buku->UserName?>
                            </td>
                            <td scope="col">
                                <?php echo $Buku->ClassName?>
                            </td>
                            <td scope="col">
                                <?php echo $Buku->SubjectName?>
                            </td>
                            <td scope="col">
                                <?php echo $Buku->createdAt?>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <object data="<?php echo $Buku->BookFileUrl?>" type="pdf" width="100%" height="100%" style="margin-left: 15px;">
                                        <a href="<?php echo $Buku->BookFileUrl?>">Preview</a>
                                    </object>
                                        <a class="dropdown-item" href="#myModal-update<?php echo $Buku->BookID?>" data-base_url = "<?php echo base_url('C_kelolaBuku'); ?>" data-toggle = "modal">Edit</a>
                                        <a class="dropdown-item" data-toggle="modal" href="#myModal-detail<?php echo $Buku->BookID?>" data-base_url = "<?php echo base_url('C_kelolaBuku'); ?>">Detail</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteModal<?php echo $Buku->BookID?>" >Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        

                        <div class="modal small fade" id="myModal-detail<?php echo $Buku->BookID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Detail Buku</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form method="post" action="<?php echo (base_url('C_kelolaBuku/detail'.$Buku->BookID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama Buku : </h4>
                                                    <p><?php echo ($Buku->BookTitle) ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama Pengirim : </h4>
                                                    <p><?php echo ($Buku->UserName) ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Untuk Kelas : </h4>
                                                    <p>
                                                        <?php
                                                            echo $Buku->ClassName;
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>       
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal small fade" id="myModal-update<?php echo $Buku->BookID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Edit Buku</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form method="post" action="<?php echo (base_url('C_kelolaBuku/update/'.$Buku->BookID)) ?>" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <div class="box-body">
                                                <label>Nama Buku</label><br>
                                                <input type="text" class="form-control" name="BookTitle" value="<?php echo $Buku->BookTitle;?>">
                                            </div>
                                            <!-- <div class="box-body">
                                                <label>File Buku</label><br>  
                                                <input type="file" accept=".pdf" name="gambar" class="dropify" data-height="200" value="<?php echo $Buku->BookFileUrl;?>">
                                            </div> -->
                                            <div class="form-group">
                                                <label for="sel1">Pilih Kelas</label>
                                                <select class="form-control" id="ClassID" name="ClassID">
                                                    <?php
                                                        foreach($daftarKelas as $row) {
                                                            ?>
                                                                <option value="<?php echo ($row->ClassID) ?>" 
                                                                <?php if ($row->ClassID == $Buku->ClassID){echo 'selected';}?>>
                                                                <?php echo $row->ClassName ?>
                                                                </option>";
                                                            <?php
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="sel1">Pilih Pelajaran</label>
                                                <select class="form-control" id="SubjectID" name="SubjectID">
                                                    <?php
                                                        foreach($daftarPelajaran as $row) {
                                                            ?>
                                                                <option value="<?php echo ($row->SubjectID) ?>" 
                                                                <?php if ($row->SubjectID == $Buku->SubjectID){echo 'selected';}?>>
                                                                <?php echo $row->SubjectName ?>
                                                                </option>";
                                                            <?php
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>                                   
                                    <div class="modal-footer">
                                        <button class="btn btn-block btn-success" style="width: 170px">Edit</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="deleteModal<?php echo $Buku->BookID?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Buku</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Apakah anda yakin untuk menghapus Buku dengan nama <?php echo $Buku->BookTitle;?> ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo (base_url('C_kelolaBuku/delete/'.$Buku->BookID)) ?>'">Hapus</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> -->

<div class="modal small fade" id="myModal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="myModalLabel">Tambah Buku Sebagai Kontribusi</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="post" action="<?php echo (base_url('C_kelolaBuku/add')) ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="box-body">
                            <label>Nama Buku</label><br>
                            <input type="text" class="form-control" name="BookTitle">
                        </div>
                        <div class="box-body">
                            <label>File Buku</label><br>  
                            <input type="file" accept=".pdf" name="gambar" class="dropify" data-height="200" required>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Pilih Kelas</label>
                            <select class="form-control" id="ClassID" name="ClassID">
                                <?php 
                                    foreach($daftarKelas as $row){
                                        echo '<option value="'.$row->ClassID.'">'.$row->ClassName.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sel1">Pilih Pelajaran</label>
                            <select class="form-control" id="SubjectID" name="SubjectID">
                                <?php 
                                    foreach($daftarPelajaran as $row){
                                        echo '<option value="'.$row->SubjectID.'">'.$row->SubjectName.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>                                
                <div class="modal-footer">
                <button class="btn btn-block btn-success" style="width: 170px">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>