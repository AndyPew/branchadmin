<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="<?php echo (base_url('C_kelolaJurusan')) ?>">Kelola Jurusan</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" method="post" action="<?php echo (base_url('C_kelolaJurusan/search')) ?>" enctype="multipart/form-data">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text" name="keyword">
            </div>
          </div>
        </form>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="main-con">
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="box-title">
                            <a href="#myModal-delete" class="btn btn-block btn-success" data-base_url = "<?php echo base_url('C_kelolaJurusan'); ?>" data-toggle = "modal">Tambahkan Jurusan</a>  
                        </h3>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table align-items-center">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">
                                Nama
                            </th>
                            <th scope="col">
                                Jenjang Sekolah
                            </th>
                            <th scope="col">
                                Dibuat pada
                            </th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody class="list">
                        
                        <?php 
                            foreach ($daftarJurusan as $Jurusan) :
                        ?>

                        <tr>
                            <td scope="col">
                                <?php echo $Jurusan->MajorName?>
                            </td>
                            <td scope="col">
                            <?php if ($Jurusan->SchoolLevelID == 1) { echo 'SD/Sederajat'; } else if ($Jurusan->SchoolLevelID == 2) { echo 'SMP/Sederajat'; } else if ($Jurusan->SchoolLevelID == 3) { echo 'SMA/Sederajat'; } ;?>
                            </td>
                            <td scope="col">
                                <?php echo $Jurusan->createdAt?>
                            </td>
                            <td class="text-right">
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" data-toggle="modal" data-target="#myModal-update<?php echo $Jurusan->MajorID?>" >Edit</a>
                                        <a class="dropdown-item" data-toggle="modal" href="#myModal-detail<?php echo $Jurusan->MajorID?>">Detail</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deleteModal<?php echo $Jurusan->MajorID?>" >Hapus</a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <div class="modal small fade" id="myModal-detail<?php echo $Jurusan->MajorID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Detail Jurusan</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form method="post" action="<?php echo (base_url('C_kelolaJurusan/detail'.$Jurusan->MajorID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Nama jurusan : </h4>
                                                    <p name="MajorName" ><?php echo ($Jurusan->MajorName) ?></p>
                                                </div>
                                                <div class="box-body" style="display: flex;margin-bottom: 12px;">
                                                    <h4 style="margin-right:4px;">Jenjang : </h4>
                                                    <p>
                                                        <?php
                                                            echo $Jurusan->SchoolLevelName;
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>                
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal small fade" id="myModal-update<?php echo $Jurusan->MajorID?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 id="myModalLabel">Edit Jurusan</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form method="post" action="<?php echo (base_url('C_kelolaJurusan/update/'.$Jurusan->MajorID)) ?>" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <div class="box-body">
                                                    <label>Nama</label><br>
                                                    <input type="text" class="form-control" name="MajorName" value="<?php echo $Jurusan->MajorName?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Pilih Jenjang Sekolah</label>
                                                    <select class="form-control" id="SchoolLevelID" name="SchoolLevelID">
                                                        <?php
                                                            foreach($daftarJenjang as $row) {
                                                                ?>
                                                                    <option value="<?php echo ($row->SchoolLevelID) ?>" 
                                                                    <?php if ($row->SchoolLevelID == $Jurusan->SchoolLevelID){echo 'selected';}?>>
                                                                    <?php echo $row->SchoolLevelName ?>
                                                                    </option>";
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>                                
                                        <div class="modal-footer">
                                        <button class="btn btn-block btn-success" style="width: 170px">Edit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="deleteModal<?php echo $Jurusan->MajorID?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Jurusan</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Apakah anda yakin untuk menghapus Jurusan dengan nama <?php echo $Jurusan->MajorName;?> ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo (base_url('C_kelolaJurusan/delete/'.$Jurusan->MajorID)) ?>'">Hapus</button>
                                </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                            endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal small fade" id="myModal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="myModalLabel">Tambah Jurusan Sebagai Kontribusi</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="post" action="<?php echo (base_url('C_kelolaJurusan/add')) ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="box-body">
                            <label>Nama</label><br>
                            <input type="text" class="form-control" name="MajorName">
                        </div>
                        <div class="form-group">
                            <label for="sel1">Pilih Jenjang Sekolah</label>
                            <select class="form-control" id="SchoolID" name="SchoolLevelID">
                                <?php 
                                    foreach($daftarJenjang as $row){
                                        echo '<option value="'.$row->SchoolLevelID.'">'.$row->SchoolLevelName.'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>                                
                <div class="modal-footer">
                <button class="btn btn-block btn-success" style="width: 170px">Tambahkan</button>
                </div>
            </form>
        </div>
    </div>
</div>