<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaSiswa extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('M_kelolaSiswa');
        if($this->session->userdata('UserLevel') == 2){
          redirect('');
        } 
  }

	public function index()
  {
    $data["daftarKelas"]= $this->M_kelolaSiswa->getClassName();

    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaSiswa->getDataSekolah($this->session->userdata('UserID'));
      $data["daftarSiswa"] = $this->M_kelolaSiswa->getAllSiswaByID($data["dataSekolah"][0]->SchoolID); 
      $data["daftarKelas"]= $this->M_kelolaSiswa->getClassNameBySchool($data["dataSekolah"][0]->SchoolID);
    } else{
      $data["daftarSiswa"]= $this->M_kelolaSiswa->getAllSiswa();
    }
     
    $main_content['page'] = 'kelolaSiswa';
    $this->load->view('header', $main_content);
    $this->load->view('kelolaSiswa', $data);
    $this->load->view('footer');    
  }

  public function add()
  {
    $this->form_validation->set_rules('UserName', 'text', 'required');
    $this->form_validation->set_rules('UserEmail', 'text', 'required');
    $this->form_validation->set_rules('UserPassword', 'text', 'required');
    $this->form_validation->set_rules('UserSchoolUnicID', 'text', 'required');
    $this->form_validation->set_rules('ClassID', 'text', 'required');
    $this->form_validation->set_rules('UserGender', 'text', 'required');
      
      
      $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
      <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
      if ($this->form_validation->run() == TRUE) {

        $pengacak = "p3ng4c4k";
        $UserPassword1 = MD5($this->input->post('UserPassword'));
        $UserPassword = md5($pengacak . md5($UserPassword1));

        $gambar='';
        $nmfile = "profile_".time();
        $config['upload_path'] = './gambar/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $nmfile;

        $this->load->library('upload', $config);
        if($this->upload->do_upload('gambar')){
        
            $upload_data = $this->upload->data();
            $file_name = 'gambar/'.$upload_data['file_name'];
            $this->M_kelolaSiswa->addSiswa($file_name, $UserPassword);
            
        }else{
            $this->M_kelolaSiswa->addSiswa_noImage($UserPassword);
        }
        
            
        echo "berhasil upload";
        $this->session->set_flashdata('create_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Siswa Berhasil ditambahkan</div>');
        redirect(base_url('C_kelolaSiswa'));
        
      }else{
          echo "gagal";
      }            

    $main_content['conten'] = $this->load->view('kelolaSiswa', true);
    $main_content['page'] = 'kelolaSiswa';
    $this->load->view('header', 'footer' , $main_content);
  }

  public function update($id)
  {
    $this->M_kelolaSiswa->updateSiswa($id);
    redirect('C_kelolaSiswa');
  }

  public function delete($ID) {
    $this->M_kelolaSiswa->deleteSiswa($ID);
    redirect(base_url('C_kelolaSiswa'));
  }

  public function detail($ID = null)
  {
    $this->M_kelolaSiswa->getStudentByID($ID);
  }

  public function search()
  {
    $keyword = $this->input->post('keyword');
    $data["daftarKelas"]= $this->M_kelolaSiswa->getClassName();

    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaSiswa->getDataSekolah($this->session->userdata('UserID'));
      if ($keyword == "") {
        $data["daftarSiswa"] = $this->M_kelolaSiswa->getAllSiswaByID($data["dataSekolah"][0]->SchoolID); 
      }else{
        $data["daftarSiswa"] = $this->M_kelolaSiswa->searchData_lvl3($keyword, $data["dataSekolah"][0]->SchoolID); 
      }
      
    } else{
      $data["daftarSiswa"]= $this->M_kelolaSiswa->searchData($keyword);
    }
    $main_content['page'] = 'kelolaSiswa';
    $this->load->view('header', $main_content);
    $this->load->view('kelolaSiswa', $data);
    $this->load->view('footer');
  }
}
