<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaBuku extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('M_kelolaBuku');
  }

	public function index()
  {
    $data["daftarKelas"]= $this->M_kelolaBuku->getAllKelas();
    $data["daftarBuku"]= $this->M_kelolaBuku->getAllBuku();
    $data["daftarPelajaran"]=$this->M_kelolaBuku->getAllPelajaran();

    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaBuku->getDataSekolah($this->session->userdata('UserID'));
      $data["daftarPelajaran"] = $this->M_kelolaBuku->getAllPelajaranSekolah($data["dataSekolah"][0]->SchoolID);
      $data["daftarKelas"]= $this->M_kelolaBuku->getAllKelasSekolah($data["dataSekolah"][0]->SchoolID);
    } else if($this->session->userdata('UserLevel') == 2){
      $data["daftarPelajaran"] = $this->M_kelolaBuku->getAllPelajaranSekolahByGuru($this->session->userdata('UserID'));
    }

    $main_content['page'] = 'kelolaBuku';
    $this->load->view('header', $main_content);
    $this->load->view('kelolaBuku', $data);
    $this->load->view('footer');
  }

  public function add()
  {       

      $this->form_validation->set_rules('BookTitle', 'text', 'required');
      $this->form_validation->set_rules('ClassID', 'text', 'required');
      
      $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
      <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
      if ($this->form_validation->run() == TRUE) {

        $gambar='';
        $nmfile = "profile_".time();
        $config['upload_path'] = './buku/';
        $config['allowed_types'] = 'pdf|word|ppt';
        $config['file_name'] = $nmfile;

        $this->load->library('upload', $config);
        if($this->upload->do_upload('gambar')){
        
            $upload_data = $this->upload->data();
            $file_name = 'buku/'.$upload_data['file_name'];
            
            $this->M_kelolaBuku->addBuku($file_name);
        }else{
            $this->M_kelolaBuku->addBuku_noFile();
        }
        
        echo "berhasil upload";
        $this->session->set_flashdata('create_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Buku Berhasil ditambahkan</div>');
        redirect(base_url('C_kelolaBuku'));
        
      }else{
          echo "gagal";
      }            
  }

  public function update($id)
  {
    $this->M_kelolaBuku->updateBuku($id);
    redirect('C_kelolaBuku');
  }

  public function delete($ID) {
    $this->M_kelolaBuku->deleteBuku($ID);
    redirect(base_url('C_kelolaBuku'));
  }

  public function detail($ID = null){
    $this->M_kelolaBuku->getBukuByID($ID);
  }

  public function search()
  {
    $keyword = $this->input->post('keyword');

    $data["daftarKelas"]= $this->M_kelolaBuku->getAllKelas();
    $data["daftarBuku"]= $this->M_kelolaBuku->searchData($keyword);
    $main_content['page'] = 'kelolaBuku';
    $this->load->view('header', $main_content);
    $this->load->view('kelolaBuku', $data);
    $this->load->view('footer');
  }
}
