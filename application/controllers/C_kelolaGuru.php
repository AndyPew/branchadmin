<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaGuru extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('M_kelolaGuru');
        if($this->session->userdata('UserLevel') == 2){
          redirect('');
        } 
  }

	public function index()
  {
    $data["kelolaGuru"]= $this->M_kelolaGuru->getSchoolName();
    $data["daftarGuru"]= $this->M_kelolaGuru->getAllGuru();
    $data["daftarPelajaran"]= $this->M_kelolaGuru->getAllSubject();

    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaGuru->getDataSekolah($this->session->userdata('UserID'));
      $data["daftarGuru"]= $this->M_kelolaGuru->getAllGuruByID($data["dataSekolah"][0]->SchoolID);
      $data["daftarPelajaran"]= $this->M_kelolaGuru->getAllSubjectSekolah($data["dataSekolah"][0]->SchoolID);
    }
    
    $main_content['page'] = 'kelolaGuru';
    $this->load->view('header', $main_content);
    $this->load->view('kelolaGuru', $data);
    $this->load->view('footer');
  }

  public function add()
  {
    $this->form_validation->set_rules('UserName', 'text', 'required');
    $this->form_validation->set_rules('UserEmail', 'text', 'required');
    $this->form_validation->set_rules('UserPassword', 'text', 'required');
    $this->form_validation->set_rules('UserNationalEmployeeID', 'text', 'required');
    $this->form_validation->set_rules('SchoolID', 'text', 'required');
    $this->form_validation->set_rules('UserGender', 'text', 'required');
      
      
      $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
      <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');

      if ($this->form_validation->run() == TRUE) {

        $pengacak = "p3ng4c4k";
        $UserPassword1 = MD5($this->input->post('UserPassword'));
        $UserPassword = md5($pengacak . md5($UserPassword1));

        $gambar='';
        $nmfile = "profile_".time();
        $config['upload_path'] = './gambar/';
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['file_name'] = $nmfile;

        $this->load->library('upload', $config);
        if($this->upload->do_upload('gambar')){
        
            $upload_data = $this->upload->data();
            $file_name = 'gambar/'.$upload_data['file_name'];

            $this->M_kelolaGuru->addGuru($file_name, $UserPassword);
            $id_guru = $this->db->insert_id();
            $this->M_kelolaGuru->addTeacherSubject($id_guru);
        }else{
            $this->M_kelolaGuru->addGuru_noImage($UserPassword);
            $id_guru = $this->db->insert_id();
            $this->M_kelolaGuru->addTeacherSubject($id_guru);
        }
        
        echo "berhasil upload";
        $this->session->set_flashdata('create_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Guru Berhasil ditambahkan</div>');
        redirect(base_url('C_kelolaGuru'));
        
      }else{
          echo "gagal";
      }            

    // $main_content['conten'] = $this->load->view('kelolaGuru', true);
    // $main_content['page'] = 'kelolaGuru';
    // $this->load->view('header', 'footer' , $main_content);
  }

  public function update($id)
  {
    $this->M_kelolaGuru->updateTeacherSubject($id);
    $this->M_kelolaGuru->updateGuru($id);
    redirect('C_kelolaGuru');
  }

  public function delete($ID) 
  {
    $this->M_kelolaGuru->deleteGuru($ID);
    redirect(base_url('C_kelolaGuru'));
  }

  public function search()
  {
    $keyword = $this->input->post('keyword');    
    $data["kelolaGuru"]= $this->M_kelolaGuru->getSchoolName();
    $data["daftarGuru"]= $this->M_kelolaGuru->searchData($keyword);
    $data["daftarPelajaran"]= $this->M_kelolaGuru->getAllSubject();
    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaGuru->getDataSekolah($this->session->userdata('UserID'));
      if ($keyword == "") {
        $data["daftarGuru"]= $this->M_kelolaGuru->getAllGuruByID($data["dataSekolah"][0]->SchoolID);
      }else{
        $data["daftarGuru"]= $this->M_kelolaGuru->searchData_lvl3($keyword, $data["dataSekolah"][0]->SchoolID);
      }
      $data["daftarPelajaran"]= $this->M_kelolaGuru->getAllSubjectSekolah($data["dataSekolah"][0]->SchoolID);
    }
    
    $main_content['page'] = 'kelolaGuru';
    $this->load->view('header', $main_content);
    $this->load->view('kelolaGuru', $data);
    $this->load->view('footer');
  }

  // public function detail($ID = null){
  //   $data = $this->M_kelolaGuru->getTeacherByID($ID);
  //   echo json_encode($data);
  //   exit();
  // }
}
