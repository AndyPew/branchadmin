<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
        $this->load->model('M_login');
        if ($this->session->userdata('UserLevel') == '1') {
            redirect('C_login');
        }
    }
	public function index()
	{
		$data['page'] = 'dashboard';
		$this->load->view('header', $data);
    	$this->load->view('dashboard');
		$this->load->view('footer');
	}

	public function logout(){
		$this->session->unset_userdata('user');
		delete_cookie('branch');
		$this->session->sess_destroy();
		redirect(base_url('C_login'));
	}
}
