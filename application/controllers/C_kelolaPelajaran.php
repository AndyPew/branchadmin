<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaPelajaran extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('M_kelolaPelajaran');
        if($this->session->userdata('UserLevel') == 2){
          redirect('');
        }
  }

	public function index(){
    $data["daftarPelajaran"]= $this->M_kelolaPelajaran->getAllPelajaran();
    $main_content['page'] = 'kelolaPelajaran';
    $this->load->view('header', $main_content);
    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaPelajaran->getDataSekolah($this->session->userdata('UserID'));
      $data["daftarPelajaran"]= $this->M_kelolaPelajaran->getAllPelajaranSekolah($data["dataSekolah"][0]->SchoolID);
      $data["daftarPelajaran2"]= $this->M_kelolaPelajaran->getAllJurusan();
      $this->load->view('kelolaPelajaranSekolah', $data);
    } else {
      $this->load->view('kelolaPelajaran', $data);
    }
    $this->load->view('footer');
  }

  public function add(){       

    $this->form_validation->set_rules('SubjectName', 'text', 'required');
    $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
    <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
    if ($this->form_validation->run() == TRUE) {

      $gambar='';
      $nmfile = "profile_".time();
      $config['upload_path'] = './gambar/';
      $config['allowed_types'] = 'jpg|jpeg|png';
      $config['file_name'] = $nmfile;

      $this->load->library('upload', $config);
      if($this->upload->do_upload('gambar')){
      
          $upload_data = $this->upload->data();
          $file_name = 'gambar/'.$upload_data['file_name'];
          $this->M_kelolaPelajaran->addPelajaran($file_name);
          
      }else{
        $this->M_kelolaPelajaran->addPelajaranNoImg();
      }
          
      echo "berhasil upload";
      $this->session->set_flashdata('create_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Jurusan Berhasil ditambahkan</div>');
      redirect(base_url('C_kelolaPelajaran'));
      
    }else{
        echo "gagal";
    } 
    $main_content['conten'] = $this->load->view('kelolaPelajaran', true);
    $main_content['page'] = 'kelolaPelajaran';
    $this->load->view('header', 'footer' , $main_content);
  }

  public function addPelajaranSekolah(){       

    $this->form_validation->set_rules('SchoolID', 'text', 'required');
    $this->form_validation->set_rules('SubjectID', 'text', 'required');
    $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
    <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
    if ($this->form_validation->run() == TRUE) {
      
      $this->M_kelolaPelajaran->addPelajaranSekolah();
          
      echo "berhasil upload";
      $this->session->set_flashdata('create_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Pelajaran Berhasil ditambahkan</div>');
      redirect(base_url('C_kelolaPelajaran'));
      
    }else{
        echo "gagal";
    } 
    $main_content['conten'] = $this->load->view('kelolaPelajaranSekolah', true);
    $main_content['page'] = 'kelolaPelajaranSekolah';
    $this->load->view('header', 'footer' , $main_content);
  }

  public function update($id)
  {
    $gambar='';
    $nmfile = "profile_".time();
    $config['upload_path'] = './gambar/';
    $config['allowed_types'] = 'jpg|jpeg|png';
    $config['file_name'] = $nmfile;

    $this->load->library('upload', $config);
    if($this->upload->do_upload('gambar')){
    
        $upload_data = $this->upload->data();
        $file_name = 'gambar/'.$upload_data['file_name'];

        $this->M_kelolaPelajaran->updatePelajaran($id, $file_name);
        echo "with gambar";
    }else{
        $this->M_kelolaPelajaran->updatePelajaranNoImg($id);
        echo "tanpa gambar";
    }

    // var_dump($this->db->last_query());exit();
    redirect('C_kelolaPelajaran');
  }

  public function delete($ID) {
    $this->M_kelolaPelajaran->deletePelajaran($ID);
    redirect(base_url('C_kelolaPelajaran'));
  }

  public function deletePelajaranSekolah($ID) {
    $this->M_kelolaPelajaran->deletePelajaranSekolah($ID);
    redirect(base_url('C_kelolaPelajaran'));
  }

  public function detail($ID = null){
    $this->M_kelolaPelajaran->getPelajaranByID($ID);
  }

  public function search(){
    $keyword = $this->input->post('keyword');
    
    $main_content['page'] = 'kelolaPelajaran';
    $this->load->view('header', $main_content);
    $data["daftarPelajaran"]= $this->M_kelolaPelajaran->searchData($keyword);
    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaPelajaran->getDataSekolah($this->session->userdata('UserID'));
      $data["daftarPelajaran"]= $this->M_kelolaPelajaran->searchData_lvl3($keyword,$data["dataSekolah"][0]->SchoolID);
      $this->load->view('kelolaPelajaranSekolah', $data);
    } else {
      $this->load->view('kelolaPelajaran', $data);
    }
    $this->load->view('footer');
  }
}
