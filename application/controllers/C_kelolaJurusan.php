<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaJurusan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('M_kelolaJurusan');
        if($this->session->userdata('UserLevel') == 2){
          redirect('');
        } 
  }

	public function index(){
    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaJurusan->getDataSekolah($this->session->userdata('UserID'));
      $data["daftarJurusan2"]= $this->M_kelolaJurusan->getAllJurusan();
      $data["daftarJurusan"]= $this->M_kelolaJurusan->getAllJurusanSekolah($data["dataSekolah"][0]->SchoolID);
      $data["daftarJenjang"]= $this->M_kelolaJurusan->getAllJenjang();
      $main_content['page'] = 'kelolaJurusan';
      $this->load->view('header', $main_content);
      $this->load->view('kelolaJurusanSekolah', $data);
      $this->load->view('footer');
    } else {
      $data["daftarJurusan"]= $this->M_kelolaJurusan->getAllJurusan();
      $data["daftarJenjang"]= $this->M_kelolaJurusan->getAllJenjang();
      $main_content['page'] = 'kelolaJurusan';
      $this->load->view('header', $main_content);
      $this->load->view('kelolaJurusan', $data);
      $this->load->view('footer');
    }
    
  }

  public function add(){       

    $this->form_validation->set_rules('SchoolLevelID', 'text', 'required');
    $this->form_validation->set_rules('MajorName', 'text', 'required');
    $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
    <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
    if ($this->form_validation->run() == TRUE) {
      
      $this->M_kelolaJurusan->addJurusan();
          
      echo "berhasil upload";
      $this->session->set_flashdata('create_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Jurusan Berhasil ditambahkan</div>');
      redirect(base_url('C_kelolaJurusan'));
      
    }else{
        echo "gagal";
    } 
    $main_content['conten'] = $this->load->view('kelolaJurusan', true);
    $main_content['page'] = 'kelolaJurusan';
    $this->load->view('header', 'footer' , $main_content);
  }

  public function addJurusanSekolah(){       

    $this->form_validation->set_rules('SchoolID', 'text', 'required');
    $this->form_validation->set_rules('MajorID', 'text', 'required');
    $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
    <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
    if ($this->form_validation->run() == TRUE) {
      
      $this->M_kelolaJurusan->addJurusanSekolah();
          
      echo "berhasil upload";
      $this->session->set_flashdata('create_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Jurusan Berhasil ditambahkan</div>');
      redirect(base_url('C_kelolaJurusan'));
      
    }else{
        echo "gagal";
    } 
    $main_content['conten'] = $this->load->view('kelolaJurusan', true);
    $main_content['page'] = 'kelolaJurusan';
    $this->load->view('header', 'footer' , $main_content);
  }

  public function update($id)
  {
    $this->M_kelolaJurusan->updateJurusan($id);
    redirect('C_kelolaJurusan');
  }

  public function delete($ID) {
    $this->M_kelolaJurusan->deleteJurusan($ID);
    redirect(base_url('C_kelolaJurusan'));
  }

  public function deleteJurusanSekolah($ID) {
    $this->M_kelolaJurusan->deleteJurusanSekolah($ID);
    redirect(base_url('C_kelolaJurusan'));
  }

  public function detail($ID = null){
    $this->M_kelolaJurusan->getMajorByID($ID);
  }

  public function search(){
    $keyword = $this->input->post('keyword');

    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaJurusan->getDataSekolah($this->session->userdata('UserID'));
      $data["daftarJurusan"]= $this->M_kelolaJurusan->searchData_lvl3($keyword,$data["dataSekolah"][0]->SchoolID);
      $data["daftarJenjang"]= $this->M_kelolaJurusan->getAllJenjang();
      $main_content['page'] = 'kelolaJurusan';
      $this->load->view('header', $main_content);
      $this->load->view('kelolaJurusanSekolah', $data);
      $this->load->view('footer');
    } else {
      $data["daftarJurusan"]= $this->M_kelolaJurusan->searchData($keyword);
      $data["daftarJenjang"]= $this->M_kelolaJurusan->getAllJenjang();
      $main_content['page'] = 'kelolaJurusan';
      $this->load->view('header', $main_content);
      $this->load->view('kelolaJurusan', $data);
      $this->load->view('footer');
    }
  }
}
