<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaKelas extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('M_kelolaKelas');
        if($this->session->userdata('UserLevel') == 2){
          redirect('');
        }
  }

	public function index(){
    $data["daftarKelas"]= $this->M_kelolaKelas->getAllKelas();
    $data["daftarNamaSekolah"]= $this->M_kelolaKelas->getAllNamaSekolah();
    $data["daftarNamaJurusan"]= $this->M_kelolaKelas->getAllJurusan();
    
    $main_content['page'] = 'kelolaKelas';
    $this->load->view('header', $main_content);
    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaKelas->getDataSekolah($this->session->userdata('UserID'));
      $data["daftarKelas"]= $this->M_kelolaKelas->getAllKelasByID($data["dataSekolah"][0]->SchoolID);
      $data["daftarNamaJurusan"]= $this->M_kelolaKelas->getAllJurusanSekolah($data["dataSekolah"][0]->SchoolID);
      // var_dump($data["dataSekolah"][0]->SchoolID);exit();
    }

    $this->load->view('kelolaKelas', $data);
    $this->load->view('footer');
  }

  public function add(){       

      $this->form_validation->set_rules('ClassName', 'text', 'required');
      $this->form_validation->set_rules('SchoolID', 'text', 'required');
      $this->form_validation->set_rules('MajorID', 'text', 'required');
      
      
      $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
      <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
      if ($this->form_validation->run() == TRUE) {
        
        $this->M_kelolaKelas->addKelas();
            
        echo "berhasil upload";
        $this->session->set_flashdata('create_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Kelas Berhasil ditambahkan</div>');
        redirect(base_url('C_kelolaKelas'));
        
      }else{
          echo "gagal";
          echo 'kelas:'.$this->input->post('ClassName').'<br>';
          echo 'IDsekolah:'.$this->input->post('SchoolID').'<br>';
          echo 'sekolah:'.$this->input->post('MajorID');
          exit;
      }            

    $main_content['conten'] = $this->load->view('kelolaKelas', true);
    $main_content['page'] = 'kelolaKelas';
    $this->load->view('header', 'footer' , $main_content);
  }

  public function update($id)
  {
    $this->M_kelolaKelas->updateKelas($id);
    redirect('C_kelolaKelas');
  }

  public function delete($ID) {
    $this->M_kelolaKelas->deleteKelas($ID);
    redirect(base_url('C_kelolaKelas'));
  }

  public function detail($ID = null){
    $this->M_kelolaKelas->getClassByID($ID);
  }

  public function search(){
    $keyword = $this->input->post('keyword');

    $data["daftarKelas"]= $this->M_kelolaKelas->searchData($keyword);
    $data["daftarNamaSekolah"]= $this->M_kelolaKelas->getAllNamaSekolah();
    $data["daftarJurusan"]= $this->M_kelolaKelas->getAllJurusan();

    if ($this->session->userdata('UserLevel') == 3) {
      $data["dataSekolah"] = $this->M_kelolaKelas->getDataSekolah($this->session->userdata('UserID'));
      if ($keyword == "") {
        $data["daftarKelas"]= $this->M_kelolaKelas->getAllKelasByID($data["dataSekolah"][0]->SchoolID);
      }else{
        $data["daftarKelas"]= $this->M_kelolaKelas->searchData_lvl3($keyword, $data["dataSekolah"][0]->SchoolID);
      }
    }

    $main_content['page'] = 'kelolaKelas';
    $this->load->view('header', $main_content);
    $this->load->view('kelolaKelas', $data);
    $this->load->view('footer');
  }
}
