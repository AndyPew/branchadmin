<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaTugas extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct() {
		parent::__construct();
		$this->load->model('M_kelolaTugas');
    }

	public function index(){
		$main_content['page'] = 'kelolaTugas';
		$data["daftarKelas"] = $this->M_kelolaTugas->getAllKelas();
		$data["daftarTugas"]= $this->M_kelolaTugas->getAllHomeWork();
		$this->load->view('header', $main_content);
		
		if ($this->session->userdata('UserLevel') == 3) {
			$data["dataSekolah"] = $this->M_kelolaTugas->getDataSekolah($this->session->userdata('UserID'));
			$data["daftarKelas"] = $this->M_kelolaTugas->getAllKelasSekolah($data["dataSekolah"][0]->SchoolID);
			// var_dump($data["dataSekolah"][0]->SchoolID);exit();
		}

        $this->load->view('kelolaTugas', $data);
        $this->load->view('footer');
	}

	public function detail($ID){
		$main_content['page'] = 'kelolaTugas';
		$data["tugas"] = $this->M_kelolaTugas->getHomeWorkByID($ID);
		$data["pertanyaan"] = $this->M_kelolaTugas->getHomeWorkQuestionByID($ID);
		$jawaban = array();
		foreach ($data["pertanyaan"] as $question) {
			array_push($jawaban ,$this->M_kelolaTugas->getHomeWorkAnswerByID($question->HomeWorkQuestionID));
		}
		$data["jawaban"] = $jawaban;
		$this->load->view('header', $main_content);
        $this->load->view('detailTugas', $data);
        $this->load->view('footer');
	}

	public function jawabanSiswa($ID){
		$main_content['page'] = 'kelolaTugas';
		$data["DataTugas"] = $this->M_kelolaTugas->getHomeWorkByID($ID);
		$data["daftarSiswa"] = $this->M_kelolaTugas->getHomeWorkUserAnswerByID($ID);
		$data["totalSoal"] = $this->M_kelolaTugas->getHomeWorkQuestionTotal($ID);

		foreach( $data["daftarSiswa"] as &$siswa ) {
			$jawaban = $this->M_kelolaTugas->getHomeWorkUserAnswerByHomeWorkID($siswa->UserID ,$ID);
			$siswa->TotalJawaban = $jawaban[0]->total;
		}

        $this->load->view('header', $main_content);
        $this->load->view('jawabanSiswa', $data);
        $this->load->view('footer');
	}

	public function nilaiSiswa($ID){
		$main_content['page'] = 'kelolaTugas';
		$data["DataTugas"] = $this->M_kelolaTugas->getHomeWorkByID($ID);
		$data["daftarSiswa"] = $this->M_kelolaTugas->getHomeWorkUserAnswerByID($ID);
		$data["totalSoal"] = $this->M_kelolaTugas->getHomeWorkQuestionTotal($ID);
		$data["pertanyaan"] = $this->M_kelolaTugas->getHomeWorkQuestionByID($ID);

		foreach( $data["daftarSiswa"] as &$siswa ) {
			$nilai = 0;
			$jawabanBenar = 0;
			$jawaban = $this->M_kelolaTugas->getHomeWorkUserAnswerByHomeWorkID($siswa->UserID ,$ID);
			foreach ($data["pertanyaan"] as &$question) {
				$jawabanUser = $this->M_kelolaTugas->getHomeWorkUserAnswerByQuestionID($siswa->UserID, $question->HomeWorkQuestionID);
				$kunciJawaban = $this->M_kelolaTugas->getHomeWorkAnswerKeyByID($question->HomeWorkQuestionID);


				if (count($jawabanUser) > 0) {
					if ($jawabanUser[0]->HomeWorkUserAnswerValue == $kunciJawaban[0]->HomeWorkAnswerKeyValue) {
						$nilai += 1;
						$jawabanBenar += 1;
					}
					$question->HomeWorkUserAnswerValue = $jawabanUser[0]->HomeWorkUserAnswerValue;
				}
			}
			$hasilAkhir = ($nilai / $data["totalSoal"][0]->total) * 100;
			$siswa->TotalJawaban = $jawaban[0]->total;
			$siswa->TotalJawabanBenar = $jawabanBenar;
			$siswa->TotalNilai = $hasilAkhir;
		}

		// var_dump($data["daftarSiswa"]);
		// var_dump($kunciJawaban);
		// exit;

        $this->load->view('header', $main_content);
        $this->load->view('nilaiSiswa', $data);
        $this->load->view('footer');
	}

	public function detailJawabanSiswa(){
		$UserID =  $this->uri->segment(3);
		$ID =  $this->uri->segment(4);

		if ($ID && $UserID) {

			$main_content['page'] = 'kelolaTugas';
			$data["tugas"] = $this->M_kelolaTugas->getHomeWorkByID($ID);
			$data["pertanyaan"] = $this->M_kelolaTugas->getHomeWorkQuestionByID($ID);
			$jawaban = array();
			foreach ($data["pertanyaan"] as &$question) {
				$jawabanUser = $this->M_kelolaTugas->getHomeWorkUserAnswerByQuestionID($UserID, $question->HomeWorkQuestionID);

				if (count($jawabanUser) > 0) {
					$question->HomeWorkUserAnswerValue = $jawabanUser[0]->HomeWorkUserAnswerValue;
				} else {
					$question->HomeWorkUserAnswerValue = 0;
				}
				
				array_push($jawaban ,$this->M_kelolaTugas->getHomeWorkAnswerByID($question->HomeWorkQuestionID));
			}
			$data["jawaban"] = $jawaban;

			$this->load->view('header', $main_content);
			$this->load->view('detailJawabanSiswa', $data);
			$this->load->view('footer');
		} else {
			redirect(base_url('C_kelolaTugas'));
		}
	}
	
	public function tambah(){
		$main_content['page'] = 'kelolaTugas';
		$data["daftarKelas"] = $this->M_kelolaTugas->getAllKelas();
		if ($this->session->userdata('UserLevel') == 4) {
			$data["daftarPelajaran"] = $this->M_kelolaTugas->getAllSubject();
		} else if ($this->session->userdata('UserLevel') == 3) {
			$data["dataSekolah"] = $this->M_kelolaTugas->getDataSekolah($this->session->userdata('UserID'));
			$data["daftarPelajaran"] = $this->M_kelolaTugas->getSubjectBySchoolID($data["dataSekolah"][0]->SchoolID);
			$data["daftarKelas"] = $this->M_kelolaTugas->getAllKelasSekolah($data["dataSekolah"][0]->SchoolID);
		} else if ($this->session->userdata('UserLevel') == 2) {
			$data["daftarPelajaran"] = $this->M_kelolaTugas->getSubjectByTeacherID($this->session->userdata('UserID'));
			// var_dump($this->session->userdata('UserID'));
			// var_dump($data["daftarPelajaran"]);
			// exit;
		}
		$this->load->view('header', $main_content);
        $this->load->view('tambahTugas', $data);
        $this->load->view('footer');
	}
	
	public function tambahTugas() {
		$title = $this->input->post('title');
		$desc = $this->input->post('desc');
		$UserID = $this->input->post('userID');
		$classID = $this->input->post('classID');
		$subjectID = $this->input->post('subjectID');
		$banyakSoal = (int)$this->input->post('banyakSoal');
		$banyakJawaban = (int)$this->input->post('banyakJawaban');

		$date = new DateTime($this->input->post('date'));
 
		$newDate = $date->format('y-m-d H:i:s');

		if ($title && $desc && $UserID && $classID && $subjectID && $banyakSoal && $banyakJawaban) {
			$ID = $this->M_kelolaTugas->addHomeWork($newDate);
			if ($ID) {
				for ($i = 1; $i <= $banyakSoal; $i++) {
					$IDPertanyaan = $this->M_kelolaTugas->addHomeWorkQuestion($ID, $this->input->post('Pertanyaan'.$i));
					echo $this->input->post('Pertanyaan'.$i).':';
					$IDPertanyaan = $this->M_kelolaTugas->addHomeWorkAnswerKey($IDPertanyaan , $this->input->post('answerofquestion'.$i));
					for ($j = 1; $j <= $banyakJawaban; $j++) {
						$this->M_kelolaTugas->addHomeWorkAnswer($IDPertanyaan, $this->input->post('Jawaban'.$i.','.$j));
					}
				}

				$token = $this->M_kelolaTugas->getToken($classID);
				$this->load->library('notification');
            	$this->notification->sendNotif($token, 'Tugas Rumah', 'Wah ada tugas baru nih buat kamu');
			} else {
				echo 'failed to insert';
			}
	
			redirect(base_url('C_kelolaTugas'));
		} else {
			redirect(base_url('C_kelolaTugas/tambah'));
		}
	}

	public function search()
  	{
	$keyword = $this->input->post('keyword');
	
	$main_content['page'] = 'kelolaTugas';
	$data["daftarTugas"]= $this->M_kelolaTugas->searchData($keyword);

	if ($this->session->userdata('UserLevel') == 3) {
		$data["dataSekolah"] = $this->M_kelolaTugas->getDataSekolah($this->session->userdata('UserID'));
		if ($keyword == "") {
		  $data["daftarKelas"]= $this->M_kelolaTugas->getAllKelasSekolah($data["dataSekolah"][0]->SchoolID);
		}else{
		  $data["daftarKelas"]= $this->M_kelolaTugas->searchData_lvl3($keyword, $data["dataSekolah"][0]->SchoolID);
		}
	}

	$this->load->view('header', $main_content);
	$this->load->view('kelolaTugas', $data);
	$this->load->view('footer');
  	}
}
