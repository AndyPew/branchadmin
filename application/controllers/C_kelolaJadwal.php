<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaJadwal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct() {
		parent::__construct();
		$this->load->model('M_kelolaJadwal');
    }

	public function index(){
		$main_content['page'] = 'kelolaJadwal';
		
		if ($this->session->userdata('UserLevel') == 3) {
			$data["dataSekolah"] = $this->M_kelolaJadwal->getDataSekolah($this->session->userdata('UserID'));
			$data["daftarKelas"] = $this->M_kelolaJadwal->getAllKelasByschoolID($data["dataSekolah"][0]->SchoolID);
		}else{
			$data["daftarKelas"] = $this->M_kelolaJadwal->getAllKelas();
		}

		foreach ( $data["daftarKelas"] as &$kelas) {
			for ($i = 1; $i <= 6; $i++) {
				$kelas->Jadwal[$i] = $this->M_kelolaJadwal->getJadwalByClassDate($kelas->ClassID, $i);
			}
			$kelas->TotalJam = $this->M_kelolaJadwal->getLastJadwalByClass($kelas->ClassID);
		}		

        $this->load->view('header', $main_content);
        $this->load->view('kelolaJadwal', $data);
        $this->load->view('footer');
	}

	public function edit($ID){
		for ($i = 1; $i <= 6; $i++) {
			$data["Jadwal"][$i] = $this->M_kelolaJadwal->getJadwalByClassDate($ID, $i);
		}
		$main_content['page'] = 'kelolaJadwal';

		$data["dataSekolah"] = $this->M_kelolaJadwal->getDataSekolah($this->session->userdata('UserID'));
		
		$data["daftarGuru"]= $this->M_kelolaJadwal->getAllGuru($data["dataSekolah"][0]->SchoolID);
		$data["daftarPelajaran"]= $this->M_kelolaJadwal->getAllPelajaranSekolah($data["dataSekolah"][0]->SchoolID);
		$data["dataKelas"] = $this->M_kelolaJadwal->getKelasByID($ID);
        $this->load->view('header', $main_content);
        $this->load->view('tambahJadwal', $data);
        $this->load->view('footer');
	}

	public function add(){
		// var_dump($this->input->post());
		$ClassID = $this->input->post('ClassID');
		echo '<br>ClassID: '.$ClassID.'<br>';
		$error = false;
		for ($i = 0; $i < 6; $i++) {
			$TotalIndex = $this->input->post('IndexCount_'.$i);
			echo '<br>Index: '.$TotalIndex.'<br>';
			for ($j = 0; $j < $TotalIndex; $j++) {
				$Jam = $this->input->post('JamPelajaran_'.$i.'_'.$j);
				echo '&emsp; Jam: '.$Jam.'<br>';
				$New = $this->input->post('New_'.$i.'_'.$j);
				echo '&emsp; New: '.$New.'<br>';
				$Update = $this->input->post('Update_'.$i.'_'.$j);
				echo '&emsp; Update: '.$Update.'<br>';
				$Delete = $this->input->post('Delete_'.$i.'_'.$j);
				echo '&emsp; Delete: '.$Delete.'<br>';
				$ID = $this->input->post('ID_'.$i.'_'.$j);
				echo '&emsp; ID: '.$ID.'<br>';
				$SubjectID = $this->input->post('SubjectID_'.$i.'_'.$j);
				echo '&emsp; SubjectID: '.$SubjectID.'<br>';
				$UserID = $this->input->post('UserID_'.$i.'_'.$j);
				echo '&emsp; UserID: '.$UserID.'<br>';
				$Desc = $this->input->post('Desc_'.$i.'_'.$j);
				echo '&emsp; Desc: '.$Desc.'<br><br>';

				if ($New == 'true' && $Delete == 'false') {
					if ($this->M_kelolaJadwal->addJadwal($Jam, $i + 1, $Desc, $UserID, $SubjectID, $ClassID)) {

					} else {
						$error = true;
					}
					
				}

				if ($New == 'false' && $Update == 'true' && $Delete == 'false' && $ID) {
					if ($this->M_kelolaJadwal->updateJadwal($Jam, $i + 1, $Desc, $UserID, $SubjectID, $ClassID, $ID)) {

					} else {
						$error = true;
					}
				}

				if ($New == 'false' && $Delete == 'true' && $ID) {
					if ($this->M_kelolaJadwal->deleteJadwal($ID)) {

					} else {
						$error = true;
					}
				}
			}
		}
		if (!$error) {
			redirect(base_url('C_kelolaJadwal'));
		}
	}
}
