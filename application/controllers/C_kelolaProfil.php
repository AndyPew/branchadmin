<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaProfil extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('M_kelolaProfil');
  }

	public function index(){
    $data["profil"]= $this->M_kelolaProfil->getProfil();

    $main_content['page'] = 'kelolaProfil';
    $this->load->view('header', $main_content);
    $this->load->view('kelolaProfil', $data);
    $this->load->view('footer');
  }

  public function update($id)
  {
    $file_name = $this->input->post('gambar_profile');
    if(!empty($file_name)){
        $this->M_kelolaProfil->updateProfil_wImage($id, $file_name);
    }else{
        $this->M_kelolaProfil->updateProfil_nImage($id);
    }

    redirect('C_kelolaProfil');
  }

  public function upload_foto(){
    $data = $_POST['gambar'];
    list($type, $data) = explode(';', $data);
    list(, $data)      = explode(',', $data);
    $data = base64_decode($data);
    $imageName = time().'.png';
    file_put_contents('gambar_profile/'.$imageName, $data);
    // $data['gambar'] = $imageName;
    // redirect('manager/template_admin/v_register', $data);
    echo '<img src="https://branchsch.tech/gambar_profile/'.$imageName.'" name="gambar_profile" id="img-profile" />
    <input type="hidden" name="gambar_profile" value="gambar_profile/'.$imageName.'">
    ';
  }

  public function changePassword(){
    $newPass = $this->input->post("UserPassNew");
    $oldPass = $this->input->post("UserPassOld");
    $pengacak = "p3ng4c4k";
    $UserPassword1 = MD5($oldPass);
    $UserPassword = md5($pengacak . md5($UserPassword1));
    $data["profilByID"]= $this->M_kelolaProfil->getProfilByID($UserPassword);
    if(count($data["profilByID"]) > 0 ){
      $pengacak = "p3ng4c4k";
      $UserPass1 = MD5($newPass);
      $UserPass = md5($pengacak . md5($UserPass1));
      $this->M_kelolaProfil->changePassword($UserPass);
      redirect('C_kelolaProfil');      
    } else{
      echo 'gagal';
    }
  }

}
