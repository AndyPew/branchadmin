<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_diskusi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('M_diskusi');
    }

	public function index() {
        $dataGuru = $this->M_diskusi->getDataGuru($this->session->userdata('UserID'));
        $IDKelas = $this->M_diskusi->getDataKelas($this->session->userdata('UserID'));

        $DataKelas = array();
        foreach ($IDKelas as $index=>$Kelas) {
            $MyKelas = $this->M_diskusi->getDataKelasByID($Kelas);
            array_push($DataKelas, $MyKelas);
        }

        $data["DataKelas"] = $DataKelas;

        $main_content['page'] = 'diskusi';
        $this->load->view('header', $main_content);
        $this->load->view('diskusi', $data);
        $this->load->view('footer');
    }

    public function Diskusi($ID) {
        if ($this->input->post('Message')) {
            $Message = $this->input->post('Message');
            $UserID = $this->session->userdata('UserID');

            if ($this->M_diskusi->insertDiskusi($Message, $UserID)) {

            }
        }

        $IDSiswa = $this->M_diskusi->getSiswaIDByClass($ID);
        $IDGuru = $this->M_diskusi->getGuruIDByClass($ID);

        $AllUserID = array_merge($IDGuru,$IDSiswa);

        $data['dataDiskusi'] = $this->M_diskusi->getDiskusiByID($AllUserID);
        
        $data['dataKelas'] = $this->M_diskusi->getDataKelasByID($ID);
        $main_content['page'] = 'diskusi';
        $this->load->view('header', $main_content);
        $this->load->view('diskusiForm', $data);
        $this->load->view('footer');
    }
}
