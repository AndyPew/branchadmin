<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaPengumuman extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

  public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('M_kelolaPengumuman');
        if($this->session->userdata('UserLevel') == 2){
          redirect('');
        } 
  }

	public function index(){
    $data["daftarPengumuman"]= $this->M_kelolaPengumuman->getAllPengumuman();
    $data["daftarSekolah"]= $this->M_kelolaPengumuman->getAllSekolah();
    $main_content['page'] = 'kelolaPengumuman';
    $this->load->view('header', $main_content);
    $this->load->view('kelolaPengumuman', $data);
    $this->load->view('footer');
  }

  public function add(){       

    $this->form_validation->set_rules('SchoolAnnoucementTitle', 'text', 'required');
    $this->form_validation->set_rules('SchoolAnnoucementDesc', 'text', 'required');
    $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
    <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
    if ($this->form_validation->run() == TRUE) {

      $gambar='';
      $nmfile = "profile_".time();
      $config['upload_path'] = './gambar/';
      $config['allowed_types'] = 'jpg|jpeg|png';
      $config['file_name'] = $nmfile;

      $this->load->library('upload', $config);
      if($this->upload->do_upload('gambar')){
      
        $upload_data = $this->upload->data();
        $file_name = 'gambar/'.$upload_data['file_name'];
        $this->M_kelolaPengumuman->addPengumuman($file_name);  
      }else{
        $this->M_kelolaPengumuman->addPengumuman_noImage();  
      }

      $token = $this->M_kelolaPengumuman->getToken($this->input->post('SchoolID'));

      $this->load->library('notification');
      $this->notification->sendNotif($token, 'Pengumuman', 'Wah ada pengumuman baru nih!');
      
      $this->session->set_flashdata('create_user', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Jurusan Berhasil ditambahkan</div>');
      redirect(base_url('C_kelolaPengumuman'));
      
    }else{
      $this->session->set_flashdata('gagalUpload', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Maaf pengumuman gagal di upload</div>');
      redirect(base_url('C_kelolaPengumuman'));
    }
  }

  public function update($id)
  {
    $this->M_kelolaPengumuman->updatePengumuman($id);
    // var_dump($this->db->last_query());exit();
    redirect('C_kelolaPengumuman');
  }

  public function delete($ID) {
    $this->M_kelolaPengumuman->deletePengumuman($ID);
    redirect(base_url('C_kelolaPengumuman'));
  }

  public function detail($ID = null){
    $this->M_kelolaPengumuman->getPengumumanByID($ID);
  }

  public function search()
  {
    $keyword = $this->input->post('keyword');

    $data["daftarPengumuman"]= $this->M_kelolaPengumuman->searchData($keyword);
    $data["daftarSekolah"]= $this->M_kelolaPengumuman->getAllSekolah();
    
    $main_content['page'] = 'kelolaPengumuman';
    $this->load->view('header', $main_content);
    $this->load->view('kelolaPengumuman', $data);
    $this->load->view('footer');
  }
}
