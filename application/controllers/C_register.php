<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('M_login');
    }

	public function index()
	{
        $this->form_validation->set_rules('SchoolStatisticNumber', 'text', 'required');
        $this->form_validation->set_rules('SchoolNationalUnicID', 'text', 'required');
        $this->form_validation->set_rules('SchoolName', 'text', 'required');
        $this->form_validation->set_rules('SchoolAddress', 'text', 'required');
        $this->form_validation->set_rules('SchoolEmail', 'text', 'required');
        $this->form_validation->set_rules('SchoolLevelID', 'text', 'required');
        $this->form_validation->set_rules('PrivacyPolicy', 'text', 'required');
        
        
        $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
            <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');
        if ($this->form_validation->run() == TRUE) {
            $this->db->db_debug = false;
            $register = $this->M_login->Register();
            if ($register) {
                $data['modal'] = true;
                $this->load->view('register', $data);
            } else {
                $data['error'] = true;
                $this->load->view('register', $data);
            }
        } else {
            $this->load->view('register');
        }
	}
}
