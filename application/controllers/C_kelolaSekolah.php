<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelolaSekolah extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('M_kelolaSekolah');
        if($this->session->userdata('UserLevel') != 4){
            redirect('');
        }
    }

	public function index(){
        $main_content['page'] = 'kelolaSekolah';
        $data["daftarSchool"]= $this->M_kelolaSekolah->getSchoolUnverified();
        $data["daftarSchool2"]= $this->M_kelolaSekolah->getSchool();
        $data["kelolaSekolah"]= $this->M_kelolaSekolah->getAllSchool();
        $this->load->view('header', $main_content);
        $this->load->view('kelolaSekolah', $data);
        $this->load->view('footer');
    }

    public function verifikasi($ID) {
        $password =  random_string('alnum', 16); 

        $pengacak = "p3ng4c4k";
        $UserPassword1 = MD5($password);
        $UserPassword = md5($pengacak . md5($UserPassword1));

        $this->M_kelolaSekolah->addAdmin($ID, $UserPassword);

        $config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'mybranch19@gmail.com',    // Ganti dengan email gmail kamu
            'smtp_pass' => 'MyBRancH19',      // Password gmail kamu
            'smtp_port' => 465,
            'crlf'      => "\r\n",
            'newline'   => "\r\n"
        ];

        
        $mydata = $this->M_kelolaSekolah->getSchoolByID($ID);
    

        // Load library email dan konfigurasinya
        $this->load->library('email', $config);

        // Email dan nama pengirim
        $this->email->from('mybranch19@gmail.com', 'branchsch.tech');

        // Email penerima
        $this->email->to($mydata[0]->SchoolEmail); // Ganti dengan email tujuan kamu

        // Subject email
        $this->email->subject('Sekolah Sudah Terverifikasi');

        // Isi email
        $this->email->message('
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <div dir="ltr">
        <span style="display:none;font-size:0px;line-height:0px;max-height:0px;max-width:0px;opacity:0;overflow:hidden" class="mcnPreviewText"></span>
        <center>
            <div>
            <br>
            </div>
            <table style="border-collapse:collapse;height:100%;margin:0;padding:0;width:100%;background-color:#384447" id="bodyTable" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0" align="center">
            <tbody>
                <tr>
                <td style="height:100%;margin:0;padding:0;width:100%;border-top:0" id="bodyCell" valign="top" align="center">
                    <table style="border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                        <td style="background:#1C375B none no-repeat center/cover;background-color:#1C375B;background-image: linear-gradient(to right top, #1c375b,#6f8197);background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:9px;padding-bottom:9px" id="templatePreheader" valign="top" align="center">
                            <table style="border-collapse:collapse;max-width:600px!important" class="templateContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                                <tr>
                                <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="preheaderContainer" valign="top">
                                    <table style="min-width:100%;border-collapse:collapse" class="mcnTextBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody class="mcnTextBlockOuter">
                                        <tr>
                                        <td style="padding-top:9px" class="mcnTextBlockInner" valign="top">
                                            <table class="mcnTextContentContainer" width="100%" style="max-width:100%;min-width:100%;border-collapse:collapse" cellspacing="0" cellpadding="0" border="0" align="left">
                                            <tbody>
                                                <tr>
                                                <td style="padding-top:0;padding-right:18px;padding-bottom:9px;padding-left:18px;word-break:break-word;color:#656565;font-family:Helvetica;font-size:12px;line-height:150%;text-align:left" class="mcnTextContent" valign="top">
                                                    <center>
                                                    <div style="text-align:left">
                                                        <img style="margin-right: 0px;" height="60" width="176" src="https://ci3.googleusercontent.com/proxy/LfnzQZcXiJXOFFSnsp5X1EmPLcxgTnYy_VD9fWBmGa9BSREYltcPHR1o0I5neeLmpDpaDfQKfdIneMB9sOVwK6D-aTA=s0-d-e1-ft#http://branchsch.tech/assets/img/Branch_white.png">
                                                    </div>
                                                    </center>
                                                </td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </td>
                                </tr>
                            </tbody>
                            </table>
                        </td>
                        </tr>
                        <tr>
                        <td style="background:#dff0f4 url(&quot;https://gallery.mailchimp.com/ed526b2f15f645fc575e0db76/_compresseds/24d9180b-aefe-4a3a-90dc-8c097d9a5f2c.jpg&quot;) no-repeat center/cover;background-color:#dff0f4;background-image:url(https://gallery.mailchimp.com/ed526b2f15f645fc575e0db76/_compresseds/24d9180b-aefe-4a3a-90dc-8c097d9a5f2c.jpg);background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:80px;padding-bottom:90px" id="templateHeader" valign="top" align="center">
                            <table style="border-collapse:collapse;max-width:600px!important" class="templateContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                                <tr>
                                <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="headerContainer" valign="top">
                                    <table style="min-width:100%;border-collapse:collapse" class="mcnTextBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody class="mcnTextBlockOuter">
                                        <tr>
                                        <td style="padding-top:9px" class="mcnTextBlockInner" valign="top">
                                            <table class="mcnTextContentContainer" width="100%" style="max-width:100%;min-width:100%;border-collapse:collapse" cellspacing="0" cellpadding="0" border="0" align="left">
                                            <tbody>
                                                <tr>
                                                <td style="padding:0px 18px 9px;word-break:break-word;font-size:18px;line-height:150%;text-align:left" class="mcnTextContent" valign="top">
                                                    <h1 style="display:block;margin:0px;padding:0px;font-size:30px;font-style:normal;font-weight:600;line-height:125%;letter-spacing:normal;text-align:left">
                                                    <font color="#1C375B" face="poppins, open sans, helvetica neue, helvetica, arial, sans-serif">Sekolahmu Sudah</font>
                                                    </h1>
                                                    <h1 style="display:block;margin:0px;padding:0px;font-size:30px;font-style:normal;font-weight:700;line-height:125%;letter-spacing:normal;text-align:left">
                                                    <font color="#1C375B" face="poppins, open sans, helvetica neue, helvetica, arial, sans-serif">Terverifikasi</font>
                                                    </h1>
                                                </td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                    <table style="min-width:100%;border-collapse:collapse" class="mcnTextBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody class="mcnTextBlockOuter">
                                        <tr>
                                        <td style="padding-top:9px" class="mcnTextBlockInner" valign="top">
                                            <table class="mcnTextContentContainer" width="100%" style="max-width:100%;min-width:100%;border-collapse:collapse" cellspacing="0" cellpadding="0" border="0" align="left">
                                            <tbody>
                                                <tr>
                                                <td style="padding:0px 18px 9px;line-height:100%;word-break:break-word;color:#202020;font-family:Poppins, Helvetica;font-size:18px;text-align:left" class="mcnTextContent" valign="top">
                                                    <span style="color:#3D3D3D">
                                                    <span style="font-size:14px">Sekarang waktunya login
                                                        <br>
                                                        kami sudah membuatkan akun untuk sekolahmu
                                                    </span>
                                                    </span>
                                                </td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                    <table style="min-width:100%;border-collapse:collapse;table-layout:fixed!important" class="mcnDividerBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody class="mcnDividerBlockOuter">
                                        <tr>
                                        <td style="min-width:100%;padding:18px" class="mcnDividerBlockInner">
                                            <table style="min-width:100%;border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" border="0" class="mcnDividerContent">
                                            <tbody>
                                                <tr>
                                                <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <span></span>
                                                </td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                    <table style="min-width:100%;border-collapse:collapse" class="mcnButtonBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody class="mcnButtonBlockOuter">
                                        <tr>
                                        <td class="mcnButtonBlockInner" align="left" valign="top" style="padding-top:0;padding-right:18px;padding-bottom:18px;padding-left:18px">
                                            <table style="border-collapse:separate!important;border-radius:5px;background-image: linear-gradient(to right top, #1c375b,#6f8197)" class="mcnButtonContentContainer" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                <td style="font-family:Arial;font-size:16px;padding:15px" class="mcnButtonContent" valign="middle" align="center">
                                                    <a style="font-weight:bold;letter-spacing:1px;line-height:100%;text-align:center;text-decoration:none;color:#ffffff;display:block" target="_blank" href="http://www.branchsch.tech/C_login" title="Try Aeon for FREE" class="mcnButton">Login Branch</a>
                                                </td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </td>
                                </tr>
                            </tbody>
                            </table>
                        </td>
                        </tr>
                        <tr>
                        <td style="background:#4ec0e2 none no-repeat center/cover;background-color:#4ec0e2;background-image: linear-gradient(to right top, #1c375b,#6f8197);background-repeat:no-repeat;background-position:center;background-size:cover;border-top:0;border-bottom:0;padding-top:80px;padding-bottom:50px" id="templateBody" valign="top" align="center">
                            <table style="border-collapse:collapse;max-width:600px!important" class="templateContainer" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                                <tr>
                                <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="bodyContainer" valign="top">
                                    <table style="min-width:100%;border-collapse:collapse" class="mcnTextBlock" width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody class="mcnTextBlockOuter">
                                        <tr>
                                        <td style="padding-top:9px" class="mcnTextBlockInner" valign="top">
                                            <table class="mcnTextContentContainer" width="100%" style="max-width:100%;min-width:100%;border-collapse:collapse" cellspacing="0" cellpadding="0" border="0" align="left">
                                            <tbody>
                                                <tr>
                                                <td style="padding:0px 18px 9px;line-height:110%;word-break:break-word;color:#202020;font-family:Poppins, Helvetica;font-size:14px;text-align: justify; font-weight:300" class="mcnTextContent">
                                                <span style="color: #FFFFFF">Selamat '.$mydata[0]->SchoolName.', sekolahmu sudah diverifikasi oleh admin branch. Sekarang waktunya  login dan mengisi semua perangkat sekolahmu. Kami sudah membuatkan akun untuk sekolahmu. Silahkan login dengan akun yang tertera dibawah ini :</span>
                                                </td>
                                                </tr>
            
                                                <tr>
                                                <td>
                                                    <div style="width: 240px; margin: 0 auto;">
                                                        <h4 style="margin: 0; padding: 0; color: #FFFFFF; margin-top: 40px">Email</h4>
                                                        <p style="background-color: white; padding-top: 12px; padding-bottom: 12px; border-radius: 50px; text-align: center; margin: 0; margin-top: 4px">'.$mydata[0]->SchoolEmail.'</p>
                                                    </div>
                                                </td>
                                                </tr>
                                                
                                                <tr>
                                                <td>
                                                    <div style="width: 240px; margin: 0 auto; margin-top: 20px; margin-bottom: 40px">
                                                        <h4 style="margin: 0; padding: 0; color: #FFFFFF">Password</h4>
                                                        <p style="background-color: white; padding-top: 12px; padding-bottom: 12px; border-radius: 50px; text-align: center; margin: 0; margin-top: 4px">'.$password.'</p>
                                                    </div>
                                                </td>
                                                </tr>
                                                
                                                <tr>
                                                <td style="padding:0px 18px 9px;line-height:100%;word-break:break-word;color:#202020;font-family:Poppins, Helvetica;font-size:14px;text-align:left; font-weight:300; text-align: center;" class="mcnTextContent">
                                                <span style="color: #FFFFFF;">Jika memerlukan bantuan, silahkan hubungi melalui email mybranch19@gmail.com  terima kasih.</span>
                                                </td>
                                                </tr>
                                            </tbody>
                                            </table>
                                        </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </td>
                                </tr>
                            </tbody>
                            </table>
                        </td>
                        </tr>
                    </tbody>
                    </table>
                </td>
                </tr>
            </tbody>
            </table>
        </center>
        <br>
        </div>

        ');

        // Tampilkan pesan sukses atau error
        if ($this->email->send()) {
            redirect(base_url('C_kelolaSekolah'));
        } else {
            echo 'Error! email tidak dapat dikirim.';
        }
    }

    public function Cancelverifikasi($id)
    {
        $this->M_kelolaSekolah->CancelVerifikasi($id);
        redirect('C_kelolaSekolah');
    }

    public function edit(){
        $this->form_validation->set_rules('SchoolStatisticNumber', 'text', 'required');
        $this->form_validation->set_rules('SchoolNationalUnicID', 'text', 'required');
        $this->form_validation->set_rules('SchoolID', 'text', 'required');
        $this->form_validation->set_rules('SchoolLevelID', 'text', 'required');

        if($this->form_validation->run() != FALSE){
            
            $data["kelolaSekolah"]= $product->getAllSchool();
            $product->updateSekolah($data);  
            
            $this->session->set_flashdata('edit', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i>Sekolah Berhasil diupdate</div>');
            // var_dump($file_name);
            // exit();
            redirect(base_url('C_kelolaSekolah'));  
        }else{
            redirect(base_url('C_kelolaSekolah'));
        }
    }

    public function update($id)
    {
        $this->M_kelolaSekolah->updateSekolah($id);
        redirect('C_kelolaSekolah');
    }

    public function delete($ID) {
        $this->M_kelolaSekolah->deleteSekolah($ID);
        redirect(base_url('C_kelolaSekolah'));
    }

    public function detail($ID = null){
        $this->M_kelolaSekolah->getSchoolByID($ID);
    }

    public function search(){
        $keyword = $this->input->post('keyword');

        $main_content['page'] = 'kelolaSekolah';
        $data["daftarSchool"]= $this->M_kelolaSekolah->getSchoolUnverified();
        $data["daftarSchool2"]= $this->M_kelolaSekolah->searchData($keyword);
        $data["kelolaSekolah"]= $this->M_kelolaSekolah->getAllSchool();
        $this->load->view('header', $main_content);
        $this->load->view('kelolaSekolah', $data);
        $this->load->view('footer');
    }
}
