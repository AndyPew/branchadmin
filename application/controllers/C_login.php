<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();
        $this->load->model('M_login');
		$this->check_cookie();
    }
	public function index() {
		if($this->M_login->logged_id()){
			redirect('Main');
		}else{	
			$this->form_validation->set_rules('UserEmail', 'text', 'required');
			$this->form_validation->set_rules('UserPassword', 'text', 'required');
			$data['error'] = "";
			if ($this->form_validation->run() == TRUE) {
				$pengacak = "p3ng4c4k";

				$UserEmail = $this->input->post("UserEmail", TRUE);
				$remember = $this->input->post('remember');
				$UserPassword1 = MD5($this->input->post('UserPassword'));
				$UserPassword = md5($pengacak . md5($UserPassword1));
				
				$checking = $this->M_login->check_login('user', array('UserEmail' => $UserEmail), array('UserPassword' => $UserPassword) );
				if ($checking != FALSE) {
					foreach ($checking as $apps) {
						// var_dump($apps->isDelete);exit();
						if ($apps->UserLevel == 1) {
							$data['error'] = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
								<span class="alert-inner--icon"><i class="fas fa-exclamation-triangle"></i></span>
								<span class="alert-inner--text"><strong>Oopss..! </strong>Sorry <br> Student Can`t Login Here</span>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							</div>'; 
						} else if($apps->isDelete != "1"){
							if ($remember) {
								$key = random_string('alnum', 64);
								set_cookie('branch', $key, 3600*24*30); 
							
								$update_data = array(
									'LoginToken' => $key
								);
								$this->M_login->update($update_data, $apps->UserID);
							}			
							$this->login($apps);
						}
						else{
							$data['error'] = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
							<span class="alert-inner--icon"><i class="fas fa-exclamation-triangle"></i></span>
							<span class="alert-inner--text"><strong>Oopss..!<br></strong>Maaf Akun anda sudah di hapus oleh admin. Silahkan Hubungi admin !</span>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							</div>';
						}
					}
				}
				else{
					$data['error'] = '<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<h5><i class="icon fa fa-ban"></i> Username atau password salah !</h5>
					</div>';
				}	
			}
			$this->load->view('login', $data);
		}
	}

	private function check_cookie(){
		$cookie = get_cookie('branch');

		if($cookie <> '') {
			// cek cookie
			$row = $this->M_login->get_by_cookie($cookie)->row();
			if ($row) {
				$this->login($row);
				$this->_daftarkan_session($row);
			}
		}
	}

	public function login($data){
		$session_data = array(	                        
			'UserID' => $data->UserID,
			'UserDetailID' => $data->UserDetailID,
			'UserEmail' => $data->UserEmail,
			'UserPassword' => $data->UserPassword,
			'UserName' => $data->UserName,
			'UserProfile' => $data->UserProfile,
			'UserGender' => $data->UserGender,
			'UserLevel' => $data->UserLevel,
			'LastOpenApp' => $data->UserName,
			'createdAt' => $data->createdAt,
		);
		if($data->UserLevel != 1) {
			$this->M_login->logged_in($data->UserID, $key);
			$this->session->set_userdata($session_data);
			redirect(base_url(''));
		}
	}

	public function forgotPass(){
		$this->load->view('forgotPassword');
	}

	public function email_reset_password_validation(){
      	$this->form_validation->set_rules('UserEmail', 'UserEmail', 'required|valid_email|trim');
  		if($this->form_validation->run()){
    
	        $email = $this->input->post('UserEmail');
	        $reset_key =  random_string('alnum', 64);
	    
	        if($this->M_login->update_reset_key($email,$reset_key)){
	        	$config = [
		            'mailtype'  => 'html',
		            'charset'   => 'utf-8',
		            'protocol'  => 'smtp',
		            'smtp_host' => 'ssl://smtp.gmail.com',
		            'smtp_user' => 'mybranch19@gmail.com',    // Ganti dengan email gmail kamu
		            'smtp_pass' => 'MyBRancH19',      // Password gmail kamu
		            'smtp_port' => 465,
		            'crlf'      => "\r\n",
		            'newline'   => "\r\n"
		        ];
			
		        $this->load->library('email', $config);
		        $this->email->from('mybranch19@gmail.com', 'branchsch.tech');
		        $this->email->to($email);
				$this->email->subject('Forgot Password Branch');
				$this->email->message('<p>Anda melakukan permintaan reset password</p>
					<br><a href="'.site_url('C_login/reset_password/'.$reset_key).'">klik reset password</a>');
		        if ($this->email->send()) {
		        	echo "Check Your Email";
		        } else {
		        	echo "gagal mengirim email";
		        }
		    }else {
		      die("Email yang anda masukan belum terdaftar");
		    }
		} else{
			$this->load->view('forgotPassword');
		}
    }

    public function reset_password($token){                                                                           
      $data['token'] = $token;      
      $this->load->view('verifPassword', $data);
    }

    public function updatePass($token){
        $this->M_login->updatePass($token);
        $this->session->set_flashdata('pesanan', '<div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h5><i class="icon fa fa-check"></i> Password berhasil diperbarui !</h5>
        </div>');
        redirect('C_login');
    }
}
