<?php  
class M_kelolaProfil extends CI_Model {

    public function getProfil()
    {
        $this->db->select('*');

        $query = $this->db->get_where('user', ["user.isDelete" => 0, "UserID" => $this->session->userdata('UserID')]);
        return $query->row();
    }

    public function updateProfil_nImage($id)
    {
        $dataUser = array(
            'UserName' => $this->input->post('UserName'),
            'UserBirth' => $this->input->post('UserBirth'),
            'UserEmail' => $this->input->post('UserEmail'),
            'UserGender' => $this->input->post('Gender'),
            'updatedAt' => date("y-m-d H:i:s"),
        );
        $this->db->where("UserID = $id");
        $this->db->update('user', $dataUser);
    }

    public function updateProfil_wImage($id, $file_name)
    {

        $dataUser = array(
            'UserName' => $this->input->post('UserName'),
            'UserBirth' => $this->input->post('UserBirth'),
            'UserEmail' => $this->input->post('UserEmail'),
            'UserProfile' => $file_name,
            'UserGender' => $this->input->post('Gender'),
            'updatedAt' => date("y-m-d H:i:s"),
        );
        $this->db->where("UserID = $id");
        $this->db->update('user', $dataUser);
    }

    public function getProfilByID($data)
    {
        $this->db->select('*');

        $query = $this->db->get_where('user', ["user.isDelete" => 0, "UserPassword" => $data]);
        return $query->result();
    }

    public function changePassword($newPass)
    {
        $dataUser = array(
            'UserPassword' => $newPass,
            'updatedAt' => date("y-m-d H:i:s"),
        );
        $this->db->where("UserID", $this->session->userdata('UserID'));
        $this->db->update('user', $dataUser);
    }
}
?>