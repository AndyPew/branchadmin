<?php  
class M_kelolaJurusan extends CI_Model {

    public $user;
    public $teacher;
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getMajorByID($ID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('major', ["MajorID" => $ID]);
        return $query->result();
    }

    public function getSchoolLevel()
    {
        $this->db->select('*');
        $query = $this->db->get_where('schoollevel', ["isDelete" => 0]);
        return $query->result();
    }

    public function getAllJurusan()
    {
        $this->db->select('*');

        $this->db->join('schoollevel', "schoollevel.SchoolLevelID = major.SchoolLevelID", 'LEFT OUTER');
        $query = $this->db->get_where('major', ["major.isDelete" => 0]);
        return $query->result();
    }

    public function getAllJurusanSekolah($SchoolID)
    {
        $this->db->select('majorschool.* , major.MajorName');
        $this->db->join('major', "majorschool.MajorID = major.MajorID", 'LEFT OUTER');
        $query = $this->db->get_where('majorschool', ["majorschool.isDelete" => 0, 'majorschool.SchoolID' => $SchoolID]);
        return $query->result();
    }

    public function getDataSekolah($userID)
    {
        $this->db->select('*');

        $this->db->join('admin', "admin.AdminID = user.UserDetailID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = admin.SchoolID", 'CONCAT');

        $query = $this->db->get_where('user', ["user.UserID" => $userID,"user.isDelete" => 0]);
        return $query->result();
    }

    public function getAllJenjang()
    {
        $this->db->select('*');

        $query = $this->db->get_where('schoollevel', ["isDelete" => 0]);
        return $query->result();
    }

    public function deleteJurusan($ID) {
        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("major", $data, array('majorID' => $ID));
    }

    public function deleteJurusanSekolah($ID) {
        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("majorschool", $data, array('MajorSchoolID' => $ID));
    }

    public function addJurusan()
	{
        $user = array(
            'MajorName' => $this->input->post('MajorName'),
            'SchoolLevelID' => $this->input->post('SchoolLevelID'),
            'createdAt' => date("y-m-d H:i:s"),
        );
        return $this->db->insert('major', $user);
    }

    public function addJurusanSekolah()
	{
        $user = array(
            'MajorID' => $this->input->post('MajorID'),
            'SchoolID' => $this->input->post('SchoolID'),
            'createdAt' => date("y-m-d H:i:s"),
        );
        return $this->db->insert('majorschool', $user);
    }

    public function updateJurusan($id)
    {
        $user = array(
            'MajorName' => $this->input->post('MajorName'),
            'SchoolLevelID' => $this->input->post('SchoolLevelID'),
            'createdAt' => date("y-m-d H:i:s"),
        );
        $this->db->where("MajorID = $id");
        return $this->db->update('major', $user);
    }

    public function searchData($keyword)
    {
        $this->db->select('*');

        $this->db->join('schoollevel', "schoollevel.SchoolLevelID = major.SchoolLevelID", 'LEFT OUTER');
        $this->db->where("MajorName LIKE '%$keyword%' OR schoollevel.SchoolLevelID LIKE '%$keyword%'");
        $query = $this->db->get_where('major', ["major.isDelete" => 0]);
        return $query->result();    
    }

    public function searchData_lvl3($keyword, $ID)
    { 
        $this->db->select('*');

        $this->db->select('majorschool.* , major.MajorName');
        $this->db->join('major', "majorschool.MajorID = major.MajorID", 'LEFT OUTER');
        $this->db->where("MajorName LIKE '%$keyword%'");
        $query = $this->db->get_where('majorschool', ["majorschool.isDelete" => 0, "majorschool.SchoolID" =>$ID]);
        return $query->result();  
    }
}
?>