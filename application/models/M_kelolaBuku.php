<?php  
class M_kelolaBuku extends CI_Model {

    public $user;
    public $teacher;
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getBukuByID($ID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('book', ["BookID" => $ID]);
        return $query->result();
    }

    public function getAllPelajaran(){
        $this->db->select('*');
        $query = $this->db->get_where('subject', ["subject.isDelete" => 0]);
        return $query->result();
    }

    public function getAllPelajaranSekolah($ID){

        $this->db->select("subjectschool.*, subject.SubjectName");
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'LEFT OUTER');
        $query = $this->db->get_where('subjectschool', ["subjectschool.isDelete" => 0, "subjectschool.SchoolID" => $ID]);
        return $query->result();
    }

    public function getAllPelajaranSekolahByGuru($ID){

        $this->db->join('subjectschool', 'subjectschool.SubjectID = teachersubject.SubjectID', 'LEFT OUTER');
        $this->db->join('subject', 'subject.SubjectID = subjectschool.SubjectID', 'LEFT OUTER');
        $query = $this->db->get_where('teachersubject', ["teachersubject.isDelete" => 0, "teachersubject.UserID" => $ID]);
        return $query->result();
    }

    public function getAllKelas()
    {
        $this->db->select('*');
        $query = $this->db->get_where('class', ["class.isDelete" => 0]);
        return $query->result();
    }

    public function getAllKelasSekolah($ID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('class', ["class.isDelete" => 0, "class.SchoolID" => $ID]);
        return $query->result();
    }

    public function getDataSekolah($userID)
    {
        $this->db->select('*');

        $this->db->join('admin', "admin.AdminID = user.UserDetailID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = admin.SchoolID", 'CONCAT');

        $query = $this->db->get_where('user', ["user.UserID" => $userID,"user.isDelete" => 0]);
        return $query->result();
    }

    public function getAllBuku()
    {
        $this->db->select('*');

        $this->db->join('subject', "subject.SubjectID = book.SubjectID", 'CONCAT');
        $this->db->join('user', "user.UserID = book.UserID", 'CONCAT');
        $this->db->join('class', "class.ClassID = book.ClassID", 'CONCAT');
        $query = $this->db->get_where('book', ["book.isDelete" => 0]);
        return $query->result();
    }

    public function deleteBuku($ID) {
        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("book", $data, array('BookID' => $ID));
    }

    public function addBuku($file_name)
	{
        $buku = array(
            'BookTitle' => $this->input->post('BookTitle'),
            'BookFileUrl' => $file_name,
            'UserID' => $this->session->userdata('UserID'),
            'ClassID' => $this->input->post('ClassID'),
            'SubjectID' =>$this->input->post('SubjectID'),
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
        );
        return $this->db->insert('book', $buku);
    }

    public function addBuku_noFile()
    {
        $buku = array(
            'BookTitle' => $this->input->post('BookTitle'),
            'UserID' => $this->session->userdata('UserID'),
            'ClassID' => $this->input->post('ClassID'),
            'SubjectID' =>$this->input->post('SubjectID'),
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
        );
        return $this->db->insert('book', $buku);
    }

    public function updateBuku($id)
    {
        $buku = array(
            'BookTitle' => $this->input->post('BookTitle'),
            'ClassID' => $this->input->post('ClassID'),
            'SubjectID' =>$this->input->post('SubjectID'),
            'createdAt' => date("y-m-d H:i:s"),
        );

        $this->db->where("BookID = $id");
        return $this->db->update('book', $buku);
    }

    public function searchData($keyword)
    {
        $this->db->select('*');

        $this->db->join('subject', "subject.SubjectID = book.SubjectID", 'CONCAT');
        $this->db->join('user', "user.UserID = book.UserID", 'CONCAT');
        $this->db->join('class', "class.ClassID = book.ClassID", 'CONCAT');
        $this->db->where("BookTitle LIKE '%$keyword%' OR class.ClassName LIKE '%$keyword%' OR user.UserName LIKE '%$keyword%' OR subject.SubjectName LIKE '%$keyword%'");
        $query = $this->db->get_where('book', ["book.isDelete" => 0]);
        return $query->result();
    }
}
?>