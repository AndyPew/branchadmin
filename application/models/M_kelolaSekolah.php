<?php  
class M_kelolaSekolah extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getSchoolUnverified()
    {
        $this->db->select('*');
        $query = $this->db->get_where('school', ["SchoolVerified" => 0, "isDelete" => 0]);
        return $query->result();
    }

    public function getAllSchool(){
        $this->db->select('*');

        $this->db->join('schoollevel', "schoollevel.SchoolLevelID = school.SchoolID", 'CONCAT');

        $query = $this->db->get_where('school', ["school.isDelete" => 0]);
        return $query->result();
    }

    public function getSchool()
    {
        $this->db->select('*');
        
        $this->db->join('schoollevel', "schoollevel.SchoolLevelID = school.SchoolLevelID", 'CONCAT');
        $query = $this->db->get_where('school', ["school.isDelete" => 0]);
        return $query->result();
    }

    public function getSchoolByID($ID)
    {
        $this->db->select('*');

        $query = $this->db->get_where('school', ["SchoolID" => $ID]);
        return $query->result();
    }

    public function addAdmin($SchoolID, $UserPassword)
	{
        $this->db->select('*');
        $School = $this->db->get_where('school', ["SchoolID" => $SchoolID])->result();
        
        $admin = array(
            'SchoolID' => $SchoolID,
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0
        );
        $this->db->insert('admin', $admin);
        $insert_id = $this->db->insert_id();
        $user = array(
            'UserName' => 'Admin '.$School[0]->SchoolName,
            'UserEmail' => $School[0]->SchoolEmail,
            'UserPassword' => $UserPassword,
            'UserGender' => 1,
            'UserProfile' => '',
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
            'UserLevel' => 3,
            'UserDetailID' => $insert_id
        );
        $this->db->insert('user', $user);

        $data = array(   
            'SchoolVerified' => 1,
        );
        return $this->db->update("school", $data, array('SchoolID' => $SchoolID));
    }

    public function CancelVerifikasi($id)
    {
        $data = array(   
            'SchoolVerified' => 0,
        );
        $this->db->where("SchoolID = $id");
        return $this->db->update("school", $data);
    }

    public function updateSekolah($id){
        $data = array(
            'SchoolStatisticNumber' => $this->input->post('SchoolStatisticNumber'),
            'SchoolNationalUnicID' => $this->input->post('SchoolNationalUnicID'),
            'SchoolEmail' => $this->input->post('SchoolEmail'),
            'SchoolName' => $this->input->post('SchoolName'),
            'SchoolAddress' => $this->input->post('SchoolAddress'),
            'SchoolLevelID' => $this->input->post('SchoolLevelID'),
            'updateAt' => date("y-m-d H:i:s"),
        );

        $this->db->where("SchoolID = $id");
        return $this->db->update('school', $data);
    }
    
    public function deleteSekolah($ID) {
        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("school", $data, array('SchoolID' => $ID));
    }

    public function searchData($keyword)
    {
        $this->db->select('*');

        $this->db->join('schoollevel', "schoollevel.SchoolLevelID = school.SchoolLevelID", 'CONCAT');
        $this->db->where("SchoolStatisticNumber LIKE '%$keyword%' OR SchoolNationalUnicID LIKE '%$keyword%' OR SchoolName LIKE '%$keyword%' OR SchoolEmail LIKE '%$keyword%'");
        $query = $this->db->get_where('school', ["school.isDelete" => 0]);
        return $query->result();    
    }
}
?>