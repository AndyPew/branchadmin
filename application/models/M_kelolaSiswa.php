<?php  
class M_kelolaSiswa extends CI_Model {

    public $user;
    public $teacher;
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getClassName()
    {
        $this->db->select('*');
        $this->db->from('class');
        $query = $this->db->get();
        return $query->result();
    }

    public function getClassNameBySchool($ID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('class', ["class.SchoolID" => $ID, "class.isDelete" => 0]);
        return $query->result();
    }

    public function getDataSekolah($userID)
    {
        $this->db->select('*');

        $this->db->join('admin', "admin.AdminID = user.UserDetailID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = admin.SchoolID", 'CONCAT');

        $query = $this->db->get_where('user', ["user.UserID" => $userID,"user.isDelete" => 0]);
        return $query->result();
    }

    public function getStudentByID($ID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('userstudent', ["UserStudentID" => $ID]);
        return $query->result();
    }

    public function getAllSiswa()
    {
        $this->db->select('*');

        $this->db->join('userstudent', "user.UserDetailID = userstudent.UserStudentID", 'CONCAT');
        $this->db->join('class', "class.ClassID = userstudent.ClassID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');

        $query = $this->db->get_where('user', ["user.isDelete" => 0, "user.UserLevel" => 1]);
        return $query->result();
    }

    public function getAllSiswaByID($ID)
    {
        $this->db->select('*');

        $this->db->join('userstudent', "user.UserDetailID = userstudent.UserStudentID", 'CONCAT');
        $this->db->join('class', "class.ClassID = userstudent.ClassID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');

        $query = $this->db->get_where('user', ["user.isDelete" => 0, "user.UserLevel" => 1, "class.SchoolID" =>$ID]);
        return $query->result();
    }

    public function deleteSiswa($ID) 
    {
        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("user", $data, array('UserID' => $ID));

        $this->db->select('UserDetailID');
        $ID2 = $this->db->get('user')->result();

        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("userstudent", $data, array('UserStudentID' => $ID2[0]->UserDetailID));
    }

    public function addSiswa($file_name, $UserPassword)
	{
        
        $userTeacher = array(
            'UserSchoolUnicID' => $this->input->post('UserSchoolUnicID'),
            'ClassID' => $this->input->post('ClassID'),
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
        );
        $this->db->insert('userstudent', $userTeacher);
        $insert_id = $this->db->insert_id();
        $user = array(
            'UserName' => $this->input->post('UserName'),
            'UserEmail' => $this->input->post('UserEmail'),
            'UserPassword' => $UserPassword,
            'UserGender' => $this->input->post('UserGender'),
            'UserProfile' => $file_name,
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
            'UserLevel' => 1,
            'UserDetailID' => $insert_id,
        );
        return $this->db->insert('user', $user);
    }

    public function addSiswa_noImage($UserPassword)
    {
        
        $userTeacher = array(
            'UserSchoolUnicID' => $this->input->post('UserSchoolUnicID'),
            'ClassID' => $this->input->post('ClassID'),
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
        );
        $this->db->insert('userstudent', $userTeacher);
        $insert_id = $this->db->insert_id();
        $user = array(
            'UserName' => $this->input->post('UserName'),
            'UserEmail' => $this->input->post('UserEmail'),
            'UserPassword' => $UserPassword,
            'UserGender' => $this->input->post('UserGender'),
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
            'UserLevel' => 1,
            'UserDetailID' => $insert_id,
        );
        return $this->db->insert('user', $user);
    }

    public function updateSiswa($id)
    {
        $dataUser = array(
            'UserName' => $this->input->post('UserName'),
            'UserEmail' => $this->input->post('UserEmail'),
            'UserGender' => $this->input->post('Gender'),
            'updateAt' => date("y-m-d H:i:s"),
        );
        $datauserstudent = array(
            'UserSchoolUnicID' => $this->input->post('UserSchoolUnicID'),
            'ClassID' => $this->input->post('ClassID'),
            'updateAt' => date("y-m-d H:i:s"),
        );
        $this->db->where("UserStudentID = $id");
        $this->db->update('userstudent', $datauserstudent);
        
        $this->db->where("UserDetailID = $id");
        $this->db->update('user', $dataUser);
    }

    public function searchData($keyword)
    {
        $this->db->select('*');

        $this->db->join('userstudent', "user.UserDetailID = userstudent.UserStudentID", 'CONCAT');
        $this->db->join('class', "class.ClassID = userstudent.ClassID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->where("UserName LIKE '%$keyword%' OR userstudent.UserSchoolUnicID LIKE '%$keyword%' OR UserEmail LIKE '%$keyword%'");
        
        $query = $this->db->get_where('user', ["user.isDelete" => 0, "user.UserLevel" => 1]);
        return $query->result();
    }

    public function searchData_lvl3($ID, $keyword)
    {
         $this->db->select('*');

        $this->db->join('userstudent', "user.UserDetailID = userstudent.UserStudentID", 'CONCAT');
        $this->db->join('class', "class.ClassID = userstudent.ClassID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->where("UserName LIKE '%$keyword%' OR userstudent.UserSchoolUnicID LIKE '%$keyword%' OR UserEmail LIKE '%$keyword%'");

        $query = $this->db->get_where('user', ["user.isDelete" => 0, "user.UserLevel" => 1, "class.SchoolID" =>$ID]);
        return $query->result();
    }
}
?>