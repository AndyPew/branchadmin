<?php  
class M_kelolaPelajaran extends CI_Model {

    public $user;
    public $teacher;
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getAllPelajaran()
    {
        $this->db->select("*");
        // $this->db->join('major', "major.MajorID = subject.MajorID", 'LEFT OUTER');
        $query = $this->db->get_where('subject', ["subject.isDelete" => 0]);
        return $query->result();
    }

    public function getAllPelajaranSekolah($id)
    {
        $this->db->select("subjectschool.*, subject.SubjectName");
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'CONCAT');
        $query = $this->db->get_where('subjectschool', ["subjectschool.isDelete" => 0, "subjectschool.SchoolID" => $id]);
        return $query->result();
    }

    public function getPelajaranByID($ID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('subject', ["SubjectID" => $ID]);
        return $query->result();
    }

    public function deletePelajaran($ID) {
        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("subject", $data, array('SubjectID' => $ID));
    }

    public function deletePelajaranSekolah($ID) {
        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("subjectschool", $data, array('SubjectSchoolID' => $ID));
    }


    public function addPelajaran($file_name)
	{
        $data = array(
            'SubjectName' => $this->input->post('SubjectName'),
            'SubjectImage' => $file_name,
            'createdAt' => date("y-m-d H:i:s"),
        );
        return $this->db->insert('subject', $data);
    }

    public function addPelajaranNoImg()
	{
        $data = array(
            'SubjectName' => $this->input->post('SubjectName'),
            'createdAt' => date("y-m-d H:i:s"),
        );
        return $this->db->insert('subject', $data);
    }

    public function addPelajaranSekolah()
	{
        $user = array(
            'SchoolID' => $this->input->post('SchoolID'),
            'SubjectID' => $this->input->post('SubjectID'),
            'createdAt' => date("y-m-d H:i:s"),
        );
        return $this->db->insert('subjectschool', $user);
    }

    public function getDataSekolah($userID)
    {
        $this->db->select('*');

        $this->db->join('admin', "admin.AdminID = user.UserDetailID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = admin.SchoolID", 'CONCAT');

        $query = $this->db->get_where('user', ["user.UserID" => $userID,"user.isDelete" => 0]);
        return $query->result();
    }

    public function getAllJurusan()
    {
        $this->db->select('*');

        $this->db->join('schoollevel', "schoollevel.SchoolLevelID = subject.SubjectID", 'LEFT OUTER');
        $query = $this->db->get_where('subject', ["subject.isDelete" => 0]);
        return $query->result();
    }

    public function updatePelajaran($id, $file_name)
    {
        $user = array(
            'SubjectName' => $this->input->post('SubjectName'),
            'SubjectImage' => $file_name,
            'createdAt' => date("y-m-d H:i:s"),
        );
        $this->db->where("SubjectID = $id");
        return $this->db->update('subject', $user);
    }

    public function updatePelajaranNoImg($id)
    {
        $user = array(
            'SubjectName' => $this->input->post('SubjectName'),
            'createdAt' => date("y-m-d H:i:s"),
        );
        $this->db->where("SubjectID = $id");
        return $this->db->update('subject', $user);
    }

    public function searchData($keyword)
    {
        $this->db->select("*");

        $this->db->where("SubjectName LIKE '%$keyword%'");
        $query = $this->db->get_where('subject', ["subject.isDelete" => 0]);
        return $query->result();    
    }

    public function searchData_lvl3($keyword, $id)
    { 
        $this->db->select("subjectschool.*, subject.SubjectName");
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'CONCAT');
        $this->db->where("SubjectName LIKE '%$keyword%'");
        $query = $this->db->get_where('subjectschool', ["subjectschool.isDelete" => 0, "subjectschool.SchoolID" => $id]);
        return $query->result();
    }
}
?>