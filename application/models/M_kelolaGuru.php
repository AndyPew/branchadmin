<?php  
class M_kelolaGuru extends CI_Model {

    public $user;
    public $teacher;
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getSchoolName()
    {
        $this->db->select('*');
        $this->db->from('school');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataSekolah($userID)
    {
        $this->db->select('*');

        $this->db->join('admin', "admin.AdminID = user.UserDetailID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = admin.SchoolID", 'CONCAT');

        $query = $this->db->get_where('user', ["user.UserID" => $userID,"user.isDelete" => 0]);
        return $query->result();
    }

    public function getAllGuru()
    {
        $this->db->select('
            user.*,
            userteacher.UserNationalEmployeeID,
            userteacher.UserTeacherID,
            school.SchoolName,
            GROUP_CONCAT(subject.SubjectName) as MataPelajaran,
            GROUP_CONCAT(subject.SubjectID) as MataPelajaranID,
        ');

        $this->db->join('userteacher', "user.UserDetailID = userteacher.UserTeacherID", 'LEFT OUTER');
        $this->db->join('school', "school.SchoolID = userteacher.SchoolID", 'LEFT OUTER');
        $this->db->join('teachersubject', 'teachersubject.UserID = user.UserID', 'LEFT OUTER');
        $this->db->join('subjectschool', 'subjectschool.SubjectID = teachersubject.SubjectID', 'LEFT OUTER');
        $this->db->join('subject', 'subject.SubjectID = subjectschool.SubjectID', 'LEFT OUTER');
        $this->db->order_by("user.UserID DESC");
        $this->db->group_by("user.UserID");
        $query = $this->db->get_where('user', ["user.isDelete" => 0, "user.UserLevel" => 2]);
        return $query->result();
    }

    public function getAllGuruByID($ID)
    {
        $this->db->select('
            user.*,
            userteacher.UserNationalEmployeeID,
            userteacher.UserTeacherID,
            school.SchoolName,
            GROUP_CONCAT(subject.SubjectName) as MataPelajaran,
            GROUP_CONCAT(subject.SubjectID) as MataPelajaranID,
        ');

        $this->db->join('userteacher', "user.UserDetailID = userteacher.UserTeacherID", 'LEFT OUTER');
        $this->db->join('school', "school.SchoolID = userteacher.SchoolID", 'LEFT OUTER');
        $this->db->join('teachersubject', 'teachersubject.UserID = user.UserID', 'LEFT OUTER');
        $this->db->join('subjectschool', 'subjectschool.SubjectID = teachersubject.SubjectID', 'LEFT OUTER');
        $this->db->join('subject', 'subject.SubjectID = subjectschool.SubjectID', 'LEFT OUTER');
        $this->db->order_by("user.UserID DESC");
        $this->db->group_by("user.UserID");
        $query = $this->db->get_where('user', ["user.isDelete" => 0, "user.UserLevel" => 2, "userteacher.SchoolID" =>$ID, 'subjectschool.SchoolID' => $ID]);
        return $query->result();
    }

    public function getAllSubject()
    {
        $this->db->select('*');
        $query = $this->db->get_where('subject', ["subject.isDelete" => 0]);
        return $query->result();
    }

    public function getAllSubjectSekolah($id)
    {
        $this->db->select('*');
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'LEFT OUTER');
        $query = $this->db->get_where('subjectschool', ["subjectschool.isDelete" => 0, "subjectschool.SchoolID" => $id]);
        return $query->result();
    }


    public function deleteGuru($ID) 
    {
        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("user", $data, array('UserID' => $ID));

        $this->db->select('TeacherSubjectID');
        $ID3 = $this->db->get('teachersubject')->result();

        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("teachersubject", $data, array('TeacherSubjectID' => $ID3[0]->UserDetailID));

        $this->db->select('UserDetailID');
        $ID2 = $this->db->get('user')->result();


        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("userteacher", $data, array('UserTeacherID' => $ID2[0]->UserDetailID));
    }

    public function addGuru($file_name, $UserPassword)
	{
        $userTeacher = array(
            'UserNationalEmployeeID' => $this->input->post('UserNationalEmployeeID'),
            'SchoolID' => $this->input->post('SchoolID'),
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
        );
        $this->db->insert('userteacher', $userTeacher);
        $insert_id = $this->db->insert_id();
        $user = array(
            'UserName' => $this->input->post('UserName'),
            'UserEmail' => $this->input->post('UserEmail'),
            'UserPassword' => $UserPassword,
            'UserGender' => $this->input->post('UserGender'),
            'UserProfile' => $file_name,
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
            'UserLevel' => 2,
            'UserDetailID' => $insert_id,
        );
        return $this->db->insert('user', $user);
    }

    public function addGuru_noImage($UserPassword)
    {
        $userTeacher = array(
            'UserNationalEmployeeID' => $this->input->post('UserNationalEmployeeID'),
            'SchoolID' => $this->input->post('SchoolID'),
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
        );
        $this->db->insert('userteacher', $userTeacher);
        $insert_id = $this->db->insert_id();
        $user = array(
            'UserName' => $this->input->post('UserName'),
            'UserEmail' => $this->input->post('UserEmail'),
            'UserPassword' => $UserPassword,
            'UserGender' => $this->input->post('UserGender'),
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
            'UserLevel' => 2,
            'UserDetailID' => $insert_id,
        );
        return $this->db->insert('user', $user);
    }

    public function addTeacherSubject($id_guru)
    {
        if (!empty($this->input->post('SubjectID'))) {
            if (!empty($this->input->post('SubjectID2'))) {
                $teacherSubject = array(
                    'UserID' => $id_guru,
                    'SubjectID' => $this->input->post('SubjectID2'),
                    'createdAt' => date("y-m-d H:i:s"),
                    'isDelete' => 0,
                );
                $this->db->insert('teachersubject', $teacherSubject);
            }
            $teacherSubject = array(
                'UserID' => $id_guru,
                'SubjectID' => $this->input->post('SubjectID'),
                'createdAt' => date("y-m-d H:i:s"),
                'isDelete' => 0,
            );
            $this->db->insert('teachersubject', $teacherSubject);
        }
    }

    public function updateTeacherSubject($id)
    {
        $this->db->where("UserID = $id");
        $this->db->delete('teachersubject');

        $post = $this->input->post();
        $CheckID = $post["SubjectID"];
        if(is_array($CheckID) && count($CheckID) > 0){
            foreach ($CheckID as $key2 => $newSubject){
                if($newSubject != ''){
                $datateacherSubject = array(
                    'UserID' => $id,
                    'SubjectID' => $newSubject,
                    'updateAt' => date("y-m-d H:i:s"),
                    'isDelete' => 0,
                );
                }
                $this->db->where("UserID = $id");
                $this->db->insert('teachersubject', $datateacherSubject);
            }
        }        
    }

    public function updateGuru($id)
    {
        $dataUser = array(
            'UserName' => $this->input->post('UserName'),
            'UserEmail' => $this->input->post('UserEmail'),
            'UserGender' => $this->input->post('UserGender'),
            'updatedAt' => date("y-m-d H:i:s"),
        );
        $datauserteacher = array(
            'UserNationalEmployeeID' => $this->input->post('UserSchoolUnicID'),
            'SchoolID' => $this->input->post('SchoolID'),
            'updateAt' => date("y-m-d H:i:s"),
        );

        $this->db->where("UserteacherID = $id");
        $this->db->update('userteacher', $datauserteacher);
        
        $this->db->where("UserID = $id");
        $this->db->update('user', $dataUser);
    }

    public function searchData($keyword)
    {
        $this->db->select('*');

        $this->db->join('userteacher', "user.UserDetailID = userteacher.UserTeacherID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = userteacher.SchoolID", 'CONCAT');
        $this->db->where("UserName LIKE '%$keyword%' OR userteacher.UserNationalEmployeeID LIKE '%$keyword%' OR UserEmail LIKE '%$keyword%'");
        $query = $this->db->get_where('user', ["user.isDelete" => 0, "user.UserLevel" => 2]);
        return $query->result();    
    }

    public function searchData_lvl3($keyword, $ID)
    { 
        $this->db->select('*');

        $this->db->join('userteacher', "user.UserDetailID = userteacher.UserTeacherID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = userteacher.SchoolID", 'CONCAT');
        $this->db->where("UserName LIKE '%$keyword%' OR userteacher.UserNationalEmployeeID LIKE '%$keyword%' OR UserEmail LIKE '%$keyword%'");
        $query = $this->db->get_where('user', ["user.isDelete" => 0, "user.UserLevel" => 2, "userteacher.SchoolID" =>$ID]);
        return $query->result();  
    }
}
?>