<?php  
class M_kelolaKelas extends CI_Model {

    public $user;
    public $teacher;
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getClassByID($ID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('class', ["ClassID" => $ID]);
        return $query->result();
    }

    public function getAllKelas()
    {
        $this->db->select('*');

        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->join('major', "major.MajorID = class.MajorID", 'CONCAT');

        $query = $this->db->get_where('class', ["class.isDelete" => 0]);
        return $query->result();
    }

    public function getAllKelasByID($ID)
    {
        $this->db->select('*');

        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->join('major', "major.MajorID = class.MajorID", 'CONCAT');

        $query = $this->db->get_where('class', ["class.isDelete" => 0, "class.SchoolID" =>$ID]);
        return $query->result();
    }


    public function getDataSekolah($userID)
    {
        $this->db->select('*');

        $this->db->join('admin', "admin.AdminID = user.UserDetailID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = admin.SchoolID", 'CONCAT');

        $query = $this->db->get_where('user', ["user.UserID" => $userID,"user.isDelete" => 0]);
        return $query->result();
    }

    public function getAllJurusan()
    {
        $this->db->select('*');
        $query = $this->db->get_where('major', ["isDelete" => 0]);
        return $query->result();
    }

    public function getAllNamaSekolah()
    {
        $this->db->select('*');

        $query = $this->db->get_where('school', ["isDelete" => 0]);
        return $query->result();
    }

    public function getAllJurusanSekolah($id)
    {
        $this->db->select('*');
        $this->db->join('major', "major.MajorID = majorschool.MajorID", 'LEFT OUTER');
        $query = $this->db->get_where('majorschool', ["majorschool.isDelete" => 0, "majorschool.SchoolID" => $id ]);
        return $query->result();
    }

    public function deleteKelas($ID) {
        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("class", $data, array('ClassID' => $ID));
    }

    public function addKelas()
	{
        $user = array(
            'ClassName' => $this->input->post('ClassName'),
            'SchoolID' => $this->input->post('SchoolID'),
            'MajorID' => $this->input->post('MajorID'),
            'createdAt' => date("y-m-d H:i:s"),
        );
        return $this->db->insert('class', $user);
    }

    public function updateKelas($id)
    {
        $user = array(
            'ClassName' => $this->input->post('ClassName'),
            'SchoolID' => $this->input->post('SchoolID'),
            'MajorID' => $this->input->post('MajorID'),
            'createdAt' => date("y-m-d H:i:s"),
        );

        $this->db->where("ClassID = $id");
        return $this->db->update('class', $user);
    }

    public function searchData($keyword)
    {
        $this->db->select('*');

        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->join('major', "major.MajorID = class.MajorID", 'CONCAT');
        $this->db->where("ClassName LIKE '%$keyword%' OR school.SchoolName LIKE '%$keyword%' OR major.MajorName LIKE '%$keyword%'");
        $query = $this->db->get_where('class', ["class.isDelete" => 0]);
        return $query->result();    
    }

    public function searchData_lvl3($keyword, $ID)
    { 
        $this->db->select('*');

        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->join('major', "major.MajorID = class.MajorID", 'CONCAT');
        $this->db->where("ClassName LIKE '%$keyword%' OR school.SchoolName LIKE '%$keyword%' OR major.MajorName LIKE '%$keyword%'");
        $query = $this->db->get_where('class', ["class.isDelete" => 0, "class.SchoolID" =>$ID]);
        return $query->result();  
    }
}
?>