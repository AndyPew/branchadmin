<?php  
class M_kelolaTugas extends CI_Model {

    public $user;
    public $teacher;
	
	public function __construct() {
		$this->load->database();
    }

    public function getAllKelas() {
        $this->db->select('*');

        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->join('major', "major.MajorID = class.MajorID", 'CONCAT');

        $query = $this->db->get_where('class', ["class.isDelete" => 0]);
        return $query->result();
    }

    public function getAllKelasSekolah($ID) {
        $this->db->select('*');

        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->join('major', "major.MajorID = class.MajorID", 'CONCAT');

        $query = $this->db->get_where('class', ["class.isDelete" => 0, "class.SchoolID" =>$ID]);
        return $query->result();
    }

    public function getDataSekolah($userID)
    {
        $this->db->select('*');

        $this->db->join('admin', "admin.AdminID = user.UserDetailID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = admin.SchoolID", 'CONCAT');

        $query = $this->db->get_where('user', ["user.UserID" => $userID,"user.isDelete" => 0]);
        return $query->result();
    }

    public function getAllHomeWork() {
        $this->db->join('class', "class.ClassID = homework.ClassID", 'CONCAT');
        $this->db->join('subjectschool', "subjectschool.SubjectSchoolID = homework.SubjectID", 'CONCAT');
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'CONCAT');
        $query = $this->db->get_where('homework', ["homework.isDelete" => 0]);
        return $query->result();
    }

    public function getHomeWorkByID($ID) {
        $this->db->join('class', "class.ClassID = homework.ClassID", 'CONCAT');
        $this->db->join('subjectschool', "subjectschool.SubjectSchoolID = homework.SubjectID", 'CONCAT');
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'CONCAT');
        $query = $this->db->get_where('homework', ["homework.isDelete" => 0, 'homework.HomeWorkID' => $ID]);
        return $query->result();
    }

    public function getHomeWorkUserAnswerByID($HomeWorkID) {
        $query = $this->db->get_where('homework', ["isDelete" => 0, 'HomeWorkID' => $HomeWorkID])->result();

        $this->db->join('user', 'userstudent.UserStudentID = user.UserDetailID', 'CONCAT');
        return $user2 = $this->db->get_where('userstudent', ['userstudent.ClassID' => $query[0]->ClassID, 'userstudent.isDelete' => 0, 'user.UserLevel' => 1])->result();
    }

    public function getHomeWorkUserAnswerByHomeWorkID($UserID, $HomeWorkID) {
        $this->db->select('count(*) as total');
        $query = $this->db->get_where('homeworkuseranswer', ["isDelete" => 0, 'UserID' => $UserID, 'HomeWorkID' => $HomeWorkID]);
        return $query->result();
    }

    public function getHomeWorkUserAnswerByQuestionID($UserID, $HomeWorkQuestionID) {
        $query = $this->db->get_where('homeworkuseranswer', ["isDelete" => 0, 'UserID' => $UserID, 'HomeWorkQuestionID' => $HomeWorkQuestionID]);
        return $query->result();
    }

    public function getHomeWorkQuestionTotal($ID) {
        $this->db->select('count(*) as total');
        $this->db->join('homeworkanswerkey', "homeworkanswerkey.HomeWorkQuestionID = homeworkquestion.HomeWorkQuestionID", 'CONCAT');
        $query = $this->db->get_where('homeworkquestion', ["homeworkquestion.isDelete" => 0, 'homeworkquestion.HomeWorkID' => $ID]);
        return $query->result();
    }

    public function getHomeWorkQuestionByID($ID) {
        $this->db->join('homeworkanswerkey', "homeworkanswerkey.HomeWorkQuestionID = homeworkquestion.HomeWorkQuestionID", 'CONCAT');
        $query = $this->db->get_where('homeworkquestion', ["homeworkquestion.isDelete" => 0, 'homeworkquestion.HomeWorkID' => $ID]);
        return $query->result();
    }

    public function getAllSubject()
    {
        $this->db->select("subjectschool.*, subject.SubjectName");
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'LEFT OUTER');
        $query = $this->db->get_where('subjectschool', ["subjectschool.isDelete" => 0]);
        return $query->result();
    }

    public function getSubjectBySchoolID($ID)
    {
        $this->db->select("subjectschool.*, subject.SubjectName");
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'LEFT OUTER');
        $query = $this->db->get_where('subjectschool', ["subjectschool.isDelete" => 0, "subjectschool.SchoolID" => $ID]);
        return $query->result();
    }

    public function getSubjectByTeacherID($ID)
    {
        $this->db->join('subjectschool', 'subjectschool.SubjectID = teachersubject.SubjectID', 'LEFT OUTER');
        $this->db->join('subject', 'subject.SubjectID = subjectschool.SubjectID', 'LEFT OUTER');
        $query = $this->db->get_where('teachersubject', ["teachersubject.isDelete" => 0, "teachersubject.UserID" => $ID]);
        return $query->result();
    }

    public function getHomeWorkAnswerKeyByID($ID) {
        $query = $this->db->get_where('homeworkanswerkey', ["isDelete" => 0, 'HomeWorkQuestionID' => $ID]);
        return $query->result();
    }

    public function getHomeWorkAnswerByID($ID) {
        $query = $this->db->get_where('homeworkanswer', ["isDelete" => 0, 'HomeWorkQuestionID' => $ID]);
        return $query->result();
    }

    public function addHomeWork($date) {
        $homework = array(
            'HomeWorkTitle' => $this->input->post('title'),
            'HomeWorkDesc' => $this->input->post('desc'),
            'HomeWorkDate' => $date,
            'UserID' => $this->input->post('userID'),
            'ClassID' => $this->input->post('classID'),
            'SubjectID' => $this->input->post('subjectID'),
            'createdAt' => date("y-m-d H:i:s"),
        );
        $this->db->insert('homework', $homework);
        return $this->db->insert_id();
    }

    public function addHomeWorkQuestion($IDHomeWork, $Pertanyaan) {
        $homework = array(
            'HomeWorkQuestionContent' => $Pertanyaan,
            'HomeWorkQuestionType' => 'pilihan_ganda',
            'HomeWorkID' => $IDHomeWork,
            'createdAt' => date("y-m-d H:i:s"),
        );
        $this->db->insert('homeworkquestion', $homework);
        return $this->db->insert_id();
    }

    public function addHomeWorkAnswerKey($IDHomeWork, $value) {
        $homework = array(
            'HomeWorkAnswerKeyValue' => $value,
            'HomeWorkQuestionID' => $IDHomeWork,
            'createdAt' => date("y-m-d H:i:s"),
        );
        $this->db->insert('homeworkanswerkey', $homework);
        return $this->db->insert_id();
    }

    public function addHomeWorkAnswer($IDPertanyaan, $Jawaban) {
        $homework = array(
            'HomeWorkAnswerContent' => $Jawaban,
            'HomeWorkQuestionID' => $IDPertanyaan,
            'createdAt' => date("y-m-d H:i:s"),
        );
        return $this->db->insert('homeworkanswer', $homework);
    }

    public function getToken($ClassID){
        $this->db->join('user', 'userstudent.UserStudentID = user.UserDetailID', 'CONCAT');
        $user2 = $this->db->get_where('userstudent', ['userstudent.ClassID' => $ClassID, 'userstudent.isDelete' => 0, 'user.UserLevel' => 1])->result();
        return array_column($user2, 'LoginToken');
    }

    public function searchData($keyword)
    {
        $this->db->select('*');
        
        $this->db->join('class', "class.ClassID = homework.ClassID", 'CONCAT');
        $this->db->join('subjectschool', "subjectschool.SubjectSchoolID = homework.SubjectID", 'CONCAT');
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'CONCAT');
        $this->db->where("HomeWorkTitle LIKE '%$keyword%' OR class.ClassName LIKE '%$keyword%' OR subject.SubjectName LIKE '%$keyword%'"); 
        $query = $this->db->get_where('homework', ["homework.isDelete" => 0]);

        return $query->result();
    }

    public function searchData_lvl3($keyword, $ID)
    { 
        $this->db->select('*');
        
        $this->db->join('class', "class.ClassID = homework.ClassID", 'CONCAT');
        $this->db->join('subjectschool', "subjectschool.SubjectSchoolID = homework.SubjectID", 'CONCAT');
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'CONCAT');
        $this->db->where("HomeWorkTitle LIKE '%$keyword%' OR class.ClassName LIKE '%$keyword%' OR subject.SubjectName LIKE '%$keyword%'"); 
        $query = $this->db->get_where('homework', ["homework.isDelete" => 0]);

        return $query->result();
    }
}
?>