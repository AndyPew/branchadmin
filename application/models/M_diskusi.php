<?php  
class M_diskusi extends CI_Model {

    public $user;
    public $teacher;
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getDataGuru($UserID)
    {
        $this->db->select('*');
        $this->db->join('userteacher', "userteacher.UserTeacherID = user.UserDetailID", 'CONCAT');
        $query = $this->db->get_where('user', ["user.isDelete" => 0, "user.UserID" => $UserID]);
        return $query->row();
    }

    public function getDataKelas($UserID)
    {
        $this->db->select('*');
        $this->db->join('class', "class.ClassID = schoolschedule.ClassID", 'CONCAT');
        $query = $this->db->get_where('schoolschedule', ["schoolschedule.isDelete" => 0, "schoolschedule.UserID" => $UserID])->result();
        return array_unique(array_column($query, 'ClassID'));
    }

    public function getSiswaIDByClass($ClassID)
    {
        $this->db->select('*');
        $this->db->join('user', 'user.UserID = userstudent.UserStudentID', 'CONCAT');
        $query = $this->db->get_where('userstudent', ["userstudent.isDelete" => 0, "userstudent.ClassID" => $ClassID])->result();
        return array_unique(array_column($query, 'UserID'));
    }

    public function getGuruIDByClass($ClassID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('schoolschedule', ["schoolschedule.isDelete" => 0, "schoolschedule.ClassID" => $ClassID])->result();
        return array_unique(array_column($query, 'UserID'));
    }

    function getDiskusiByID($UserID){
        $this->db->select('discussion.*, user.UserName');
        $this->db->join('user', 'user.UserID = discussion.UserID', 'CONCAT');
        $this->db->order_by("discussion.createdAt", "asc");
        $this->db->where_in('user.UserID', $UserID);
        return $this->db->get_where('discussion', ['discussion.isDelete' => 0])->result();
    }

    public function getDataKelasByID($ClassID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('class', ["isDelete" => 0, "ClassID" => $ClassID]);
        return $query->row();
    }

    function insertDiskusi($DiscussionContent, $UserID){
        $Discussion = array(
            'DiscussionContent' =>$DiscussionContent,
            'UserID' => $UserID,
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
        );
        $this->db->insert('discussion', $Discussion);
    }
}
?>