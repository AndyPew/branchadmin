<?php  
class M_kelolaPengumuman extends CI_Model {

    public $user;
    public $teacher;
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getAllPengumuman()
    {
        $this->db->select('*');
        
        $this->db->join('school', "school.SchoolID = schoolannoucement.SchoolID", 'CONCAT');
        $query = $this->db->get_where('schoolannoucement', ["schoolannoucement.isDelete" => 0]);

        return $query->result();
    }

    public function getPengumumanByID($ID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('schoolannoucement', ["SchoolAnnoucementID" => $ID]);
        return $query->result();
    }

    public function getAllSekolah()
    {
        $this->db->select('*');

        $query = $this->db->get_where('school', ["isDelete" => 0]);
        return $query->result();
    }

    public function deletePengumuman($ID) {
        $data = array(   
            'isDelete' => 1,
        );
        $this->db->update("schoolannoucement", $data, array('SchoolAnnoucementID' => $ID));
    }

    public function addPengumuman($file_name)
	{
        $user = array(
            'SchoolAnnoucementTitle' => $this->input->post('SchoolAnnoucementTitle'),
            'SchoolAnnoucementDesc' => $this->input->post('SchoolAnnoucementDesc'),
            'SchoolID' => $this->input->post('SchoolID'),
            'SchoolAnnoucementImage' => $file_name,
            'createdAt' => date("y-m-d H:i:s"),
        );
        return $this->db->insert('schoolannoucement', $user);
    }

    public function addPengumuman_noImage()
    {
        $user = array(
            'SchoolAnnoucementTitle' => $this->input->post('SchoolAnnoucementTitle'),
            'SchoolAnnoucementDesc' => $this->input->post('SchoolAnnoucementDesc'),
            'SchoolID' => $this->input->post('SchoolID'),
            'createdAt' => date("y-m-d H:i:s"),
        );
        return $this->db->insert('schoolannoucement', $user);
    }

    public function updatePengumuman($id)
    {
        $user = array(
            'SchoolAnnoucementTitle' => $this->input->post('SchoolAnnoucementTitle'),
            'SchoolAnnoucementDesc' => $this->input->post('SchoolAnnoucementDesc'),
            'SchoolID' => $this->input->post('SchoolID'),
            'createdAt' => date("y-m-d H:i:s"),
        );
        $this->db->where("SchoolAnnoucementID = $id");
        return $this->db->update('SchoolAnnoucement', $user);
    }

    public function getToken($SchoolID)
    {
        $this->db->join('userstudent', 'userstudent.ClassID = class.ClassID', 'CONCAT');
        $this->db->join('user', 'userstudent.UserStudentID = user.UserDetailID', 'CONCAT');
        $user2 = $this->db->get_where('class', ['class.SchoolID' => $SchoolID, 'class.isDelete' => 0, 'user.UserLevel' => 1])->result();
        var_dump(array_column($user2, 'LoginToken'));
        return array_column($user2, 'LoginToken');
    }

    public function searchData($keyword)
    {
        $this->db->select('*');
        $this->db->where("SchoolAnnoucementTitle LIKE '%$keyword%' OR SchoolAnnoucementDesc LIKE '%$keyword%' OR school.SchoolName LIKE '%$keyword%'");
        $this->db->join('school', "school.SchoolID = schoolannoucement.SchoolID", 'CONCAT');
        $query = $this->db->get_where('schoolannoucement', ["schoolannoucement.isDelete" => 0]);

        return $query->result();
    }
}
?>