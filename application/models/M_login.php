<?php  
class M_login extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
        }

	public function Register()
	{
                $this->SchoolStatisticNumber = $this->input->post('SchoolStatisticNumber');
                $this->SchoolNationalUnicID = $this->input->post('SchoolNationalUnicID');
                $this->SchoolName = $this->input->post('SchoolName');
                $this->SchoolAddress = $this->input->post('SchoolAddress');
                $this->SchoolEmail = $this->input->post('SchoolEmail');
                $this->SchoolLevelID = $this->input->post('SchoolLevelID');
                $this->SchoolVerified = 0;
                $this->createdAt = date("y-m-d H:i:s");
                $this->isDelete = 0;
                return $this->db->insert('school', $this);
        }
        
        function check_login($_table2, $field2, $field5){
                $this->db->select('*');
                $this->db->from($_table2);
                $this->db->where($field2);
                $this->db->where($field5);
                $query = $this->db->get();
                if ($query->num_rows() == 0) {
                    return FALSE;
                } else {
                    return $query->result();
                }
        }

        function get_by_cookie($cookie){
                $this->db->select('*');
                $this->db->from('user');
                $this->db->where('LoginToken', $cookie);
                return $this->db->get();
        }

        function update($update_data, $id){
                $this->db->update('user', $update_data, array('UserID' => $id));
        }

        function logged_id(){
            return $this->session->userdata('UserLevel');
        }

        function logged_in($UserID){
                unset($this->UserID);        
                $post = $this->input->post();
                $data = array(
                    'LastOpenApp' => date("y-m-d H:i:s")
                );
                $this->db->update('user', $data, array('UserID' => $UserID));
        }

        public function update_reset_key($email, $reset_key){
            $this->db->where('UserEmail', $email);
            $data = array('ForgotKey' => $reset_key);
            $this->db->update('user', $data);
            if($this->db->affected_rows()>0){
              return TRUE;
            }else{
              return FALSE;
            }
        }

        public function updatePass($token){
          $pengacak = "p3ng4c4k";
          $password1 = MD5($this->input->post('UserPassword'));
          $password2 = md5($pengacak . md5($password1));
          $data = array(   
              'UserPassword' => $password2,
              'createdAt' => date("y-m-d H:i:s")
          );
          $this->db->update('user', $data, array('ForgotKey' => $token));
        }

        public function all($token){
          $this->db->select("
          user.UserName,
          user.UserEmail,
          user.gambar,
          user.created_at,
          user.updated_at ");
          return $this->db->get_where(user, ["reset_password" => $token])->row();
        }
}


?>