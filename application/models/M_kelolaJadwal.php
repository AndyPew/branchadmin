<?php  
class M_kelolaJadwal extends CI_Model {

    public $user;
    public $teacher;
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getAllKelas()
    {
        $this->db->select('*');

        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->join('major', "major.MajorID = class.MajorID", 'CONCAT');

        $query = $this->db->get_where('class', ["class.isDelete" => 0]);
        return $query->result();
    }

    public function getAllKelasByschoolID($id)
    {
        $this->db->select('*');

        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->join('major', "major.MajorID = class.MajorID", 'CONCAT');

        $query = $this->db->get_where('class', ["class.isDelete" => 0, "class.SchoolID" => $id]);
        return $query->result();
    }

    public function getDataSekolah($userID)
    {
        $this->db->select('*');

        $this->db->join('admin', "admin.AdminID = user.UserDetailID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = admin.SchoolID", 'CONCAT');

        $query = $this->db->get_where('user', ["user.UserID" => $userID,"user.isDelete" => 0]);
        return $query->result();
    }

    public function getKelasByID($ID)
    {
        $this->db->select('*');

        $this->db->join('school', "school.SchoolID = class.SchoolID", 'CONCAT');
        $this->db->join('major', "major.MajorID = class.MajorID", 'CONCAT');

        $query = $this->db->get_where('class', ["class.isDelete" => 0, "class.ClassID" => $ID]);
        return $query->row();
    }

    public function getAllGuru($id)
    {
        $this->db->select('*');

        $this->db->join('userteacher', "user.UserDetailID = userteacher.UserTeacherID", 'CONCAT');
        $this->db->join('school', "school.SchoolID = userteacher.SchoolID", 'CONCAT');
        $query = $this->db->get_where('user', ["user.isDelete" => 0, "user.UserLevel" => 2, "userteacher.SchoolID" => $id]);
        return $query->result();
    }

    public function getJadwalByClassDate($ClassID, $day)
    {
        $this->db->join('user', "user.UserID = schoolschedule.UserID", 'CONCAT');
        $this->db->join('subjectschool', "subjectschool.SubjectSchoolID = schoolschedule.SubjectSchoolID", 'CONCAT');
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'LEFT OUTER');
        $query = $this->db->get_where('schoolschedule', ["schoolschedule.isDelete" => 0, 'schoolschedule.ClassID' => $ClassID, 'schoolschedule.SchoolScheduleDay' => $day]);
        return $query->result();
    }

    public function getLastJadwalByClass($ClassID)
    {
        $this->db->select('schoolschedule.SchoolScheduleHour');
        $this->db->order_by("schoolschedule.SchoolScheduleHour", "desc");
        $query = $this->db->get_where('schoolschedule', ["schoolschedule.isDelete" => 0, 'schoolschedule.ClassID' => $ClassID]);
        return $query->row();
    }

    public function getAllPelajaranSekolah($id)
    {
        $this->db->select("subjectschool.*, subject.SubjectName");
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'LEFT OUTER');
        $query = $this->db->get_where('subjectschool', ["subjectschool.isDelete" => 0, "subjectschool.SchoolID" => $id]);
        return $query->result();
    }

    public function addJadwal($SchoolScheduleHour, $SchoolScheduleDay, $SchoolScheduleDesc, $UserID, $SubjectSchoolID, $ClassID)
	{
        $user = array(
            'SchoolScheduleHour' => $SchoolScheduleHour,
            'SchoolScheduleDay' => $SchoolScheduleDay,
            'SchoolScheduleDesc' => $SchoolScheduleDesc,
            'UserID' => $UserID,
            'SubjectSchoolID' => $SubjectSchoolID,
            'ClassID' => $ClassID,

            'createdAt' => date("y-m-d H:i:s"),
        );
        return $this->db->insert('schoolschedule', $user);
    }

    public function updateJadwal($SchoolScheduleHour, $SchoolScheduleDay, $SchoolScheduleDesc, $UserID, $SubjectSchoolID, $ClassID, $ID)
	{
        $user = array(
            'SchoolScheduleHour' => $SchoolScheduleHour,
            'SchoolScheduleDay' => $SchoolScheduleDay,
            'SchoolScheduleDesc' => $SchoolScheduleDesc,
            'UserID' => $UserID,
            'SubjectSchoolID' => $SubjectSchoolID,
            'ClassID' => $ClassID,

            'createdAt' => date("y-m-d H:i:s"),
            'updateAt' => date("y-m-d H:i:s"),
        );
        $this->db->where("SchoolScheduleID = $ID");
        return $this->db->update('schoolschedule', $user);
    }

    public function deleteJadwal($ID)
	{
        $user = array(
            'isDelete' => 1,
            'updateAt' => date("y-m-d H:i:s"),
        );
        $this->db->where("SchoolScheduleID = $ID");
        return $this->db->update('schoolschedule', $user);
    }
}
?>